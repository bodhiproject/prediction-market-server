#!/bin/sh
# Creates a compressed build context of the entire project.
# Uploads the compressed file to an S3 bucket.
# Triggers the image building pipeline stage.

# exit after any error (non-zero statement)
set -e

# create filename
FILENAME=pm-server-$CI_COMMIT_TAG.tgz

# create the file first so no error: "tar: file changed as we read it"
touch $FILENAME
# compress entire root folder
tar --exclude=$FILENAME --exclude='./.git' --exclude='./data' -zcvf $FILENAME .

# install AWS-CLI
apt update
apt install -y python-pip
pip install awscli --upgrade --user
export PATH=~/.local/bin:$PATH

# set AWS config
aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
aws configure set default.region $AWS_REGION
aws configure set default.output json

# use AWS-CLI to upload to S3
aws s3 cp $FILENAME $S3_BUCKET/$FILENAME

# trigger build image pipeline
curl \
--request POST \
--form token=$CI_JOB_TOKEN \
--form ref=$CI_COMMIT_TAG \
--form variables[BUILD_IMAGE]=true \
--form variables[VERSION]=$CI_COMMIT_TAG \
--form variables[FILENAME]=$FILENAME \
https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/trigger/pipeline
