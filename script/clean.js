const AWS = require('aws-sdk');
const axios = require('axios');

const DOCKER_HUB_REPO = 'bodhiproject/prediction-market';
const S3_BUCKET = 'kaniko-builds';
const CONTEXTS_FOLDER = 'prediction-market-server';
const FILENAME_REGEX = /pm-server-(\d+\.\d+\.\d+)\.tgz$/;

const getDockerToken = async () => {
  try {
    const res = await axios.post('https://hub.docker.com/v2/users/login/', {
      username: process.env.DOCKER_HUB_USERNAME,
      password: process.env.DOCKER_HUB_PASSWORD,
    });
    return res.data.token;
  } catch (err) {
    throw err;
  }
};

const getDockerHubTags = async (token) => {
  try {
    const res = await axios.get(
      `https://hub.docker.com/v2/repositories/${DOCKER_HUB_REPO}/tags/?page_size=100`,
      { headers: { Authorization: `JWT ${token}` } },
    );
    return res.data.results.map(json => json.name);
  } catch (err) {
    throw err;
  }
};

console.log(`Cleaning up ${S3_BUCKET}...`);

const s3 = new AWS.S3();
s3.listObjectsV2({ Bucket: S3_BUCKET, Prefix: `${CONTEXTS_FOLDER}/` }, async (err, data) => {
  if (err) throw err;

  // Get image tags from Docker Hub
  const token = await getDockerToken();
  const tags = await getDockerHubTags(token);

  // Check S3 files against Docker Hub tags
  const regex = new RegExp(FILENAME_REGEX);
  data.Contents.forEach((val) => {
    const match = val.Key.match(regex);
    if (match && tags.includes(match[1])) {
      // Matching tag found for S3 file, delete S3 file
      console.log('Found Docker Hub tag', match[1]);
      s3.deleteObject({ Bucket: S3_BUCKET, Key: val.Key }, (deleteErr) => {
        if (deleteErr) throw deleteErr;
        console.log('Deleted', val.Key);
      });
    }
  });
});
