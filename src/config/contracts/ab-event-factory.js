/* eslint-disable */
/**
 * Use the contract version number as the object key.
 * IMPORTANT: The deployBlocks must be a block in the future that has not already
 * been synced.
 */
module.exports = {
  1: {
    mainnet: '0x819d3acD24338b751FCc8C3627CaA7D534214550',
    mainnetDeployBlock: 6780194,
    testnet: '0x0592865afE7F4Ff441D7696F4801F1474Cd6e265',
    testnetDeployBlock: 6655394,
    abi: [{"inputs": [{"name": "configManager","type": "address"}],"payable": false,"stateMutability": "nonpayable","type": "constructor"},{"anonymous": false,"inputs": [{"indexed": true,"name": "eventAddress","type": "address"},{"indexed": true,"name": "ownerAddress","type": "address"}],"name": "ABEventCreated","type": "event"},{"constant": false,"inputs": [{"name": "from","type": "address"},{"name": "value","type": "uint256"},{"name": "data","type": "bytes"}],"name": "tokenFallback","outputs": [],"payable": false,"stateMutability": "nonpayable","type": "function"},{"constant": true,"inputs": [],"name": "version","outputs": [{"name": "","type": "uint16"}],"payable": false,"stateMutability": "pure","type": "function"}],
  },
  0: {
    mainnet: '0x64FeFCe74eAAd69471276Adcf50BC5FE2c51146B',
    mainnetDeployBlock: 6405625,
    testnet: '0xA0C96049f3BC93126cB5Eba299cAE3102814d33D',
    testnetDeployBlock: 6194663,
    abi: [{"inputs": [{"name": "configManager","type": "address"}],"payable": false,"stateMutability": "nonpayable","type": "constructor"},{"anonymous": false,"inputs": [{"indexed": true,"name": "eventAddress","type": "address"},{"indexed": true,"name": "ownerAddress","type": "address"}],"name": "ABEventCreated","type": "event"},{"constant": false,"inputs": [{"name": "from","type": "address"},{"name": "value","type": "uint256"},{"name": "data","type": "bytes"}],"name": "tokenFallback","outputs": [],"payable": false,"stateMutability": "nonpayable","type": "function"},{"constant": true,"inputs": [],"name": "version","outputs": [{"name": "","type": "uint16"}],"payable": false,"stateMutability": "pure","type": "function"}],
  },
};
/* eslint-enable */
