const fs = require('fs-extra');
const path = require('path');
const { map, sortBy, filter, each, isUndefined, isEmpty, isNumber, isNull } = require('lodash');
const { BLOCKCHAIN_ENV } = require('../constants');
const ConfigManager = require('./contracts/config-manager');
const MREventFactory = require('./contracts/mr-event-factory');
const MREvent = require('./contracts/mr-event');
const ABEventFactory = require('./contracts/ab-event-factory');
const ABEvent = require('./contracts/ab-event');
const RewardEventFactory = require('./contracts/reward-event-factory');
const RewardEvent = require('./contracts/reward-event');
const EventSig = require('./event-sig');

const Config = {
  NETWORK: process.env.NETWORK,
  PROTOCOL: process.env.SSL === 'true' ? 'https' : 'http',
  PROVIDER: process.env.PROVIDER,
  HTTP_PROVIDER_MAINNET: 'https://api.nakachain.org',
  HTTP_PROVIDER_TESTNET: 'https://testnet.api.nakachain.org',
  WS_PROVIDER_MAINNET: 'wss://api.nakachain.org/ws',
  WS_PROVIDER_TESTNET: 'wss://testnet.api.nakachain.org/ws',
  IPC_PROVIDER_MAINNET: process.env.IPC_PROVIDER_PATH || '/root/.naka/geth.ipc',
  IPC_PROVIDER_TESTNET: process.env.IPC_PROVIDER_PATH || '/root/.naka/geth.ipc',
  NAKABASE_API_KEY: process.env.NAKABASE_API_KEY,
  NAKA_BASE_MAINNET: 'https://base.nakachain.org/mainnet/naka',
  NAKA_BASE_TESTNET: 'https://base.nakachain.org/testnet/naka',
  HOSTNAME: 'localhost',
  API_PORT: 5000,
  MONGO_USERNAME: process.env.MONGO_USERNAME,
  MONGO_PASSWORD: process.env.MONGO_PASSWORD,
  MONGO_HOSTNAME: process.env.MONGO_HOSTNAME || 'localhost',
  MONGO_PORT: process.env.MONGO_PORT || 27017,
  MONGO_REPLICASET: process.env.MONGO_REPLICASET,
  DB_NAME_MAINNET: 'bodhipmMainnet',
  DB_NAME_TESTNET: 'bodhipmTestnet',
  DB_NAME_TEST: 'bodhipmTest',
  DB_LAST_MIGRATION: process.env.DB_LAST_MIGRATION,
  DEFAULT_LOG_LEVEL: 'debug',
  MIN_CONTRACT_VERSION_MAINNET_CLASSIC: 5,
  MIN_CONTRACT_VERSION_TESTNET_CLASSIC: 5,
  MIN_CONTRACT_VERSION_MAINNET_HOUSE: 0,
  MIN_CONTRACT_VERSION_TESTNET_HOUSE: 0,
  MIN_CONTRACT_VERSION_MAINNET_REWARD: 1,
  MIN_CONTRACT_VERSION_TESTNET_REWARD: 1,
};

const mrEventFactoryVersions = [];
const abEventFactoryVersions = [];
const rewardEventFactoryVersions = [];

/**
 * Validates the required env variables.
 */
const validateEnv = () => {
  if (Config.NETWORK !== 'mainnet' && Config.NETWORK !== 'testnet') {
    throw Error('NETWORK must be [mainnet|testnet]');
  }
  if (process.env.SSL !== 'true' && process.env.SSL !== 'false') {
    throw Error('SSL must be [true|false]');
  }
  if (!Config.NAKABASE_API_KEY) {
    throw Error('NAKABASE_API_KEY must be defined');
  }
};

/**
 * Creates the versionConfig to determine the start block for the sync. e.g.
 * @param {object} metadata EventFactory metadata
 * @param {number} minVer Minimum version to put into config
 * @param {array} arr Empty array to populate
 */
const createVersionConfig = (metadata, minVer, arr) => {
  // Get all version numbers and sort
  let keys = Object.keys(metadata);
  keys = sortBy(map(keys, key => Number(key)));

  // Filter out keys less than starting version
  keys = filter(keys, key => key >= minVer);
  if (keys.length === 0) throw Error('No EventFactory versions found');

  // Get the correct key for deploy block
  const blockKey = process.env.NETWORK === BLOCKCHAIN_ENV.MAINNET
    ? 'mainnetDeployBlock' : 'testnetDeployBlock';

  // Calculate start and end blocks for each version
  const maxVersion = keys[keys.length - 1];
  each(keys, (key) => {
    const startBlock = metadata[`${key}`][blockKey];
    const endBlock = key + 1 < maxVersion
      ? metadata[`${key + 1}`][blockKey] - 1
      : -1;
    arr.push({
      version: key,
      startBlock,
      endBlock,
    });
  });

  if (arr.length === 0) throw Error('Could not initialize versionConfig');
};

/**
 * Initializes the config needed for the server.
 */
const initConfig = () => {
  validateEnv();
  createVersionConfig(
    MREventFactory,
    getClassicMinContractVer(),
    mrEventFactoryVersions,
  );
  createVersionConfig(
    ABEventFactory,
    getHouseMinContractVer(),
    abEventFactoryVersions,
  );
  createVersionConfig(
    RewardEventFactory,
    getRewardMinContractVer(),
    rewardEventFactoryVersions,
  );
};

/**
 * Returns the base data dir path, and also creates it if necessary.
 * @return {string} Path to the base data directory.
 */
const getBaseDataDir = () => {
  let dataDir;

  // TEST_ENV=true set in environment
  if (process.env.TEST_ENV === 'true') {
    dataDir = path.resolve('./data/test');
    fs.ensureDirSync(dataDir);
    return dataDir;
  }

  // DATA_DIR is defined in environment
  if (!isEmpty(process.env.DATA_DIR)) {
    dataDir = path.resolve(process.env.DATA_DIR);
    fs.ensureDirSync(dataDir);
    return dataDir;
  }

  // Ensure network is defined
  dataDir = path.resolve(`./data/${Config.NETWORK}`);
  fs.ensureDirSync(dataDir);
  return dataDir;
};

/**
 * Returns the logs dir path, and also creates it if necesssary.
 * @return {string} Path to the logs directory.
 */
function getLogsDir() {
  const basePath = getBaseDataDir();
  const logsDir = `${basePath}/logs`;
  fs.ensureDirSync(logsDir);
  return path.resolve(logsDir);
}

/**
 * Returns if the network is mainnet or not.
 * @return {boolean} Is mainnet or not
 */
const isMainnet = () => Config.NETWORK === BLOCKCHAIN_ENV.MAINNET;

/**
 * Returns the MongoDB name.
 * @return {string} DB name
 */
const getDBName = () => {
  if (process.env.TEST_ENV === 'true') return Config.DB_NAME_TEST;
  if (process.env.MONGO_DB) return process.env.MONGO_DB;
  return isMainnet() ? Config.DB_NAME_MAINNET : Config.DB_NAME_TESTNET;
};

/**
 * Gets the ConfigManager smart contract metadata based on version.
 * @param version {Number} Version number of the contracts to get, e.g. 0, 1, 2.
 * @return {Object} Contract metadata.
 */
const configManagerMeta = (version) => {
  if (!isNumber(version)) throw Error('Must supply a version number');
  return ConfigManager[version];
};

/**
 * Gets the MREventFactory smart contract metadata based on version.
 * @param version {Number} Version number of the contracts to get, e.g. 0, 1, 2.
 * @return {Object} Contract metadata.
 */
const mrEventFactoryMeta = (version) => {
  if (!isNumber(version)) throw Error('Must supply a version number');
  return MREventFactory[version];
};

/**
 * Gets the ABEventFactory smart contract metadata based on version.
 * @param version {Number} Version number of the contracts to get, e.g. 0, 1, 2.
 * @return {Object} Contract metadata.
 */
const abEventFactoryMeta = (version) => {
  if (!isNumber(version)) throw Error('Must supply a version number');
  return ABEventFactory[version];
};

/**
 * Gets the RewardEventFactory smart contract metadata based on version.
 * @param version {Number} Version number of the contracts to get, e.g. 0, 1, 2.
 * @return {Object} Contract metadata.
 */
const rewardEventFactoryMeta = (version) => {
  if (!isNumber(version)) throw Error('Must supply a version number');
  return RewardEventFactory[version];
};

/**
 * Gets the MREvent smart contract metadata based on version.
 * @param version {Number} Version number of the contracts to get, e.g. 0, 1, 2.
 * @return {Object} Contract metadata.
 */
const mrEventMeta = (version) => {
  if (!isNumber(version)) throw Error('Must supply a version number');
  return MREvent[version];
};

/**
 * Gets the ABEvent smart contract metadata based on version.
 * @param version {Number} Version number of the contracts to get, e.g. 0, 1, 2.
 * @return {Object} Contract metadata.
 */
const abEventMeta = (version) => {
  if (!isNumber(version)) throw Error('Must supply a version number');
  return ABEvent[version];
};

/**
 * Gets the RewardEvent smart contract metadata based on version.
 * @param version {Number} Version number of the contracts to get, e.g. 0, 1, 2.
 * @return {Object} Contract metadata.
 */
const rewardEventMeta = (version) => {
  if (!isNumber(version)) throw Error('Must supply a version number');
  return RewardEvent[version];
};

/**
 * Determines the correct contract version for a block number.
 * @param {array} versions Array of version config.
 * @param {number} blockNum Block number to determine which contract version to use.
 * @return {number} Contract version.
 */
const determineContractVersion = (versions, blockNum) => {
  if (versions.length === 0) throw Error('versionConfig was not initialized');
  if (isUndefined(blockNum)) throw Error('blockNum is undefined');

  let contractVersion = null;
  each(versions, (cfg) => {
    // If block is in current version range, set version and break loop
    if ((blockNum >= cfg.startBlock && blockNum <= cfg.endBlock)
      || (blockNum >= cfg.startBlock && cfg.endBlock === -1)) {
      contractVersion = cfg.version;
      return false;
    }
    return true;
  });
  return contractVersion;
};

/**
 * Looks up and returns the Event metadata.
 * @param {string} eventSig Event signature (topic[0])
 * @param {number} blockNum Block number to determine which metadata to use
 * @return {object} Event metadata
 */
const getEventMeta = (eventSig, blockNum) => {
  switch (eventSig) {
    case EventSig.MultipleResultsEventCreated: {
      const ver = determineContractVersion(mrEventFactoryVersions, blockNum);
      return !isNull(ver) ? mrEventMeta(ver) : null;
    }
    case EventSig.ABEventCreated: {
      const ver = determineContractVersion(abEventFactoryVersions, blockNum);
      return !isNull(ver) ? abEventMeta(ver) : null;
    }
    case EventSig.RewardEventCreated: {
      const ver = determineContractVersion(rewardEventFactoryVersions, blockNum);
      return !isNull(ver) ? rewardEventMeta(ver) : null;
    }
    default: {
      throw Error('Invalid event signature');
    }
  }
};

const getClassicMinContractVer = () => (isMainnet()
  ? Config.MIN_CONTRACT_VERSION_MAINNET_CLASSIC
  : Config.MIN_CONTRACT_VERSION_TESTNET_CLASSIC);

const getHouseMinContractVer = () => (isMainnet()
  ? Config.MIN_CONTRACT_VERSION_MAINNET_HOUSE
  : Config.MIN_CONTRACT_VERSION_TESTNET_HOUSE);

const getRewardMinContractVer = () => (isMainnet()
  ? Config.MIN_CONTRACT_VERSION_MAINNET_REWARD
  : Config.MIN_CONTRACT_VERSION_TESTNET_REWARD);

const getSSLCredentials = () => {
  if (!process.env.SSL_KEY || !process.env.SSL_CERT) {
    throw Error('SSL Key and Cert not found');
  }

  return {
    key: process.env.SSL_KEY,
    cert: process.env.SSL_CERT,
  };
};

module.exports = {
  Config,
  initConfig,
  getBaseDataDir,
  getLogsDir,
  isMainnet,
  getDBName,
  configManagerMeta,
  mrEventFactoryMeta,
  abEventFactoryMeta,
  rewardEventFactoryMeta,
  mrEventMeta,
  abEventMeta,
  rewardEventMeta,
  getEventMeta,
  getClassicMinContractVer,
  getHouseMinContractVer,
  getRewardMinContractVer,
  getSSLCredentials,
};
