const { isNull } = require('lodash');
const EventsDB = require('../db/controllers/events');
const logger = require('../utils/logger');

/**
 * Updates the event if the result set is the latest one.
 * @param {object} resultSet Parsed result set event.
 */
module.exports = async (resultSet) => {
  const event = await EventsDB.findOne({ address: resultSet.eventAddress });

  // Log error if event does not exist
  if (isNull(event)) {
    logger.warn(`Error updateEvent ${resultSet.eventAddress}: Event does not exist`);
    return;
  }

  if (event.currentRound < resultSet.eventRound + 1) {
    await EventsDB.updateOne({
      txid: event.txid,
      currentRound: resultSet.eventRound + 1,
      currentResultIndex: resultSet.resultIndex,
      consensusThreshold: resultSet.nextConsensusThreshold,
      arbitrationEndTime: resultSet.nextArbitrationEndTime,
    });
  }
};
