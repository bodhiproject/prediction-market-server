const { each, isNull, find } = require('lodash');
const web3 = require('../web3');
const { TX_STATUS } = require('../constants');
const { toLowerCase } = require('../utils');
const logger = require('../utils/logger');
const { getTransactionReceipt } = require('../utils/web3-utils');
const TxReceiptsDB = require('../db/controllers/tx-receipts');
const EventsDB = require('../db/controllers/events');
const WithdrawsDB = require('../db/controllers/withdraws');
const EventSig = require('../config/event-sig');
const parseWithdraw = require('./parsers/withdraw');
const insertName = require('./insert-name');

const parseAndUpdate = async (log, txReceipt) => {
  const withdraw = parseWithdraw({ log });

  // Stop parsing if withdraw exists
  const existing = await WithdrawsDB.findOne({ txid: withdraw.txid });
  if (existing && existing.txStatus !== TX_STATUS.PENDING) {
    return;
  }

  // Stop parsing if event does not exist (previous version)
  const event = await EventsDB.findOne({ address: withdraw.eventAddress });
  if (isNull(event)) return;

  // Insert new withdraw
  withdraw.eventType = event.eventType;
  await WithdrawsDB.updateOne(withdraw);

  // Fetch tx receipt if needed before update
  if (txReceipt) await TxReceiptsDB.updateOne(txReceipt);
  else await TxReceiptsDB.updateOne(await getTransactionReceipt(withdraw.txid));

  // Add to event's withdrawn list
  await EventsDB.addToWithdrawnList(withdraw);

  // Query and insert name
  await insertName(withdraw.winnerAddress);
};

const syncWinningsWithdrawn = async (
  { startBlock, endBlock, syncPromises, limit },
) => {
  try {
    // Fetch logs
    const logs = await web3.eth.getPastLogs({
      fromBlock: startBlock,
      toBlock: endBlock,
      topics: [EventSig.WinningsWithdrawn],
    });
    if (logs.length === 0) return;

    // Add to syncPromises array to be executed in parallel
    logger.info(`Found ${logs.length} WinningsWithdrawn`);
    each(logs, (log) => {
      syncPromises.push(limit(async (logObj) => {
        try {
          await parseAndUpdate(logObj);
        } catch (insertErr) {
          logger.error('Error syncWinningsWithdrawn parse');
          throw insertErr;
        }
      }, log));
    });
  } catch (err) {
    logger.error('Error syncWinningsWithdrawn');
    throw err;
  }
};

const pendingWinningsWithdrawn = async ({ syncPromises, limit }) => {
  try {
    const pending = await WithdrawsDB.find({ txStatus: TX_STATUS.PENDING });
    if (pending.length === 0) return;
    logger.info(`Checking ${pending.length} pending WinningsWithdrawn`);

    each(pending, (p) => {
      syncPromises.push(limit(async (pendingWithdraw) => {
        try {
          const txReceipt = await getTransactionReceipt(pendingWithdraw.txid);
          if (isNull(txReceipt)) return;

          if (txReceipt.status) {
            // Parse individual log with success status
            const logs = await web3.eth.getPastLogs({
              fromBlock: txReceipt.blockNum,
              toBlock: txReceipt.blockNum,
              topics: [EventSig.WinningsWithdrawn],
            });
            const foundLog = find(
              logs,
              log => toLowerCase(log.transactionHash) === txReceipt.transactionHash,
            );
            if (foundLog) await parseAndUpdate(foundLog, txReceipt);
          } else {
            // Update withdraw with failed status
            await WithdrawsDB.updateOne({
              txid: txReceipt.transactionHash,
              txStatus: TX_STATUS.FAIL,
            });
            await TxReceiptsDB.updateOne(txReceipt);
          }
        } catch (insertErr) {
          logger.error(`Error pendingWinningsWithdrawn: ${insertErr.message}`);
        }
      }, p));
    });
  } catch (err) {
    logger.error(`Error pendingWinningsWithdrawn WithdrawsDB.find: ${err.message}`);
  }
};

module.exports = {
  syncWinningsWithdrawn,
  pendingWinningsWithdrawn,
};
