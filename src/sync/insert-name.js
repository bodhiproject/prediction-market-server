const { isUndefined, forEach, isArray } = require('lodash');
const { resolveAddress } = require('../api/address-name-service');
const Name = require('../models/name');
const NamesDB = require('../db/controllers/names');

/**
 * Insert name to NamesDB when the address has a name.
 * @param {string} address Address to check name
 */
module.exports = async (address) => {
  if (isUndefined(address)) return;

  let addresses = address;
  if (!isArray(address)) addresses = [address];
  const toCheckAddresses = [];
  let promises = [];
  forEach(addresses, async (item, key) => {
    promises.push(new Promise(async (resolve, reject) => {
      try {
        const ret = await NamesDB.findOne({ address: item });
        if (!ret) {
          toCheckAddresses.push(item);
        }
        resolve();
      } catch (err) {
        reject(err);
      }
    }));
  });
  await Promise.all(promises);

  const addressNameObj = await resolveAddress(toCheckAddresses);
  Object.keys(addressNameObj).forEach(key =>
    (addressNameObj[key] === '') && delete addressNameObj[key]);

  promises = [];
  forEach(addressNameObj, async (name, key) => {
    promises.push(new Promise(async (resolve, reject) => {
      try {
        await NamesDB.updateOne(new Name({ address: key, name }));
        resolve();
      } catch (err) {
        reject(err);
      }
    }));
  });
  await Promise.all(promises);
};
