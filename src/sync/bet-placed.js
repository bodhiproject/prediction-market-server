const { each, isNull, find } = require('lodash');
const web3 = require('../web3');
const { TX_STATUS, EVENT_TYPE } = require('../constants');
const { toLowerCase } = require('../utils');
const logger = require('../utils/logger');
const { getTransactionReceipt } = require('../utils/web3-utils');
const TxReceiptsDB = require('../db/controllers/tx-receipts');
const BetsDB = require('../db/controllers/bets');
const EventsDB = require('../db/controllers/events');
const EventLeaderboardUsersDB = require('../db/controllers/event-leaderboard-users');
const EventSig = require('../config/event-sig');
const parseBet = require('./parsers/bet');
const EventLeaderboardUser = require('../models/event-leaderboard-user');
const EventAPI = require('../api/ab-event');
const insertName = require('./insert-name');

const parseAndUpdate = async (log, txReceipt) => {
  const bet = parseBet({ log });

  // Stop parsing if bet exists
  const existing = await BetsDB.findOne({ txid: bet.txid });
  if (existing && existing.txStatus !== TX_STATUS.PENDING) {
    return;
  }

  // Stop parsing if event does not exist (previous version)
  const event = await EventsDB.findOne({ address: bet.eventAddress });
  if (isNull(event)) return;
  // Insert new bet
  bet.eventType = event.eventType;
  await BetsDB.updateOne(bet);

  // Fetch tx receipt if needed before update
  if (txReceipt) await TxReceiptsDB.updateOne(txReceipt);
  else await TxReceiptsDB.updateOne(await getTransactionReceipt(bet.txid));

  // Increment bet round results amount
  await EventsDB.addToBetRoundResultsAmount(bet);

  // Only update maxBets for ABEvents
  if (event.eventType === EVENT_TYPE.AB_EVENT) {
    const maxBets = await EventAPI.maxBets({ eventAddress: bet.eventAddress });
    await EventsDB.updateOneByAddress(bet.eventAddress, { maxBets });
  }

  // Update event leaderboard investments
  const leaderboardUser = new EventLeaderboardUser({
    eventAddress: bet.eventAddress,
    userAddress: bet.betterAddress,
    investments: bet.amount,
    winnings: '0',
  });
  const count = await EventLeaderboardUsersDB.count({
    userAddress: leaderboardUser.userAddress,
    eventAddress: leaderboardUser.eventAddress,
  });
  if (count === 0) await EventLeaderboardUsersDB.updateOne(leaderboardUser);
  else await EventLeaderboardUsersDB.addToInvestments(leaderboardUser);

  // Query and insert name
  await insertName(bet.betterAddress);
};

const syncBetPlaced = async ({ startBlock, endBlock, syncPromises, limit }) => {
  try {
    // Fetch logs
    const logs = await web3.eth.getPastLogs({
      fromBlock: startBlock,
      toBlock: endBlock,
      topics: [EventSig.BetPlaced],
    });
    if (logs.length === 0) return;

    // Add to syncPromises array to be executed in parallel
    logger.info(`Found ${logs.length} BetPlaced`);
    each(logs, (log) => {
      syncPromises.push(limit(async (logObj) => {
        try {
          await parseAndUpdate(logObj);
        } catch (insertErr) {
          logger.error('Error syncBetPlaced parse');
          throw insertErr;
        }
      }, log));
    });
  } catch (err) {
    logger.error('Error syncBetPlaced');
    throw err;
  }
};

const pendingBetPlaced = async ({ syncPromises, limit }) => {
  try {
    const pending = await BetsDB.find({
      txStatus: TX_STATUS.PENDING,
      eventRound: 0,
    });
    if (pending.length === 0) return;
    logger.info(`Checking ${pending.length} pending BetPlaced`);

    each(pending, (p) => {
      syncPromises.push(limit(async (pendingBet) => {
        try {
          const txReceipt = await getTransactionReceipt(pendingBet.txid);
          if (isNull(txReceipt)) return;

          if (txReceipt.status) {
            // Parse individual log with success status
            const logs = await web3.eth.getPastLogs({
              fromBlock: txReceipt.blockNum,
              toBlock: txReceipt.blockNum,
              topics: [EventSig.BetPlaced],
            });
            const foundLog = find(
              logs,
              log => toLowerCase(log.transactionHash) === txReceipt.transactionHash,
            );
            if (foundLog) await parseAndUpdate(foundLog, txReceipt);
          } else {
            // Update bet with failed status
            await BetsDB.updateOne({
              txid: txReceipt.transactionHash,
              txStatus: TX_STATUS.FAIL,
            });
            await TxReceiptsDB.updateOne(txReceipt);
          }
        } catch (insertErr) {
          logger.error(`Error pendingBetPlaced: ${insertErr.message}`);
        }
      }, p));
    });
  } catch (err) {
    logger.error(`Error pendingBetPlaced BetsDB.find: ${err.message}`);
  }
};

module.exports = {
  syncBetPlaced,
  pendingBetPlaced,
};
