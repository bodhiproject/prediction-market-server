const { each, find } = require('lodash');
const { eachOfSeries } = require('async');
const web3 = require('../web3');
const { TX_STATUS, EVENT_TYPE } = require('../constants');
const logger = require('../utils/logger');
const BetsDB = require('../db/controllers/bets');
const ResultSetsDB = require('../db/controllers/result-sets');
const EventLeaderboardUsersDB = require('../db/controllers/event-leaderboard-users');
const GlobalLeaderboardUsersDB = require('../db/controllers/global-leaderboard-users');
const MREventAPI = require('../api/mr-event');
const ABEventAPI = require('../api/ab-event');
const RewardEventAPI = require('../api/reward-event');
const EventLeaderboardUser = require('../models/event-leaderboard-user');
const GlobalLeaderboardUser = require('../models/global-leaderboard-user');

/**
 * Accumulates all the bets for a user.
 * @param {array} txs Bets to parse
 * @return {string} Amount of total investments for user
 */
const getInvestments = async (txs) => {
  try {
    const accumulated = txs.reduce((acc, cur) => {
      const amount = web3.utils.toBN(cur.amount);
      if (!cur.betterAddress) cur.betterAddress = cur.centralizedOracleAddress;
      if (Object.keys(acc).includes(cur.betterAddress)) {
        acc[cur.betterAddress] = web3.utils.toBN(acc[cur.betterAddress])
          .add(amount).toString(10);
      } else {
        acc[cur.betterAddress] = amount.toString(10);
      }
      return acc;
    }, {});
    return accumulated;
  } catch (err) {
    logger.error(`Error getInvestments: ${err.message}`);
    throw err;
  }
};

/**
 * Calls the proper contract API to get the winnings for the user.
 * @param {object} event Event object
 * @param {string} address User to query
 * @return {string} User's winnings
 */
const getWinnings = async (event, address) => {
  try {
    let res;
    if (event.eventType === EVENT_TYPE.MR_EVENT) {
      res = await MREventAPI.calculateWinnings({
        eventAddress: event.address,
        address,
      });
    } else if (event.eventType === EVENT_TYPE.AB_EVENT) {
      res = await ABEventAPI.calculateWinnings({
        eventAddress: event.address,
        address,
      });
    } else if (event.eventType === EVENT_TYPE.REWARD_EVENT) {
      res = await RewardEventAPI.calculateWinnings({
        eventAddress: event.address,
        address,
      });
    } else {
      throw Error(`Invalid eventType for updating leaderboard: ${event.eventType}`);
    }
    return res.toString(10);
  } catch (err) {
    logger.error(`Error getWinnings: ${err.message}`);
    throw err;
  }
};

/**
 * Updates EventLeaderboardUser winnings
 * @param {object} event Event object
 * @param {string} userAddress User to update
 * @param {string} winnings Amount user won
 */
const updateEventLeaderboard = async (event, userAddress, winnings) => {
  try {
    const eventUser = new EventLeaderboardUser({
      eventAddress: event.address,
      userAddress,
      investments: '0', // user already has investments
      winnings,
    });
    await EventLeaderboardUsersDB.addToWinnings(eventUser);
  } catch (err) {
    logger.error(`Error updateEventLeaderboard: ${err.message}`);
    throw err;
  }
};

/**
 * Updates GlobalLeaderboardUser investments and winnings
 * @param {string} userAddress User to update
 * @param {string} investments Amount user invested
 * @param {string} winnings Amount user won
 */
const updateGlobalLeaderboard = async (userAddress, investments, winnings) => {
  try {
    const user = new GlobalLeaderboardUser({
      userAddress,
      investments,
      winnings,
    });
    const count = await GlobalLeaderboardUsersDB.count({
      userAddress: user.userAddress,
    });
    if (count === 0) await GlobalLeaderboardUsersDB.updateOne(user);
    else await GlobalLeaderboardUsersDB.addToInvestmentsAndWinnings(user);
  } catch (err) {
    logger.error(`Error updateGlobalLeaderboard: ${err.message}`);
    throw err;
  }
};

/**
 * Wrapper for making async calls within the loop.
 * @param {object} event Event object
 * @param {string} userAddress User to update
 * @param {string} investments Amount user invested
 * @param {function} callback To call after all async functions done
 */
const processLeaderboardUser = async (event, userAddress, investments, callback) => {
  const winnings = await getWinnings(event, userAddress);
  await updateEventLeaderboard(event, userAddress, winnings);
  await updateGlobalLeaderboard(userAddress, investments, winnings);
  callback();
};

const updateLeaderboardUsers = async ({ newWithdrawEvents, syncPromises, limit }) => {
  try {
    each(newWithdrawEvents, async (withdrawEvent) => {
      syncPromises.push(limit(async (event) => {
        const filtered = [];
        let investments;
        try {
          // get all the participants
          const bets = await BetsDB.find({
            eventAddress: event.address,
            txStatus: TX_STATUS.SUCCESS,
          });
          const resultSets = await ResultSetsDB.find({
            eventAddress: event.address,
            txStatus: TX_STATUS.SUCCESS,
            eventRound: 0,
          });
          const txs = resultSets.concat(bets);
          investments = await getInvestments(txs);
          each(txs, (tx) => {
            if (!find(filtered, {
              eventAddress: tx.eventAddress,
              betterAddress: tx.betterAddress,
            })) {
              filtered.push(tx);
            }
          });
        } catch (err) {
          logger.error(`Error calculating investments: ${err.message}`);
          throw err;
        }

        await eachOfSeries(filtered, (value, key, callback) => {
          try {
            const { betterAddress, centralizedOracleAddress } = value;
            const userAddress = betterAddress || centralizedOracleAddress;
            processLeaderboardUser(
              event,
              userAddress,
              investments[userAddress],
              callback,
            );
          } catch (err) {
            callback(err);
          }
        }, (err) => {
          if (err) {
            logger.error(`Error processing leaderboard user: ${err.message}`);
            throw err;
          }
        });
      }, withdrawEvent));
    });
  } catch (err) {
    logger.error('Error updateLeaderboardUsers');
    throw err;
  }
};

module.exports = updateLeaderboardUsers;
