const { each, isNull, find } = require('lodash');
const updateEvent = require('./update-event');
const web3 = require('../web3');
const { TX_STATUS } = require('../constants');
const { toLowerCase } = require('../utils');
const logger = require('../utils/logger');
const { getTransactionReceipt } = require('../utils/web3-utils');
const TxReceiptsDB = require('../db/controllers/tx-receipts');
const ResultSetsDB = require('../db/controllers/result-sets');
const EventsDB = require('../db/controllers/events');
const EventSig = require('../config/event-sig');
const parseResultSet = require('./parsers/result-set');

const parseAndUpdate = async (log, txReceipt) => {
  const resultSet = parseResultSet({ log });

  // Stop parsing if result set exists
  const existing = await ResultSetsDB.findOne({ txid: resultSet.txid });
  if (existing && existing.txStatus !== TX_STATUS.PENDING) {
    return;
  }

  // Stop parsing if event does not exist (previous version)
  const event = await EventsDB.findOne({ address: resultSet.eventAddress });
  if (isNull(event)) return;
  // Insert new vote result set
  resultSet.eventType = event.eventType;
  await ResultSetsDB.updateOne(resultSet);

  // Fetch tx receipt if needed before update
  if (txReceipt) await TxReceiptsDB.updateOne(txReceipt);
  else await TxReceiptsDB.updateOne(await getTransactionReceipt(resultSet.txid));

  // Update event's round, index, and threshold
  await updateEvent(resultSet);
};

const syncVoteResultSet = async (
  { startBlock, endBlock, syncPromises, limit },
) => {
  try {
    // Fetch logs
    const logs = await web3.eth.getPastLogs({
      fromBlock: startBlock,
      toBlock: endBlock,
      topics: [EventSig.VoteResultSet],
    });
    if (logs.length === 0) return;

    // Add to syncPromises array to be executed in parallel
    logger.info(`Found ${logs.length} VoteResultSet`);
    each(logs, (log) => {
      syncPromises.push(limit(async (logObj) => {
        try {
          await parseAndUpdate(logObj);
        } catch (insertErr) {
          logger.error('Error syncVoteResultSet parse');
          throw insertErr;
        }
      }, log));
    });
  } catch (err) {
    logger.error('Error syncVoteResultSet');
    throw err;
  }
};

const pendingVoteResultSet = async ({ syncPromises, limit }) => {
  try {
    const pending = await ResultSetsDB.find({
      txStatus: TX_STATUS.PENDING,
      eventRound: { $gte: 1 },
    });
    if (pending.length === 0) return;
    logger.info(`Checking ${pending.length} pending VoteResultSet`);

    each(pending, (p) => {
      syncPromises.push(limit(async (pendingSet) => {
        try {
          const txReceipt = await getTransactionReceipt(pendingSet.txid);
          if (isNull(txReceipt)) return;

          if (txReceipt.status) {
            // Parse individual log with success status
            const logs = await web3.eth.getPastLogs({
              fromBlock: txReceipt.blockNum,
              toBlock: txReceipt.blockNum,
              topics: [EventSig.VoteResultSet],
            });
            const foundLog = find(
              logs,
              log => toLowerCase(log.transactionHash) === txReceipt.transactionHash,
            );
            if (foundLog) await parseAndUpdate(foundLog, txReceipt);
          } else {
            // Update result set with failed status
            await ResultSetsDB.updateOne({
              txid: txReceipt.transactionHash,
              txStatus: TX_STATUS.FAIL,
            });
            await TxReceiptsDB.updateOne(txReceipt);
          }
        } catch (insertErr) {
          logger.error(`Error pendingVoteResultSet: ${insertErr.message}`);
        }
      }, p));
    });
  } catch (err) {
    logger.error(`Error pendingVoteResultSet ResultSetsDB.find: ${err.message}`);
  }
};

module.exports = {
  syncVoteResultSet,
  pendingVoteResultSet,
};
