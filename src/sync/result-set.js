const { each, isNull, find } = require('lodash');
const updateEvent = require('./update-event');
const web3 = require('../web3');
const { TX_STATUS } = require('../constants');
const { toLowerCase } = require('../utils');
const logger = require('../utils/logger');
const { getTransactionReceipt } = require('../utils/web3-utils');
const TxReceiptsDB = require('../db/controllers/tx-receipts');
const EventsDB = require('../db/controllers/events');
const ResultSetsDB = require('../db/controllers/result-sets');
const EventLeaderboardUsersDB = require('../db/controllers/event-leaderboard-users');
const EventSig = require('../config/event-sig');
const parseResultSet = require('./parsers/result-set');
const EventLeaderboardUser = require('../models/event-leaderboard-user');
const insertName = require('./insert-name');

const parseAndUpdate = async (log, txReceipt) => {
  const resultSet = parseResultSet({ log });

  // Stop parsing if result set exists
  const existing = await ResultSetsDB.findOne({ txid: resultSet.txid });
  if (existing && existing.txStatus !== TX_STATUS.PENDING) {
    return;
  }

  // Stop parsing if event does not exist (previous version)
  const event = await EventsDB.findOne({ address: resultSet.eventAddress });
  if (isNull(event)) return;

  // Insert new result set
  resultSet.eventType = event.eventType;
  await ResultSetsDB.updateOne(resultSet);

  // Fetch tx receipt if needed before update
  if (txReceipt) await TxReceiptsDB.updateOne(txReceipt);
  else await TxReceiptsDB.updateOne(await getTransactionReceipt(resultSet.txid));

  // Update event's round, index, and threshold
  await updateEvent(resultSet);

  // Update event leaderboard investments
  const leaderboardUser = new EventLeaderboardUser({
    eventAddress: resultSet.eventAddress,
    userAddress: resultSet.centralizedOracleAddress,
    investments: resultSet.amount,
    winnings: '0',
  });
  const leaderboardCount = await EventLeaderboardUsersDB.count({
    userAddress: leaderboardUser.userAddress,
    eventAddress: leaderboardUser.eventAddress,
  });
  if (leaderboardCount === 0) {
    await EventLeaderboardUsersDB.updateOne(leaderboardUser);
  } else {
    await EventLeaderboardUsersDB.addToInvestments(leaderboardUser);
  }

  // Query and insert name
  await insertName(resultSet.centralizedOracleAddress);
};

const syncResultSet = async ({ startBlock, endBlock, syncPromises, limit }) => {
  try {
    // Fetch logs
    const logs = await web3.eth.getPastLogs({
      fromBlock: startBlock,
      toBlock: endBlock,
      topics: [EventSig.ResultSet],
    });
    if (logs.length === 0) return;

    // Add to syncPromises array to be executed in parallel
    logger.info(`Found ${logs.length} ResultSet`);
    each(logs, (log) => {
      syncPromises.push(limit(async (logObj) => {
        try {
          await parseAndUpdate(logObj);
        } catch (insertErr) {
          logger.error('Error syncResultSet parse');
          throw insertErr;
        }
      }, log));
    });
  } catch (err) {
    logger.error('Error syncResultSet');
    throw err;
  }
};

const pendingResultSet = async ({ syncPromises, limit }) => {
  try {
    const pending = await ResultSetsDB.find({
      txStatus: TX_STATUS.PENDING,
      eventRound: 0,
    });
    if (pending.length === 0) return;
    logger.info(`Checking ${pending.length} pending ResultSet`);

    each(pending, (p) => {
      syncPromises.push(limit(async (pendingSet) => {
        try {
          const txReceipt = await getTransactionReceipt(pendingSet.txid);
          if (isNull(txReceipt)) return;

          if (txReceipt.status) {
            // Parse individual log with success status
            const logs = await web3.eth.getPastLogs({
              fromBlock: txReceipt.blockNum,
              toBlock: txReceipt.blockNum,
              topics: [EventSig.ResultSet],
            });
            const foundLog = find(
              logs,
              log => toLowerCase(log.transactionHash) === txReceipt.transactionHash,
            );
            if (foundLog) await parseAndUpdate(foundLog, txReceipt);
          } else {
            // Update result set with failed status
            await ResultSetsDB.updateOne({
              txid: txReceipt.transactionHash,
              txStatus: TX_STATUS.FAIL,
            });
            await TxReceiptsDB.updateOne(txReceipt);
          }
        } catch (insertErr) {
          logger.error(`Error pendingResultSet: ${insertErr.message}`);
        }
      }, p));
    });
  } catch (err) {
    logger.error(`Error pendingResultSet ResultSetsDB.find: ${err.message}`);
  }
};

module.exports = {
  syncResultSet,
  pendingResultSet,
};
