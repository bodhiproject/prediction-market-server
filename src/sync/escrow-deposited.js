const { each, isNull, find } = require('lodash');
const web3 = require('../web3');
const { TX_STATUS, EVENT_TYPE } = require('../constants');
const { toLowerCase } = require('../utils');
const logger = require('../utils/logger');
const { getTransactionReceipt } = require('../utils/web3-utils');
const TxReceiptsDB = require('../db/controllers/tx-receipts');
const DepositsDB = require('../db/controllers/deposits');
const EventsDB = require('../db/controllers/events');
const EventSig = require('../config/event-sig');
const parseDeposit = require('./parsers/deposit');
const EventAPI = require('../api/ab-event');

const parseAndUpdate = async (log, txReceipt) => {
  const deposit = parseDeposit({ log });

  // Stop parsing if deposit exists
  const existing = await DepositsDB.findOne({ txid: deposit.txid });
  if (existing && existing.txStatus !== TX_STATUS.PENDING) {
    return;
  }

  // Stop parsing if event does not exist (previous version)
  // or if the event is an MREvent.
  const event = await EventsDB.findOne({ address: deposit.eventAddress });
  if (isNull(event) || event.eventType === EVENT_TYPE.MR_EVENT) return;

  // Insert new deposit
  deposit.eventType = event.eventType;
  await DepositsDB.updateOne(deposit);

  // Fetch tx receipt if needed before update
  if (txReceipt) await TxReceiptsDB.updateOne(txReceipt);
  else await TxReceiptsDB.updateOne(await getTransactionReceipt(deposit.txid));

  await EventsDB.addToEscrowAmount(deposit);

  if (event.eventType === EVENT_TYPE.AB_EVENT) {
    // Only update maxBets for ABEvents
    const maxBets = await EventAPI.maxBets({ eventAddress: deposit.eventAddress });
    await EventsDB.updateOneByAddress(deposit.eventAddress, { maxBets });
  }
};

const syncEscrowDeposited = async ({ startBlock, endBlock, syncPromises, limit }) => {
  try {
    // Fetch logs
    const logs = await web3.eth.getPastLogs({
      fromBlock: startBlock,
      toBlock: endBlock,
      topics: [EventSig.EscrowDeposited],
    });
    if (logs.length === 0) return;

    // Add to syncPromises array to be executed in parallel
    logger.info(`Found ${logs.length} EscrowDeposited`);
    each(logs, (log) => {
      syncPromises.push(limit(async (logObj) => {
        try {
          await parseAndUpdate(logObj);
        } catch (insertErr) {
          logger.error('Error syncEscrowDeposited parse');
          throw insertErr;
        }
      }, log));
    });
  } catch (err) {
    logger.error('Error syncEscrowDeposited');
    throw err;
  }
};

const pendingEscrowDeposited = async ({ syncPromises, limit }) => {
  try {
    const pending = await DepositsDB.find({
      txStatus: TX_STATUS.PENDING,
      eventRound: 0,
    });
    if (pending.length === 0) return;
    logger.info(`Checking ${pending.length} pending EscrowDeposited`);

    each(pending, (p) => {
      syncPromises.push(limit(async (pendingDeposit) => {
        try {
          const txReceipt = await getTransactionReceipt(pendingDeposit.txid);
          if (isNull(txReceipt)) return;

          if (txReceipt.status) {
            // Parse individual log with success status
            const logs = await web3.eth.getPastLogs({
              fromBlock: txReceipt.blockNum,
              toBlock: txReceipt.blockNum,
              topics: [EventSig.EscrowDeposited],
            });
            const foundLog = find(
              logs,
              log => toLowerCase(log.transactionHash) === txReceipt.transactionHash,
            );
            if (foundLog) await parseAndUpdate(foundLog, txReceipt);
          } else {
            // Update deposit with failed status
            await DepositsDB.updateOne({
              txid: txReceipt.transactionHash,
              txStatus: TX_STATUS.FAIL,
            });
            await TxReceiptsDB.updateOne(txReceipt);
          }
        } catch (insertErr) {
          logger.error(`Error pendingEscrowDeposited: ${insertErr.message}`);
        }
      }, p));
    });
  } catch (err) {
    logger.error(`Error pendingEscrowDeposited DepositsDB.find: ${err.message}`);
  }
};

module.exports = {
  syncEscrowDeposited,
  pendingEscrowDeposited,
};
