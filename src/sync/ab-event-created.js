const { each, isNull, find } = require('lodash');
const web3 = require('../web3');
const { TX_STATUS, EVENT_TYPE } = require('../constants');
const EventSig = require('../config/event-sig');
const { toLowerCase } = require('../utils');
const { getTransactionReceipt } = require('../utils/web3-utils');
const logger = require('../utils/logger');
const TxReceiptsDB = require('../db/controllers/tx-receipts');
const EventsDB = require('../db/controllers/events');
const parseABEvent = require('./parsers/ab-event');
const insertName = require('./insert-name');

const parseAndUpdate = async (log, txReceipt) => {
  const event = await parseABEvent({ log });

  // Stop parsing if no matching contract found
  if (isNull(event)) return;

  // Stop parsing if event exists
  const existing = await EventsDB.findOne({ txid: event.txid });
  if (existing && existing.txStatus !== TX_STATUS.PENDING) {
    return;
  }

  // Insert new event
  await EventsDB.updateOne(event);

  // Fetch tx receipt if needed before update
  if (txReceipt) await TxReceiptsDB.updateOne(txReceipt);
  else await TxReceiptsDB.updateOne(await getTransactionReceipt(event.txid));

  // Query and insert names
  await insertName(event.ownerAddress);
};

const syncABEventCreated = async (
  { startBlock, endBlock, syncPromises, limit },
) => {
  try {
    // Fetch logs
    const logs = await web3.eth.getPastLogs({
      fromBlock: startBlock,
      toBlock: endBlock,
      topics: [EventSig.ABEventCreated],
    });
    if (logs.length === 0) return;

    // Add to syncPromises array to be executed in parallel
    logger.info(`Found ${logs.length} ABEventCreated`);
    each(logs, (log) => {
      syncPromises.push(limit(async (logObj) => {
        try {
          await parseAndUpdate(logObj);
        } catch (insertErr) {
          logger.error('Error syncABEventCreated parse');
          throw insertErr;
        }
      }, log));
    });
  } catch (err) {
    logger.error('Error syncABEventCreated');
    throw err;
  }
};

const pendingABEventCreated = async ({ syncPromises, limit }) => {
  try {
    const pending = await EventsDB.find({
      txStatus: TX_STATUS.PENDING,
      eventType: EVENT_TYPE.AB_EVENT,
    });
    if (pending.length === 0) return;
    logger.info(`Checking ${pending.length} pending ABEventCreated`);

    each(pending, (p) => {
      syncPromises.push(limit(async (pendingEvent) => {
        try {
          const txReceipt = await getTransactionReceipt(pendingEvent.txid);
          if (isNull(txReceipt)) return;

          if (txReceipt.status) {
            // Parse individual log with success status
            const logs = await web3.eth.getPastLogs({
              fromBlock: txReceipt.blockNum,
              toBlock: txReceipt.blockNum,
              topics: [EventSig.ABEventCreated],
            });
            const foundLog = find(
              logs,
              log => toLowerCase(log.transactionHash) === txReceipt.transactionHash,
            );
            if (foundLog) await parseAndUpdate(foundLog, txReceipt);
          } else {
            // Update event with failed status
            await EventsDB.updateOne({
              txid: txReceipt.transactionHash,
              txStatus: TX_STATUS.FAIL,
            });
            await TxReceiptsDB.updateOne(txReceipt);
          }
        } catch (insertErr) {
          logger.error('Error pendingABEventCreated');
          throw insertErr;
        }
      }, p));
    });
  } catch (err) {
    logger.error(`Error pendingABEventCreated EventsDB.find: ${err.message}`);
  }
};

module.exports = {
  syncABEventCreated,
  pendingABEventCreated,
};
