const pLimit = require('p-limit');
const moment = require('moment');
const { isNull, isNumber } = require('lodash');
const {
  getClassicMinContractVer,
  getHouseMinContractVer,
  getRewardMinContractVer,
  mrEventFactoryMeta,
  abEventFactoryMeta,
  rewardEventFactoryMeta,
  isMainnet,
} = require('../config');
const { EVENT_MESSAGE } = require('../constants');
const web3 = require('../web3');
const {
  syncMultipleResultsEventCreated,
  pendingMultipleResultsEventCreated,
} = require('./multiple-results-event-created');
const {
  syncABEventCreated,
  pendingABEventCreated,
} = require('./ab-event-created');
const {
  syncRewardEventCreated,
  pendingRewardEventCreated,
} = require('./reward-event-created');
const { syncBetPlaced, pendingBetPlaced } = require('./bet-placed');
const { syncEscrowDeposited, pendingEscrowDeposited } = require('./escrow-deposited');
const { syncResultSet, pendingResultSet } = require('./result-set');
const { syncVotePlaced, pendingVotePlaced } = require('./vote-placed');
const { syncVoteResultSet, pendingVoteResultSet } = require('./vote-result-set');
const {
  syncWinningsWithdrawn,
  pendingWinningsWithdrawn,
} = require('./winnings-withdrawn');
const syncBlocks = require('./blocks');
const updateLeaderboardUsers = require('./update-leaderboard');
const BlocksDB = require('../db/controllers/blocks');
const EventsDB = require('../db/controllers/events');
const logger = require('../utils/logger');
const { publishSyncInfo } = require('../graphql/subscriptions');
const emitter = require('../utils/emitter');

const SYNC_START_DELAY = 3000;
const BLOCK_BATCH_COUNT = 500;
const PROMISE_CONCURRENCY_LIMIT = 15;

const limit = pLimit(PROMISE_CONCURRENCY_LIMIT);
let dbConnected = false;
let web3Connected = false;
let syncPromises = [];
let startBlock;

/**
 * Event listener for DB connected.
 */
const onDBConnected = () => {
  dbConnected = true;
};

/**
 * Event listener for DB disconnected.
 */
const onDBDisconnected = () => {
  dbConnected = false;
};

/**
 * Event listener for websocket connected.
 */
const onWeb3Connected = () => {
  web3Connected = true;
};

/**
 * Event listener for websocket disconnected.
 */
const onWeb3Disconnected = () => {
  web3Connected = false;
};

/**
 * Initial setup before starting the sync.
 */
const initSync = () => {
  emitter.addListener(EVENT_MESSAGE.DB_CONNECTED, onDBConnected);
  emitter.addListener(EVENT_MESSAGE.DB_DISCONNECTED, onDBDisconnected);
  emitter.addListener(EVENT_MESSAGE.WEB3_CONNECTED, onWeb3Connected);
  emitter.addListener(EVENT_MESSAGE.WEB3_DISCONNECTED, onWeb3Disconnected);
};

/**
 * Determines the start block to start syncing from.
 */
const getStartBlock = async () => {
  let start;

  const latest = await BlocksDB.findLatestBlock();
  if (!isNull(latest)) {
    // Blocks found in DB. Use the highest block num minus the block batch count.
    // We need to reparse the previously parsed blocks because the blocks are added
    // async and there may be blocks missing in the middle.
    start = isNumber(latest.blockNum)
      ? Math.max(0, latest.blockNum - BLOCK_BATCH_COUNT)
      : 0;
  } else {
    // No blocks found in DB
    // Get MREventFactory start block
    const mrFactoryMeta = mrEventFactoryMeta(getClassicMinContractVer());
    const mrFactoryStart = isMainnet()
      ? mrFactoryMeta.mainnetDeployBlock
      : mrFactoryMeta.testnetDeployBlock;

    // Get ABEventFactory start block
    const abFactoryMeta = abEventFactoryMeta(getHouseMinContractVer());
    const abFactoryStart = isMainnet()
      ? abFactoryMeta.mainnetDeployBlock
      : abFactoryMeta.testnetDeployBlock;

    // Get RewardEventFactory start block
    const rewardFactoryMeta = rewardEventFactoryMeta(getRewardMinContractVer());
    const rewardFactoryStart = isMainnet()
      ? rewardFactoryMeta.mainnetDeployBlock
      : rewardFactoryMeta.testnetDeployBlock;

    // Use lowest of the all start blocks
    start = Math.min(mrFactoryStart, abFactoryStart, rewardFactoryStart);
  }
  return start;
};

/**
 * Delays for the specified time then calls startSync.
 * @param {number} delay Number of milliseconds to delay.
 */
const delayThenSync = (delay) => {
  setTimeout(() => {
    startSync();
  }, delay);
};

/**
 * Starts the sync logic. It will loop indefinitely until cancelled.
 */
const startSync = async () => {
  try {
    // Delay sync if certain conditions are met
    if (!dbConnected || !web3Connected) {
      delayThenSync(SYNC_START_DELAY);
      return;
    }

    // Track exec time
    const execStartMs = moment().valueOf();

    // Determine start and end blocks
    const latestBlock = await web3.eth.getBlockNumber();
    startBlock = startBlock || await getStartBlock();
    const endBlock = Math.min(startBlock + BLOCK_BATCH_COUNT, latestBlock);

    logger.info(`Syncing blocks ${startBlock} - ${endBlock}`);

    // Events need to be synced before all other types to avoid race conditions
    // when updating the event.
    syncPromises = [];
    await syncMultipleResultsEventCreated({
      startBlock,
      endBlock,
      syncPromises,
      limit,
    });
    await syncABEventCreated({
      startBlock,
      endBlock,
      syncPromises,
      limit,
    });
    await syncRewardEventCreated({
      startBlock,
      endBlock,
      syncPromises,
      limit,
    });
    await Promise.all(syncPromises);
    syncPromises = [];
    await pendingMultipleResultsEventCreated({ syncPromises, limit });
    await pendingABEventCreated({ syncPromises, limit });
    await pendingRewardEventCreated({ syncPromises, limit });
    await Promise.all(syncPromises);

    // Handle event actions and blocks
    syncPromises = [];
    await syncBetPlaced({ startBlock, endBlock, syncPromises, limit });
    await syncEscrowDeposited({ startBlock, endBlock, syncPromises, limit });
    await syncResultSet({ startBlock, endBlock, syncPromises, limit });
    await syncVotePlaced({ startBlock, endBlock, syncPromises, limit });
    await syncVoteResultSet({ startBlock, endBlock, syncPromises, limit });
    await syncWinningsWithdrawn({ startBlock, endBlock, syncPromises, limit });
    syncBlocks({ startBlock, endBlock, syncPromises, limit });
    await Promise.all(syncPromises);

    // Handle pending event actions
    syncPromises = [];
    await pendingBetPlaced({ syncPromises, limit });
    await pendingEscrowDeposited({ syncPromises, limit });
    await pendingResultSet({ syncPromises, limit });
    await pendingVotePlaced({ syncPromises, limit });
    await pendingVoteResultSet({ syncPromises, limit });
    await pendingWinningsWithdrawn({ syncPromises, limit });
    await Promise.all(syncPromises);

    // Update statuses
    const { blockTime } = await BlocksDB.findOne({ blockNum: endBlock });
    await EventsDB.updateEventStatusPreBetting(blockTime);
    await EventsDB.updateEventStatusBetting(blockTime);
    await EventsDB.updateEventStatusPreResultSetting(blockTime);
    await EventsDB.updateEventStatusOracleResultSetting(blockTime);
    await EventsDB.updateEventStatusOpenResultSetting(blockTime);
    await EventsDB.updateEventStatusArbitration(blockTime);
    const newWithdrawEvents = await EventsDB.updateEventStatusWithdrawing(blockTime);

    // Post-sync updates
    syncPromises = [];
    await updateLeaderboardUsers({ newWithdrawEvents, syncPromises, limit });
    await Promise.all(syncPromises);

    // Send syncInfo subscription message
    await publishSyncInfo(endBlock, blockTime);

    // Display exec time
    const execTimeMs = moment().valueOf() - execStartMs;
    logger.info(`Completed in ${execTimeMs} ms`);

    // Set startBlock for next sync
    startBlock = endBlock + 1;

    delayThenSync(SYNC_START_DELAY);
  } catch (err) {
    throw err;
  }
};

module.exports = {
  initSync,
  startSync,
};
