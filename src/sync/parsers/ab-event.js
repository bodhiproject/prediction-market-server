const { map, isNull } = require('lodash');
const web3 = require('../../web3');
const Event = require('../../models/event');
const { EVENT_TYPE, TX_STATUS } = require('../../constants');
const { getEventMeta } = require('../../config');
const { getContract } = require('../../utils/web3-utils');

const { toBN } = web3.utils;

module.exports = async ({ log }) => {
  const address = web3.eth.abi.decodeParameter('address', log.topics[1]);
  const ownerAddress = web3.eth.abi.decodeParameter('address', log.topics[2]);
  const blockNum = Number(log.blockNumber);

  const eventMeta = getEventMeta(log.topics[0], blockNum);
  if (isNull(eventMeta)) return null; // null if no matching contract found

  const contract = getContract(eventMeta.abi, address);
  let res = await contract.methods.eventMetadata().call();
  const version = res['0'];
  const eventName = res['1'];
  const eventResults = res['2'];
  const numOfResults = res['3'];
  const winningChances = res['4'];
  const odds = res['5'];

  res = await contract.methods.centralizedMetadata().call();
  const centralizedOracle = res['0'];
  const betStartTime = res['1'];
  const betEndTime = res['2'];
  const resultSetStartTime = res['3'];
  const resultSetEndTime = res['4'];

  res = await contract.methods.configMetadata().call();
  const escrowAmount = 0; // EscrowDeposited event will increment escrow
  const arbitrationLength = res['1'];
  const thresholdPercentIncrease = res['2'];
  const arbitrationRewardPercentage = res['3'];

  const consensusThreshold =
    await contract.methods.currentConsensusThreshold().call();
  const arbitrationEndTime =
    await contract.methods.currentArbitrationEndTime().call();

  const maxBets = await contract.methods.maxBets().call();

  return new Event({
    txid: log.transactionHash,
    txStatus: TX_STATUS.SUCCESS,
    blockNum,
    address,
    ownerAddress,
    version: toBN(version).toNumber(),
    name: eventName,
    results: eventResults,
    numOfResults: toBN(numOfResults).toNumber(),
    winningChances: map(winningChances, item => toBN(item).toNumber()),
    // contract odds are in whole percentages, e.g. 120% = 120
    odds: map(odds, item => toBN(item).toNumber() / 100),
    centralizedOracle,
    betStartTime: toBN(betStartTime).toNumber(),
    betEndTime: toBN(betEndTime).toNumber(),
    resultSetStartTime: toBN(resultSetStartTime).toNumber(),
    resultSetEndTime: toBN(resultSetEndTime).toNumber(),
    escrowAmount: toBN(escrowAmount).toString(10),
    arbitrationLength: toBN(arbitrationLength).toNumber(),
    thresholdPercentIncrease: toBN(thresholdPercentIncrease).toNumber(),
    arbitrationRewardPercentage: toBN(arbitrationRewardPercentage).toNumber(),
    consensusThreshold: toBN(consensusThreshold).toString(10),
    arbitrationEndTime: toBN(arbitrationEndTime).toNumber(),
    maxBets: map(maxBets, item => toBN(item).toString(10)),
    eventType: EVENT_TYPE.AB_EVENT,
  });
};
