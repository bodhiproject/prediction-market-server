const web3 = require('../../web3');
const Deposit = require('../../models/deposit');
const { TX_STATUS } = require('../../constants');

module.exports = ({ log }) => {
  const eventAddress = web3.eth.abi.decodeParameter('address', log.topics[1]);
  const depositorAddress = web3.eth.abi.decodeParameter('address', log.topics[2]);
  const decodedData = web3.eth.abi.decodeParameters(
    ['uint256'],
    log.data,
  );
  const amount = decodedData['0'];

  return new Deposit({
    txid: log.transactionHash,
    txStatus: TX_STATUS.SUCCESS,
    blockNum: Number(log.blockNumber),
    eventAddress,
    depositorAddress,
    amount: amount.toString(10),
  });
};
