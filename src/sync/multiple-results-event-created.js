const { each, isNull, find } = require('lodash');
const web3 = require('../web3');
const { TX_STATUS, EVENT_TYPE } = require('../constants');
const EventSig = require('../config/event-sig');
const { toLowerCase } = require('../utils');
const { getTransactionReceipt } = require('../utils/web3-utils');
const logger = require('../utils/logger');
const TxReceiptsDB = require('../db/controllers/tx-receipts');
const EventsDB = require('../db/controllers/events');
const parseMREvent = require('./parsers/mr-event');
const insertName = require('./insert-name');

const parseAndUpdate = async (log, txReceipt) => {
  const event = await parseMREvent({ log });

  // Stop parsing if no matching contract found
  if (isNull(event)) return;

  // Stop parsing if event exists
  const existing = await EventsDB.findOne({ txid: event.txid });
  if (existing && existing.txStatus !== TX_STATUS.PENDING) {
    return;
  }

  // Insert new event
  await EventsDB.updateOne(event);

  // Fetch tx receipt if needed before update
  if (txReceipt) await TxReceiptsDB.updateOne(txReceipt);
  else await TxReceiptsDB.updateOne(await getTransactionReceipt(event.txid));

  // Query and insert names
  await insertName(event.ownerAddress);
};

const syncMultipleResultsEventCreated = async (
  { startBlock, endBlock, syncPromises, limit },
) => {
  try {
    // Fetch logs
    const logs = await web3.eth.getPastLogs({
      fromBlock: startBlock,
      toBlock: endBlock,
      topics: [EventSig.MultipleResultsEventCreated],
    });
    if (logs.length === 0) return;

    // Add to syncPromises array to be executed in parallel
    logger.info(`Found ${logs.length} MultipleResultsEventCreated`);
    each(logs, (log) => {
      syncPromises.push(limit(async (logObj) => {
        try {
          await parseAndUpdate(logObj);
        } catch (insertErr) {
          logger.error('Error syncMultipleResultsEventCreated parse');
          throw insertErr;
        }
      }, log));
    });
  } catch (err) {
    logger.error('Error syncMultipleResultsEventCreated');
    throw err;
  }
};

const pendingMultipleResultsEventCreated = async ({ syncPromises, limit }) => {
  try {
    const pending = await EventsDB.find({
      txStatus: TX_STATUS.PENDING,
      eventType: EVENT_TYPE.MR_EVENT,
    });
    if (pending.length === 0) return;
    logger.info(`Checking ${pending.length} pending MultipleResultsEventCreated`);

    each(pending, (p) => {
      syncPromises.push(limit(async (pendingEvent) => {
        try {
          const txReceipt = await getTransactionReceipt(pendingEvent.txid);
          if (isNull(txReceipt)) return;

          if (txReceipt.status) {
            // Parse individual log with success status
            const logs = await web3.eth.getPastLogs({
              fromBlock: txReceipt.blockNum,
              toBlock: txReceipt.blockNum,
              topics: [EventSig.MultipleResultsEventCreated],
            });
            const foundLog = find(
              logs,
              log => toLowerCase(log.transactionHash) === txReceipt.transactionHash,
            );
            if (foundLog) await parseAndUpdate(foundLog, txReceipt);
          } else {
            // Update event with failed status
            await EventsDB.updateOne({
              txid: txReceipt.transactionHash,
              txStatus: TX_STATUS.FAIL,
            });
            await TxReceiptsDB.updateOne(txReceipt);
          }
        } catch (insertErr) {
          logger.error('Error pendingMultipleResultsEventCreated');
          throw insertErr;
        }
      }, p));
    });
  } catch (err) {
    logger.error(`Error pendingMultipleResultsEventCreated EventsDB.find: ${err.message}`);
  }
};

module.exports = {
  syncMultipleResultsEventCreated,
  pendingMultipleResultsEventCreated,
};
