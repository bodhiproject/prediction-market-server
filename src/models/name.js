const { isString } = require('lodash');
const { toLowerCase } = require('../utils');

module.exports = class Name {
  constructor(params) {
    this.validate(params);
    this.format(params);
  }

  validate(params) {
    if (!isString(params.address)) throw Error('address must be a string');
    if (!isString(params.name)) throw Error('name must be a string');
  }

  format(params) {
    this.address = toLowerCase(params.address);
    this.name = toLowerCase(params.name);
  }
};
