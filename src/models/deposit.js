const { isString } = require('lodash');
const { TX_TYPE } = require('../constants');
const { toLowerCase } = require('../utils');

module.exports = class Deposit {
  constructor(params) {
    this.validate(params);
    this.format(params);
  }

  validate(params) {
    if (!isString(params.txid)) throw Error('txid must be a String');
    if (!isString(params.txStatus)) throw Error('txStatus must be a String');
    if (!isString(params.eventAddress)) throw Error('eventAddress must be a String');
    if (!isString(params.depositorAddress)) throw Error('depositorAddress must be a String');
    if (!isString(params.amount)) throw Error('amount must be a String');
  }

  format(params) {
    // Chain params
    this.txType = TX_TYPE.DEPOSIT;
    this.txid = toLowerCase(params.txid);
    this.txStatus = params.txStatus;
    this.blockNum = params.blockNum || 0;

    // Deposit params
    this.eventAddress = toLowerCase(params.eventAddress);
    this.depositorAddress = toLowerCase(params.depositorAddress);
    this.amount = params.amount.toString(10);
    this.eventType = params.eventType;
  }
};
