const { isUndefined, isFinite, isString, map, filter } = require('lodash');
const { TX_TYPE, INVALID_RESULT_INDEX, EVENT_STATUS, EVENT_TYPE } = require('../constants');
const { toLowerCase } = require('../utils');
const web3 = require('../web3');

module.exports = class Event {
  constructor(params) {
    this.validate(params);
    this.format(params);
  }

  validate(params) {
    if (!isString(params.txid)) throw Error('txid must be a string');
    if (!isString(params.txStatus)) throw Error('txStatus must be a string');
    if (!isString(params.ownerAddress)) {
      throw Error('ownerAddress must be a string');
    }
    if (!isString(params.name)) throw Error('name must be a string');
    if (!isFinite(params.numOfResults)) {
      throw Error('numOfResults must be a number');
    }
    if (!isString(params.centralizedOracle)) {
      throw Error('centralizedOracle must be a string');
    }
    if (!isUndefined(params.betStartTime) && !isFinite(params.betStartTime)) {
      throw Error('betStartTime must be a number');
    }
    if (!isFinite(params.betEndTime)) throw Error('betEndTime must be a number');
    if (!isFinite(params.resultSetStartTime)) {
      throw Error('resultSetStartTime must be a number');
    }
    if (!isUndefined(params.resultSetEndTime) && !isFinite(params.resultSetEndTime)) {
      throw Error('resultSetEndTime must be a number');
    }
    if (!isString(params.eventType)) throw Error('eventType must be a string');
    if (![EVENT_TYPE.MR_EVENT,
      EVENT_TYPE.AB_EVENT,
      EVENT_TYPE.REWARD_EVENT].includes(params.eventType)) {
      throw Error('Invalid eventType');
    }
  }

  format(params) {
    // Chain params
    this.txType = TX_TYPE.CREATE_EVENT;
    this.txid = toLowerCase(params.txid);
    this.txStatus = params.txStatus;
    this.blockNum = params.blockNum || 0;

    // Event params
    this.address = params.address ? toLowerCase(params.address) : '';
    this.ownerAddress = toLowerCase(params.ownerAddress);
    this.version = params.version || 0;
    this.name = params.name;
    this.results = filter(
      map(params.results, (value, index) => {
        try {
          return web3.utils.hexToUtf8(value);
        } catch (err) {
          // UI was passing Chinese chars converted fromAscii so the hex is
          // invalid. This will change the result names so it doesn't break
          // the sync.
          return `#${index} parse error`;
        }
      }),
      item => !!item,
    );
    this.numOfResults = params.numOfResults;
    this.winningChances = params.winningChances || Array(params.numOfResults).fill(0);
    this.odds = params.odds || Array(params.numOfResults).fill(0);
    this.centralizedOracle = toLowerCase(params.centralizedOracle);
    this.betStartTime = params.betStartTime || 0;
    this.betEndTime = params.betEndTime;
    this.resultSetStartTime = params.resultSetStartTime;
    this.resultSetEndTime = params.resultSetEndTime || 0;
    this.escrowAmount = params.escrowAmount || '0';
    this.arbitrationLength = params.arbitrationLength || 0;
    this.arbitrationRewardPercentage = params.arbitrationRewardPercentage || 0;
    this.thresholdPercentIncrease = params.thresholdPercentIncrease || 0;
    this.currentRound = 0;
    this.currentResultIndex = INVALID_RESULT_INDEX;
    this.consensusThreshold = params.consensusThreshold || '0';
    this.arbitrationEndTime = params.arbitrationEndTime || 0;

    // Backend params
    this.eventType = params.eventType;
    this.status = params.status || EVENT_STATUS.PRE_BETTING;
    this.language = params.language || 'zh-Hans-CN';
    this.betRoundResultsAmount = Array(params.numOfResults).fill('0');
    this.maxBets = params.maxBets ? params.maxBets : Array(params.numOfResults).fill('0');
    this.withdrawnList = [];
  }
};
