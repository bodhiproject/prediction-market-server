const { isString } = require('lodash');
const { TX_TYPE } = require('../constants');
const { toLowerCase } = require('../utils');

module.exports = class Withdraw {
  constructor(params) {
    this.validate(params);
    this.format(params);
  }

  validate(params) {
    if (!isString(params.txid)) throw Error('txid must be a String');
    if (!isString(params.txStatus)) throw Error('txStatus must be a String');
    if (!isString(params.eventAddress)) throw Error('eventAddress must be a String');
    if (!isString(params.winnerAddress)) throw Error('winnerAddress must be a String');
    if (!isString(params.winningAmount)) throw Error('winningAmount must be a String');
    if (!isString(params.creatorReturnAmount)) throw Error('creatorReturnAmount must be a String');
  }

  format(params) {
    // Chain params
    this.txType = TX_TYPE.WITHDRAW;
    this.txid = toLowerCase(params.txid);
    this.txStatus = params.txStatus;
    this.blockNum = params.blockNum || 0;

    // Withdraw params
    this.eventAddress = toLowerCase(params.eventAddress);
    this.winnerAddress = toLowerCase(params.winnerAddress);
    this.winningAmount = params.winningAmount.toString(10);
    this.creatorReturnAmount = params.creatorReturnAmount.toString(10);
    this.eventType = params.eventType;
  }
};
