const { isFinite } = require('lodash');

module.exports = class Block {
  constructor(params) {
    this.validate(params);
    this.format(params);
  }

  validate(params) {
    if (!isFinite(params.blockNum)) throw Error('blockNum must be a number');
    if (!isFinite(params.blockTime)) throw Error('blockTime must be a number');
  }

  format(params) {
    this.blockNum = params.blockNum;
    this.blockTime = params.blockTime;
  }
};
