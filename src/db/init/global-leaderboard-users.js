const logger = require('../../utils/logger');
const { checkCollectionExists } = require('./utils');
const globalLeaderboardUsersValidator = require('../validators/global-leaderboard-users');

module.exports = async (db) => {
  if (await checkCollectionExists(db, 'globalLeaderboardUsers')) {
    logger.info('Existing collection found: globalLeaderboardUsers');
    return;
  }

  logger.info('Creating collection: globalLeaderboardUsers');
  await db.createCollection(
    'globalLeaderboardUsers',
    { validator: { ...globalLeaderboardUsersValidator } },
  );
  await db.createIndex('globalLeaderboardUsers', { userAddress: 1 }, { unique: true });
};
