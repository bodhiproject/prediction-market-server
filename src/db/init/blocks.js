const logger = require('../../utils/logger');
const { checkCollectionExists } = require('./utils');
const blocksValidator = require('../validators/blocks');

module.exports = async (db) => {
  if (await checkCollectionExists(db, 'blocks')) {
    logger.info('Existing collection found: blocks');
    return;
  }

  logger.info('Creating collection: blocks');
  await db.createCollection('blocks', { validator: { ...blocksValidator } });
  await db.createIndex('blocks', { blockNum: 1 }, { unique: true });
};
