const logger = require('../../utils/logger');
const { checkCollectionExists } = require('./utils');
const betsValidator = require('../validators/bets');

module.exports = async (db) => {
  if (await checkCollectionExists(db, 'bets')) {
    logger.info('Existing collection found: bets');
    return;
  }

  logger.info('Creating collection: bets');
  await db.createCollection('bets', { validator: { ...betsValidator } });
  await db.createIndex('bets', { txid: 1 }, { unique: true });
  await db.createIndex('bets', { eventAddress: 1 });
  await db.createIndex('bets', { betterAddress: 1 });
  await db.createIndex('bets', { txStatus: 1, eventRound: 1 });
  await db.createIndex('bets', { eventAddress: 1, betterAddress: 1 });
};
