const logger = require('../../utils/logger');
const { checkCollectionExists } = require('./utils');
const eventsValidator = require('../validators/events');

module.exports = async (db) => {
  if (await checkCollectionExists(db, 'events')) {
    logger.info('Existing collection found: events');
    return;
  }

  logger.info('Creating collection: events');
  await db.createCollection('events', { validator: { ...eventsValidator } });
  await db.createIndex('events', { txid: 1 }, { unique: true });
  await db.createIndex('events', { address: 1 }, { unique: true });
  await db.createIndex('events', { eventType: 1 });
};
