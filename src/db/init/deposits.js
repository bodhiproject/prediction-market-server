const logger = require('../../utils/logger');
const { checkCollectionExists } = require('./utils');
const depositsValidator = require('../validators/deposits');

module.exports = async (db) => {
  if (await checkCollectionExists(db, 'deposits')) {
    logger.info('Existing collection found: deposits');
    return;
  }

  logger.info('Creating collection: deposits');
  await db.createCollection('deposits', { validator: { ...depositsValidator } });
  await db.createIndex('deposits', { txid: 1 }, { unique: true });
  await db.createIndex('deposits', { txStatus: 1, eventRound: 1 });
};
