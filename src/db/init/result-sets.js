const logger = require('../../utils/logger');
const { checkCollectionExists } = require('./utils');
const resultSetsValidator = require('../validators/result-sets');

module.exports = async (db) => {
  if (await checkCollectionExists(db, 'resultSets')) {
    logger.info('Existing collection found: resultSets');
    return;
  }

  logger.info('Creating collection: resultSets');
  await db.createCollection('resultSets', { validator: { ...resultSetsValidator } });
  await db.createIndex('resultSets', { txid: 1 }, { unique: true });
  await db.createIndex('resultSets', { eventAddress: 1 });
  await db.createIndex('resultSets', { eventAddress: 1, eventRound: 1 });
  await db.createIndex('resultSets', { txStatus: 1, eventRound: 1 });
};
