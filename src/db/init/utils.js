const { find, isNil } = require('lodash');

const checkCollectionExists = async (db, dbName) => {
  const collections = await db.listCollections().toArray();
  return !isNil(find(collections, { type: 'collection', name: dbName }));
};

module.exports = { checkCollectionExists };
