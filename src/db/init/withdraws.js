const logger = require('../../utils/logger');
const { checkCollectionExists } = require('./utils');
const withdrawsValidator = require('../validators/withdraws');

module.exports = async (db) => {
  if (await checkCollectionExists(db, 'withdraws')) {
    logger.info('Existing collection found: withdraws');
    return;
  }

  logger.info('Creating collection: withdraws');
  await db.createCollection('withdraws', { validator: { ...withdrawsValidator } });
  await db.createIndex('withdraws', { txid: 1 }, { unique: true });
  await db.createIndex('withdraws', { txStatus: 1 });
};
