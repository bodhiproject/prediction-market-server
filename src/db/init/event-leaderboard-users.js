const logger = require('../../utils/logger');
const { checkCollectionExists } = require('./utils');
const eventLeaderboardUsersValidator = require('../validators/event-leaderboard-users');

module.exports = async (db) => {
  if (await checkCollectionExists(db, 'eventLeaderboardUsers')) {
    logger.info('Existing collection found: eventLeaderboardUsers');
    return;
  }

  logger.info('Creating collection: eventLeaderboardUsers');
  await db.createCollection(
    'eventLeaderboardUsers',
    { validator: { ...eventLeaderboardUsersValidator } },
  );
  await db.createIndex('eventLeaderboardUsers', { userAddress: 1, eventAddress: 1 });
};
