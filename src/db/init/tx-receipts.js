const logger = require('../../utils/logger');
const { checkCollectionExists } = require('./utils');
const txReceiptsValidator = require('../validators/tx-receipts');

module.exports = async (db) => {
  if (await checkCollectionExists(db, 'txReceipts')) {
    logger.info('Existing collection found: txReceipts');
    return;
  }

  logger.info('Creating collection: txReceipts');
  await db.createCollection('txReceipts', { validator: { ...txReceiptsValidator } });
  await db.createIndex('txReceipts', { transactionHash: 1 }, { unique: true });
};
