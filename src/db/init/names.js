const logger = require('../../utils/logger');
const { checkCollectionExists } = require('./utils');
const namesValidator = require('../validators/names');

module.exports = async (db) => {
  if (await checkCollectionExists(db, 'names')) {
    logger.info('Existing collection found: names');
    return;
  }

  logger.info('Creating collection: names');
  await db.createCollection('names', { validator: { ...namesValidator } });
  await db.createIndex('names', { address: 1 }, { unique: true });
};
