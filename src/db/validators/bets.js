/**
 * DO NOT MODIFY once in production.
 * Write a migration to modify.
 */
module.exports = {
  $jsonSchema: {
    bsonType: 'object',
    required: [
      'txType',
      'txid',
      'txStatus',
      'eventAddress',
      'betterAddress',
      'resultIndex',
      'amount',
      'eventRound',
    ],
    properties: {
      txType: {
        enum: [
          'CREATE_EVENT',
          'BET',
          'RESULT_SET',
          'VOTE',
          'WITHDRAW',
          'DEPOSIT',
        ],
      },
      txid: { bsonType: 'string' },
      txStatus: { enum: ['PENDING', 'SUCCESS', 'FAIL'] },
      blockNum: { bsonType: 'int' },
      eventAddress: { bsonType: 'string' },
      betterAddress: { bsonType: 'string' },
      resultIndex: { bsonType: 'int' },
      amount: { bsonType: 'long' },
      eventRound: { bsonType: 'int' },
    },
  },
};
