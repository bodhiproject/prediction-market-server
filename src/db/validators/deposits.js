/**
 * DO NOT MODIFY once in production.
 * Write a migration to modify.
 */
module.exports = {
  $jsonSchema: {
    bsonType: 'object',
    required: [
      'txType',
      'txid',
      'txStatus',
      'eventAddress',
      'depositorAddress',
      'amount',
    ],
    properties: {
      txType: {
        enum: [
          'CREATE_EVENT',
          'BET',
          'RESULT_SET',
          'VOTE',
          'WITHDRAW',
          'DEPOSIT',
        ],
      },
      txid: { bsonType: 'string' },
      txStatus: { enum: ['PENDING', 'SUCCESS', 'FAIL'] },
      blockNum: { bsonType: 'int' },
      eventAddress: { bsonType: 'string' },
      depositorAddress: { bsonType: 'string' },
      amount: { bsonType: 'long' },
    },
  },
};
