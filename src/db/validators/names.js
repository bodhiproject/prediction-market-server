/**
 * DO NOT MODIFY once in production.
 * Write a migration to modify.
 */
module.exports = {
  $jsonSchema: {
    bsonType: 'object',
    required: ['address', 'name'],
    properties: {
      address: { bsonType: 'string' },
      name: { bsonType: 'string' },
    },
  },
};
