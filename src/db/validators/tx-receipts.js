/**
 * DO NOT MODIFY once in production.
 * Write a migration to modify.
 */
module.exports = {
  $jsonSchema: {
    bsonType: 'object',
    required: ['transactionHash', 'from', 'cumulativeGasUsed', 'gasUsed'],
    properties: {
      status: { bsonType: 'bool' },
      blockHash: { bsonType: 'string' },
      blockNum: { bsonType: 'int' },
      transactionHash: { bsonType: 'string' },
      from: { bsonType: 'string' },
      to: { bsonType: 'string' },
      contractAddress: { bsonType: 'string' },
      cumulativeGasUsed: { bsonType: 'int' },
      gasUsed: { bsonType: 'int' },
      gasPrice: { bsonType: 'long' },
    },
  },
};
