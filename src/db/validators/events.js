/**
 * DO NOT MODIFY once in production.
 * Write a migration to modify.
 */
module.exports = {
  $jsonSchema: {
    bsonType: 'object',
    required: [
      'txType',
      'txid',
      'txStatus',
      'status',
      'language',
      'eventType',
      'ownerAddress',
      'name',
      'results',
      'numOfResults',
      'centralizedOracle',
      'betEndTime',
      'resultSetStartTime',
    ],
    properties: {
      txType: {
        enum: [
          'CREATE_EVENT',
          'BET',
          'RESULT_SET',
          'VOTE',
          'WITHDRAW',
          'DEPOSIT',
        ],
      },
      txid: { bsonType: 'string' },
      txStatus: { enum: ['PENDING', 'SUCCESS', 'FAIL'] },
      blockNum: { bsonType: 'int' },
      status: {
        enum: [
          'CREATED',
          'PRE_BETTING',
          'BETTING',
          'PRE_RESULT_SETTING',
          'ORACLE_RESULT_SETTING',
          'OPEN_RESULT_SETTING',
          'ARBITRATION',
          'WITHDRAWING',
        ],
      },
      language: { bsonType: 'string' },
      eventType: { enum: ['MR_EVENT', 'AB_EVENT'] },
      version: { bsonType: 'int' },
      address: { bsonType: 'string' },
      ownerAddress: { bsonType: 'string' },
      name: { bsonType: 'string' },
      results: {
        bsonType: 'array',
        items: { bsonType: 'string', uniqueItems: true },
      },
      numOfResults: { bsonType: 'int' },
      winningChances: {
        bsonType: 'array',
        items: { bsonType: 'int' },
      },
      odds: {
        bsonType: 'array',
        items: { bsonType: 'decimal' },
      },
      centralizedOracle: { bsonType: 'string' },
      betStartTime: { bsonType: 'int' },
      betEndTime: { bsonType: 'int' },
      resultSetStartTime: { bsonType: 'int' },
      resultSetEndTime: { bsonType: 'int' },
      escrowAmount: { bsonType: 'long' },
      arbitrationLength: { bsonType: 'int' },
      arbitrationRewardPercentage: { bsonType: 'int' },
      thresholdPercentIncrease: { bsonType: 'int' },
      currentRound: { bsonType: 'int' },
      currentResultIndex: { bsonType: 'int' },
      consensusThreshold: { bsonType: 'long' },
      arbitrationEndTime: { bsonType: 'int' },
      betRoundResultsAmount: {
        bsonType: 'array',
        items: { bsonType: 'long' },
      },
      maxBets: {
        bsonType: 'array',
        items: { bsonType: 'long' },
      },
      withdrawnList: {
        bsonType: 'array',
        items: { bsonType: 'string', uniqueItems: true },
      },
    },
  },
};
