/**
 * DO NOT MODIFY once in production.
 * Write a migration to modify.
 */
module.exports = {
  $jsonSchema: {
    bsonType: 'object',
    required: [
      'txType',
      'txid',
      'txStatus',
      'eventAddress',
      'winnerAddress',
      'winningAmount',
      'creatorReturnAmount',
    ],
    properties: {
      txType: {
        enum: [
          'CREATE_EVENT',
          'BET',
          'RESULT_SET',
          'VOTE',
          'WITHDRAW',
          'DEPOSIT',
        ],
      },
      txid: { bsonType: 'string' },
      txStatus: { enum: ['PENDING', 'SUCCESS', 'FAIL'] },
      blockNum: { bsonType: 'int' },
      eventAddress: { bsonType: 'string' },
      winnerAddress: { bsonType: 'string' },
      winningAmount: { bsonType: 'long' },
      creatorReturnAmount: { bsonType: 'long' },
    },
  },
};
