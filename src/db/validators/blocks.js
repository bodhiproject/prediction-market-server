/**
 * DO NOT MODIFY once in production.
 * Write a migration to modify.
 */
module.exports = {
  $jsonSchema: {
    bsonType: 'object',
    required: ['blockNum', 'blockTime'],
    properties: {
      blockNum: { bsonType: 'int' },
      blockTime: { bsonType: 'int' },
    },
  },
};
