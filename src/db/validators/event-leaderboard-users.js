/**
 * DO NOT MODIFY once in production.
 * Write a migration to modify.
 */
module.exports = {
  $jsonSchema: {
    bsonType: 'object',
    required: [
      'eventAddress',
      'userAddress',
      'investments',
      'winnings',
      'returnRatio',
    ],
    properties: {
      eventAddress: { bsonType: 'string' },
      userAddress: { bsonType: 'string' },
      investments: { bsonType: 'long' },
      winnings: { bsonType: 'long' },
      returnRatio: { bsonType: 'decimal' },
    },
  },
};
