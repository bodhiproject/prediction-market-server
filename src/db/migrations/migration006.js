const { isNil, reduce, each } = require('lodash');
const Web3 = require('web3');
const { Long } = require('mongodb');
const { db } = require('..');
const BlocksDB = require('../controllers/blocks');
const DepositsDB = require('../controllers/deposits');
const logger = require('../../utils/logger');
const { isMainnet } = require('../../config');
const { EVENT_TYPE } = require('../../constants');

const { utils: { toBN } } = new Web3();

/**
 * Re-parses the deposits for RewardEvents on the testnet
 * and updates the RewardEvent.escrowAmount.
 */
module.exports = async (next) => {
  try {
    if (isMainnet()) {
      // Exit early since RewardEvents on mainnet don't have this issue
      next(null, true); // Execute next(null, true) on success
      return;
    }

    // Only find deposits up to the latest block
    const latestBlock = await BlocksDB.findLatestBlock();
    if (isNil(latestBlock)) {
      // Exit early since no latest block found
      next(null, true); // Execute next(null, true) on success
      return;
    }

    // First RewardEvent created on testnet
    const startBlock = 6583313;

    // Exit early since blocks are not up to date and haven't parsed up to the first ABEvent
    if (startBlock >= latestBlock) {
      next(null, true); // Execute next(null, true) on success
      return;
    }

    // Get all RewardEvent deposits within block range
    const deposits = await DepositsDB.find({
      eventType: EVENT_TYPE.REWARD_EVENT,
      blockNum: {
        $gte: startBlock,
        $lte: latestBlock.blockNum,
      },
    });

    // Accumulate escrowAmounts for each deposit
    const amounts = reduce(deposits, (res, deposit) => {
      if (res[deposit.eventAddress]) {
        res[deposit.eventAddress] = toBN(deposit.amount)
          .add(res[deposit.eventAddress]);
      } else {
        res[deposit.eventAddress] = toBN(deposit.amount);
      }
      return res;
    }, {});

    // Update events with new escrowAmounts
    const promises = [];
    each(amounts, (val, key) => {
      promises.push(new Promise(async (resolve, reject) => {
        try {
          await db().collection('events').updateOne(
            { address: key },
            { $set: { escrowAmount: Long.fromString(val.toString(10)) } },
          );
          resolve();
        } catch (err) {
          logger.error(`Error setting RewardEvent.escrowAmount for ${key}`);
          reject(err);
        }
      }));
    });
    await Promise.all(promises);

    logger.info('Reparsed RewardEvent escrowAmounts');
    next(null, true); // Execute next(null, true) on success
  } catch (err) {
    next(err, false); // Execute next(err, false) on error
  }
};
