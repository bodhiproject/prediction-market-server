const { db } = require('..');
const logger = require('../../utils/logger');

/**
 * Removes the unique flag from Event address index.
 * Caused issues because mutated events don't have an address.
 * Would not allow more than 1 pending Event at a time.
 */
module.exports = async (next) => {
  try {
    // Drop previous unique index address
    db().collection('events').dropIndex({ address: 1 });

    // Create new address index without unique field
    db().collection('events').createIndex(
      { address: 1 },
      (err, res) => {
        if (err) throw err;
        logger.info(`Events index recreated: ${res}`);

        next(null, true); // Execute next(null, true) on success
      },
    );
  } catch (err) {
    next(err, false); // Execute next(err, false) on error
  }
};
