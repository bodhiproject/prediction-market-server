const { cloneDeep } = require('lodash');
const { db } = require('..');
const logger = require('../../utils/logger');
const eventValidator = require('../validators/events');

/**
 * Adds the RewardEvent type to eventTypes in the Event validator.
 */
module.exports = async (next) => {
  try {
    // Clone and add reward event type
    const validator = cloneDeep(eventValidator);
    validator.$jsonSchema.properties.eventType.enum.push('REWARD_EVENT');

    // Apply new validator
    await db().command({
      collMod: 'events',
      validator,
      validationLevel: 'moderate',
    });

    logger.info('Added EventType REWARD_EVENT');
    next(null, true); // Execute next(null, true) on success
  } catch (err) {
    next(err, false); // Execute next(err, false) on error
  }
};
