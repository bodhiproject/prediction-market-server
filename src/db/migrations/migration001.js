/**
 * SAMPLE MIGRATION.
 */
module.exports = async (next) => {
  try {
    next(null, true); // Execute next(null, true) on success
  } catch (err) {
    next(err, false); // Execute next(err, false) on error
  }
};
