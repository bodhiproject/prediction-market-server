require('dotenv').config();
const fs = require('fs-extra');
const { filter, each } = require('lodash');
const { eachOfSeries } = require('async');
require('../../utils/logger');
const { connectDB } = require('..');
const logger = require('../../utils/logger');
const { Config } = require('../../config');

/**
 * Reads the last migration field from env.
 * @return {string} Last migration executed
 */
const readLastMigration = () => {
  if (!Config.DB_LAST_MIGRATION) {
    throw Error('DB_LAST_MIGRATION not be defined in env');
  }
  logger.info(`Last migration run: ${Config.DB_LAST_MIGRATION}`);
  return Config.DB_LAST_MIGRATION;
};

/**
 * Returns all the migration filenames.
 * @param {number} lastMigration Last migration filename run
 * @return {array} String array of migration filenames to run
 */
const getMigrationFilenames = (lastMigration) => {
  try {
    // Read all files in dir and only keep migration filenames
    let filenames = fs.readdirSync(__dirname);
    const regex = /(migration)(\d+)/;
    filenames = filter(filenames, file => regex.test(file));

    // Read each filename and if the lastMigration number was found,
    // set the startIndex to the next index.
    let startIndex = -1;
    each(filenames, (filename, index) => {
      if (lastMigration === filename) {
        startIndex = index + 1;
        return false;
      }
      return true;
    });

    // Keep filenames only of migrations after the lastMigration
    if (startIndex !== -1) {
      filenames = filenames.slice(startIndex);
    }

    return filenames;
  } catch (err) {
    logger.error(`Migration files read error: ${err.message}`);
    throw err;
  }
};

/**
 * Runs all the migrations after the last migration run.
 * @param {array} filenames Filesnames of all the migrations to run
 */
const runMigrations = async filenames => new Promise((resolve, reject) => {
  let curr = -1;
  eachOfSeries(
    filenames,
    (filename, index, callback) => {
      logger.info(`Running ${filenames[index]}`);
      curr = index;
      // eslint-disable-next-line global-require, import/no-dynamic-require
      const migrate = require(`./${filename}`);
      migrate(callback);
    },
    (err) => {
      if (err) {
        logger.error(`${filenames[curr]} error: ${err.message}`);
        reject(err);
        return;
      }

      logger.info('Migrations successful!');
      resolve();
    },
  );
});

(async () => {
  try {
    await connectDB();
    const lastMigration = readLastMigration();
    const filenames = getMigrationFilenames(lastMigration);
    logger.info('Migrations started...');
    await runMigrations(filenames);
    process.exit();
  } catch (err) {
    throw err;
  }
})();

process.on('unhandledRejection', (reason) => {
  throw Error(reason);
});
