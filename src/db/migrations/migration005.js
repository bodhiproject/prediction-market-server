const { cloneDeep, forEach } = require('lodash');
const { db } = require('..');
const logger = require('../../utils/logger');
const BetsDB = require('../controllers/bets');
const DepositsDB = require('../controllers/deposits');
const ResultSetsDB = require('../controllers/result-sets');
const WithdrawsDB = require('../controllers/withdraws');
const EventsDB = require('../controllers/events');
const { EVENT_TYPE } = require('../../constants');
const depositsValidator = require('../validators/deposits');
const betsValidator = require('../validators/bets');
const resultSetsValidator = require('../validators/result-sets');
const withdrawsValidator = require('../validators/withdraws');

const EVENT_TYPES = [
  EVENT_TYPE.MR_EVENT,
  EVENT_TYPE.AB_EVENT,
  EVENT_TYPE.REWARD_EVENT,
];
/**
 * Adds the eventTypes in deposits, bets, resultSets and withdraws collections validator.
 */
module.exports = async (next) => {
  try {
    // Clone and add event type to depositsValidator
    let validator = cloneDeep(depositsValidator);
    validator.$jsonSchema.properties.eventType = { enum: EVENT_TYPES };
    // Apply new validator
    await db().command({
      collMod: 'deposits',
      validator,
      validationLevel: 'moderate',
    });
    logger.info('Added eventType to deposits collection validatior');

    // Clone and add event type to betsValidator
    validator = cloneDeep(betsValidator);
    validator.$jsonSchema.properties.eventType = { enum: EVENT_TYPES };
    // Apply new validator
    await db().command({
      collMod: 'bets',
      validator,
      validationLevel: 'moderate',
    });
    logger.info('Added eventType to bets collection validatior');

    // Clone and add event type to resultSetsValidator
    validator = cloneDeep(resultSetsValidator);
    validator.$jsonSchema.properties.eventType = { enum: EVENT_TYPES };
    // Apply new validator
    await db().command({
      collMod: 'resultSets',
      validator,
      validationLevel: 'moderate',
    });
    logger.info('Added eventType to resultSets collection validatior');

    // Clone and add event type to withdrawsValidator
    validator = cloneDeep(withdrawsValidator);
    validator.$jsonSchema.properties.eventType = { enum: EVENT_TYPES };
    // Apply new validator
    await db().command({
      collMod: 'withdraws',
      validator,
      validationLevel: 'moderate',
    });
    logger.info('Added eventType to withdraws collection validatior');

    const promises = [];

    const bets = await BetsDB.find({ eventType: { $exists: false } }, { limit: 0 });
    forEach(bets, async (item, key) => {
      promises.push(new Promise(async (resolve, reject) => {
        try {
          const ret = await EventsDB.findOne({ address: item.eventAddress });
          if (ret) {
            item.eventType = ret.eventType;
            await BetsDB.updateOne(item);
          }
          resolve();
        } catch (err) {
          reject(err);
        }
      }));
    });

    const deposits = await DepositsDB.find({ eventType: { $exists: false } }, { limit: 0 });
    forEach(deposits, async (item, key) => {
      promises.push(new Promise(async (resolve, reject) => {
        try {
          const ret = await EventsDB.findOne({ address: item.eventAddress });
          if (ret) {
            item.eventType = ret.eventType;
            await DepositsDB.updateOne(item);
          }
          resolve();
        } catch (err) {
          reject(err);
        }
      }));
    });

    const resultSets = await ResultSetsDB.find({ eventType: { $exists: false } }, { limit: 0 });
    forEach(resultSets, async (item, key) => {
      promises.push(new Promise(async (resolve, reject) => {
        try {
          const ret = await EventsDB.findOne({ address: item.eventAddress });
          if (ret) {
            item.eventType = ret.eventType;
            await ResultSetsDB.updateOne(item);
          }
          resolve();
        } catch (err) {
          reject(err);
        }
      }));
    });

    const withdraws = await WithdrawsDB.find({ eventType: { $exists: false } }, { limit: 0 });
    forEach(withdraws, async (item, key) => {
      promises.push(new Promise(async (resolve, reject) => {
        try {
          const ret = await EventsDB.findOne({ address: item.eventAddress });
          if (ret) {
            item.eventType = ret.eventType;
            await WithdrawsDB.updateOne(item);
          }
          resolve();
        } catch (err) {
          reject(err);
        }
      }));
    });

    await Promise.all(promises);
    next(null, true); // Execute next(null, true) on success
  } catch (err) {
    next(err, false); // Execute next(err, false) on error
  }
};
