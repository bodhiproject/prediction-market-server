const { map } = require('lodash');
const { db } = require('..');
const { applyCursorOptions } = require('./utils');
const logger = require('../../utils/logger');
const MongoGlobalLeaderboardUser = require('../parsers/global-leaderboard-user');

/**
 * GlobalLeaderboardUsers DB Controller.
 */
module.exports = class GlobalLeaderboardUsersDB {
  static async updateOne(user) {
    try {
      await db().collection('globalLeaderboardUsers').updateOne(
        { userAddress: user.userAddress },
        { $set: MongoGlobalLeaderboardUser.encode(user) },
        { upsert: true },
      );
    } catch (err) {
      logger.error(`GlobalLeaderboardUsersDB.updateOne error: ${err.message}`);
      throw err;
    }
  }

  static async find(query, options) {
    try {
      let cursor = db().collection('globalLeaderboardUsers').find(query);
      cursor = applyCursorOptions(cursor, options);
      const arr = await cursor.toArray();
      return map(arr, user => MongoGlobalLeaderboardUser.decode(user));
    } catch (err) {
      logger.error(`GlobalLeaderboardUsersDB.find error: ${err.message}`);
      throw err;
    }
  }

  static async findOne(query) {
    try {
      const user = await db().collection('globalLeaderboardUsers').findOne(query);
      return user ? MongoGlobalLeaderboardUser.decode(user) : null;
    } catch (err) {
      logger.error(`GlobalLeaderboardUsersDB.findOne error: ${err.message}`);
      throw err;
    }
  }

  static async count(query) {
    try {
      return db().collection('globalLeaderboardUsers').countDocuments(query);
    } catch (err) {
      logger.error(`GlobalLeaderboardUsersDB.count error: ${err.message}`);
      throw err;
    }
  }

  static async addToInvestmentsAndWinnings(user) {
    try {
      if (!user) throw Error('user is undefined');

      const mongoUser = MongoGlobalLeaderboardUser.encode(user);
      await db().collection('globalLeaderboardUsers').updateOne(
        { userAddress: user.userAddress, eventAddress: user.eventAddress },
        {
          $inc: {
            investments: mongoUser.investments,
            winnings: mongoUser.winnings,
          },
        },
      );
    } catch (err) {
      logger.error(`GlobalLeaderboardUsersDB.addToInvestmentsAndWinnings error: ${err.message}`);
      throw err;
    }
  }
};
