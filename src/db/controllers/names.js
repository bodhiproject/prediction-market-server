const { db } = require('..');
const { applyCursorOptions } = require('./utils');
const logger = require('../../utils/logger');
const MongoName = require('../parsers/name');

/**
 * Names DB Controller.
 */
module.exports = class NamesDB {
  static async updateOne(name) {
    try {
      await db().collection('names').updateOne(
        { address: name.address },
        { $set: MongoName.encode(name) },
        { upsert: true },
      );
    } catch (err) {
      logger.error(`NamesDB.updateOne error: ${err.message}`);
      throw err;
    }
  }

  static async find(query, options) {
    try {
      let cursor = db().collection('names').find(query);
      cursor = applyCursorOptions(cursor, options);
      return cursor.toArray();
    } catch (err) {
      logger.error(`NamesDB.find error: ${err.message}`);
      throw err;
    }
  }

  static async findOne(query) {
    try {
      const name = await db().collection('names').findOne(query);
      return name ? MongoName.decode(name) : null;
    } catch (err) {
      logger.error(`NamesDB.findOne error: ${err.message}`);
      throw err;
    }
  }

  static async count(query) {
    try {
      return db().collection('names').countDocuments(query);
    } catch (err) {
      logger.error(`NamesDB.count error: ${err.message}`);
      throw err;
    }
  }
};
