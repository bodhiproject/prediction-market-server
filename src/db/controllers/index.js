const BetsDB = require('./bets');
const BlocksDB = require('./blocks');
const DepositsDB = require('./deposits');
const EventLeaderboardUsersDB = require('./event-leaderboard-users');
const EventsDB = require('./events');
const GlobalLeaderboardUsersDB = require('./global-leaderboard-users');
const NamesDB = require('./names');
const ResultSetsDB = require('./result-sets');
const TxReceiptsDB = require('./tx-receipts');
const WithdrawsDB = require('./withdraws');

module.exports = {
  BetsDB,
  BlocksDB,
  DepositsDB,
  EventLeaderboardUsersDB,
  EventsDB,
  GlobalLeaderboardUsersDB,
  NamesDB,
  ResultSetsDB,
  TxReceiptsDB,
  WithdrawsDB,
};
