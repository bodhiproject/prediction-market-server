const { map } = require('lodash');
const { db } = require('..');
const { applyCursorOptions } = require('./utils');
const logger = require('../../utils/logger');
const MongoResultSet = require('../parsers/result-set');

/**
 * ResultSets DB Controller.
 */
module.exports = class ResultSetsDB {
  static async updateOne(resultSet) {
    try {
      await db().collection('resultSets').updateOne(
        { txid: resultSet.txid },
        { $set: MongoResultSet.encode(resultSet) },
        { upsert: true },
      );
    } catch (err) {
      logger.error(`ResultSetsDB.updateOne error: ${err.message}`);
      throw err;
    }
  }

  static async find(query, options) {
    try {
      let cursor = db().collection('resultSets').find(query);
      cursor = applyCursorOptions(cursor, options);
      const arr = await cursor.toArray();
      return map(arr, resultSet => MongoResultSet.decode(resultSet));
    } catch (err) {
      logger.error(`ResultSetsDB.find error: ${err.message}`);
      throw err;
    }
  }

  static async findOne(query) {
    try {
      const resultSet = await db().collection('resultSets').findOne(query);
      return resultSet ? MongoResultSet.decode(resultSet) : null;
    } catch (err) {
      logger.error(`ResultSetsDB.findOne error: ${err.message}`);
      throw err;
    }
  }

  static async distinct(field, query) {
    try {
      return db().collection('resultSets').distinct(field, query);
    } catch (err) {
      logger.error(`ResultSetsDB.distinct error: ${err.message}`);
      throw err;
    }
  }

  static async count(query) {
    try {
      return db().collection('resultSets').countDocuments(query);
    } catch (err) {
      logger.error(`ResultSetsDB.count error: ${err.message}`);
      throw err;
    }
  }
};
