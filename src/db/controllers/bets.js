const { map } = require('lodash');
const { db } = require('..');
const { applyCursorOptions } = require('./utils');
const logger = require('../../utils/logger');
const MongoBet = require('../parsers/bet');

/**
 * Bets DB Controller.
 */
module.exports = class BetsDB {
  static async updateOne(bet) {
    try {
      await db().collection('bets').updateOne(
        { txid: bet.txid },
        { $set: MongoBet.encode(bet) },
        { upsert: true },
      );
    } catch (err) {
      logger.error(`BetsDB.updateOne error: ${err.message}`);
      throw err;
    }
  }

  static async find(query, options) {
    try {
      let cursor = db().collection('bets').find(query);
      cursor = applyCursorOptions(cursor, options);
      const arr = await cursor.toArray();
      return map(arr, bet => MongoBet.decode(bet));
    } catch (err) {
      logger.error(`BetsDB.find error: ${err.message}`);
      throw err;
    }
  }

  static async findOne(query) {
    try {
      const bet = await db().collection('bets').findOne(query);
      return bet ? MongoBet.decode(bet) : null;
    } catch (err) {
      logger.error(`BetsDB.findOne error: ${err.message}`);
      throw err;
    }
  }

  static async distinct(field, query) {
    try {
      return db().collection('bets').distinct(field, query);
    } catch (err) {
      logger.error(`BetsDB.distinct error: ${err.message}`);
      throw err;
    }
  }

  static async count(query) {
    try {
      return db().collection('bets').countDocuments(query);
    } catch (err) {
      logger.error(`BetsDB.count error: ${err.message}`);
      throw err;
    }
  }

  static async aggregateAmount() {
    try {
      const cursor = db().collection('bets').aggregate([
        { $group: { _id: null, total: { $sum: '$amount' } } },
      ]);
      const res = await cursor.toArray();
      return res[0].total.toString(10);
    } catch (err) {
      logger.error(`BetsDB.aggregateAmount error: ${err.message}`);
      throw err;
    }
  }
};
