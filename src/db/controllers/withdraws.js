const { map } = require('lodash');
const { db } = require('..');
const { applyCursorOptions } = require('./utils');
const logger = require('../../utils/logger');
const MongoWithdraw = require('../parsers/withdraw');

/**
 * Withdraws DB Controller.
 */
module.exports = class WithdrawsDB {
  static async updateOne(withdraw) {
    try {
      await db().collection('withdraws').updateOne(
        { txid: withdraw.txid },
        { $set: MongoWithdraw.encode(withdraw) },
        { upsert: true },
      );
    } catch (err) {
      logger.error(`WithdrawsDB.updateOne error: ${err.message}`);
      throw err;
    }
  }

  static async find(query, options) {
    try {
      let cursor = db().collection('withdraws').find(query);
      cursor = applyCursorOptions(cursor, options);
      const arr = await cursor.toArray();
      return map(arr, withdraw => MongoWithdraw.decode(withdraw));
    } catch (err) {
      logger.error(`WithdrawsDB.find error: ${err.message}`);
      throw err;
    }
  }

  static async findOne(query) {
    try {
      const withdraw = await db().collection('withdraws').findOne(query);
      return withdraw ? MongoWithdraw.decode(withdraw) : null;
    } catch (err) {
      logger.error(`WithdrawsDB.findOne error: ${err.message}`);
      throw err;
    }
  }

  static async count(query) {
    try {
      return db().collection('withdraws').countDocuments(query);
    } catch (err) {
      logger.error(`WithdrawsDB.count error: ${err.message}`);
      throw err;
    }
  }
};
