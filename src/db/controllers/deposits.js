const { map } = require('lodash');
const { db } = require('..');
const { applyCursorOptions } = require('./utils');
const logger = require('../../utils/logger');
const MongoDeposit = require('../parsers/deposit');

/**
 * Deposits DB Controller.
 */
module.exports = class DepositsDB {
  static async updateOne(deposit) {
    try {
      await db().collection('deposits').updateOne(
        { txid: deposit.txid },
        { $set: MongoDeposit.encode(deposit) },
        { upsert: true },
      );
    } catch (err) {
      logger.error(`DepositsDB.updateOne error: ${err.message}`);
      throw err;
    }
  }

  static async find(query, options) {
    try {
      let cursor = db().collection('deposits').find(query);
      cursor = applyCursorOptions(cursor, options);
      const arr = await cursor.toArray();
      return map(arr, deposit => MongoDeposit.decode(deposit));
    } catch (err) {
      logger.error(`DepositsDB.find error: ${err.message}`);
      throw err;
    }
  }

  static async findOne(query) {
    try {
      const deposit = await db().collection('deposits').findOne(query);
      return deposit ? MongoDeposit.decode(deposit) : null;
    } catch (err) {
      logger.error(`DepositsDB.findOne error: ${err.message}`);
      throw err;
    }
  }

  static async count(query) {
    try {
      return db().collection('deposits').countDocuments(query);
    } catch (err) {
      logger.error(`DepositsDB.count error: ${err.message}`);
      throw err;
    }
  }
};
