const { map } = require('lodash');
const { db } = require('..');
const { applyCursorOptions } = require('./utils');
const logger = require('../../utils/logger');
const { EVENT_STATUS, TX_STATUS } = require('../../constants');
const MongoEvent = require('../parsers/event');
const MongoBet = require('../parsers/bet');
const MongoDeposit = require('../parsers/deposit');

/**
 * Events DB Controller.
 */
module.exports = class EventsDB {
  static async updateOne(event) {
    try {
      await db().collection('events').updateOne(
        { txid: event.txid },
        { $set: MongoEvent.encode(event) },
        { upsert: true },
      );
    } catch (err) {
      logger.error(`EventsDB.updateOne error: ${err.message}`);
      throw err;
    }
  }

  static async updateOneByAddress(address, fields) {
    try {
      await db().collection('events').updateOne(
        { address },
        { $set: MongoEvent.encode(fields) },
      );
    } catch (err) {
      logger.error(`EventsDB.updateOneByAddress error: ${err.message}`);
      throw err;
    }
  }

  static async find(query, options) {
    try {
      let cursor = db().collection('events').find(query);
      cursor = applyCursorOptions(cursor, options);
      const arr = await cursor.toArray();
      return map(arr, event => MongoEvent.decode(event));
    } catch (err) {
      logger.error(`EventsDB.find error: ${err.message}`);
      throw err;
    }
  }

  static async findOne(query) {
    try {
      const event = await db().collection('events').findOne(query);
      return event ? MongoEvent.decode(event) : null;
    } catch (err) {
      logger.error(`EventsDB.findOne error: ${err.message}`);
      throw err;
    }
  }

  static async count(query) {
    try {
      return db().collection('events').countDocuments(query);
    } catch (err) {
      logger.error(`EventsDB.count error: ${err.message}`);
      throw err;
    }
  }

  static async updateEventStatusPreBetting(currBlockTime) {
    try {
      await db().collection('events').updateMany(
        {
          txStatus: TX_STATUS.SUCCESS,
          status: { $ne: EVENT_STATUS.PRE_BETTING },
          betStartTime: { $gt: currBlockTime },
          currentRound: 0,
        },
        { $set: { status: EVENT_STATUS.PRE_BETTING } },
      );
    } catch (err) {
      logger.error(`EventsDB.updateEventStatusPreBetting error: ${err.message}`);
      throw err;
    }
  }

  static async updateEventStatusBetting(currBlockTime) {
    try {
      await db().collection('events').updateMany(
        {
          txStatus: TX_STATUS.SUCCESS,
          status: { $ne: EVENT_STATUS.BETTING },
          betStartTime: { $lte: currBlockTime },
          betEndTime: { $gt: currBlockTime },
          currentRound: 0,
        },
        { $set: { status: EVENT_STATUS.BETTING } },
      );
    } catch (err) {
      logger.error(`EventsDB.updateEventStatusBetting error: ${err.message}`);
      throw err;
    }
  }

  static async updateEventStatusPreResultSetting(currBlockTime) {
    try {
      await db().collection('events').updateMany(
        {
          txStatus: TX_STATUS.SUCCESS,
          status: { $ne: EVENT_STATUS.PRE_RESULT_SETTING },
          betEndTime: { $lte: currBlockTime },
          resultSetStartTime: { $gt: currBlockTime },
          currentRound: 0,
        },
        { $set: { status: EVENT_STATUS.PRE_RESULT_SETTING } },
      );
    } catch (err) {
      logger.error(`EventsDB.updateEventStatusPreResultSetting error: ${err.message}`);
      throw err;
    }
  }

  static async updateEventStatusOracleResultSetting(currBlockTime) {
    try {
      await db().collection('events').updateMany(
        {
          txStatus: TX_STATUS.SUCCESS,
          status: { $ne: EVENT_STATUS.ORACLE_RESULT_SETTING },
          resultSetStartTime: { $lte: currBlockTime },
          resultSetEndTime: { $gt: currBlockTime },
          currentRound: 0,
        },
        { $set: { status: EVENT_STATUS.ORACLE_RESULT_SETTING } },
      );
    } catch (err) {
      logger.error(`EventsDB.updateEventStatusOracleResultSetting error: ${err.message}`);
      throw err;
    }
  }

  static async updateEventStatusOpenResultSetting(currBlockTime) {
    try {
      await db().collection('events').updateMany(
        {
          txStatus: TX_STATUS.SUCCESS,
          status: { $ne: EVENT_STATUS.OPEN_RESULT_SETTING },
          resultSetEndTime: { $lte: currBlockTime },
          currentRound: 0,
        },
        { $set: { status: EVENT_STATUS.OPEN_RESULT_SETTING } },
      );
    } catch (err) {
      logger.error(`EventsDB.updateEventStatusOpenResultSetting error: ${err.message}`);
      throw err;
    }
  }

  static async updateEventStatusArbitration(currBlockTime) {
    try {
      await db().collection('events').updateMany(
        {
          txStatus: TX_STATUS.SUCCESS,
          status: { $ne: EVENT_STATUS.ARBITRATION },
          arbitrationEndTime: { $gt: currBlockTime },
          currentRound: { $gt: 0 },
        },
        { $set: { status: EVENT_STATUS.ARBITRATION } },
      );
    } catch (err) {
      logger.error(`EventsDB.updateEventStatusArbitration error: ${err.message}`);
      throw err;
    }
  }

  static async updateEventStatusWithdrawing(currBlockTime) {
    try {
      const query = {
        txStatus: TX_STATUS.SUCCESS,
        status: { $ne: EVENT_STATUS.WITHDRAWING },
        arbitrationEndTime: { $lte: currBlockTime },
        currentRound: { $gt: 0 },
      };

      // Get events that will be updated to update leaderboards
      const cursor = db().collection('events').find(query);
      let withdrawingEvents = await cursor.toArray();
      withdrawingEvents = map(withdrawingEvents, event => MongoEvent.decode(event));

      await db().collection('events').updateMany(
        {
          txStatus: TX_STATUS.SUCCESS,
          status: { $ne: EVENT_STATUS.WITHDRAWING },
          arbitrationEndTime: { $lte: currBlockTime },
          currentRound: { $gt: 0 },
        },
        { $set: { status: EVENT_STATUS.WITHDRAWING } },
      );

      return withdrawingEvents;
    } catch (err) {
      logger.error(`EventsDB.updateEventStatusWithdrawing error: ${err.message}`);
      throw err;
    }
  }

  static async addToWithdrawnList(withdraw) {
    try {
      if (!withdraw) throw Error('withdraw is undefined');

      await db().collection('events').updateOne(
        { address: withdraw.eventAddress },
        { $addToSet: { withdrawnList: withdraw.winnerAddress } },
      );
    } catch (err) {
      logger.error(`EventsDB.addToWithdrawnList error: ${err.message}`);
      throw err;
    }
  }

  static async addToEscrowAmount(deposit) {
    try {
      if (!deposit) throw Error('deposit is undefined');

      const mongoDeposit = MongoDeposit.encode(deposit);
      await db().collection('events').updateOne(
        { address: mongoDeposit.eventAddress },
        { $inc: { escrowAmount: mongoDeposit.amount } },
      );
    } catch (err) {
      logger.error(`EventsDB.addToEscrowAmount error: ${err.message}`);
      throw err;
    }
  }

  static async addToBetRoundResultsAmount(bet) {
    try {
      if (!bet) throw Error('bet is undefined');

      const mongoBet = MongoBet.encode(bet);
      await db().collection('events').updateOne(
        { address: mongoBet.eventAddress },
        { $inc: { [`betRoundResultsAmount.${bet.resultIndex}`]: mongoBet.amount } },
      );
    } catch (err) {
      logger.error(`EventsDB.addToBetRoundResultsAmount error: ${err.message}`);
      throw err;
    }
  }
};
