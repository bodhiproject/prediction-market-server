const { db } = require('../');
const { applyCursorOptions } = require('./utils');
const logger = require('../../utils/logger');
const MongoBlock = require('../parsers/block');

/**
 * Blocks DB Controller.
 */
module.exports = class BlocksDB {
  static async updateOne(block) {
    try {
      await db().collection('blocks').updateOne(
        { blockNum: block.blockNum },
        { $set: MongoBlock.encode(block) },
        { upsert: true },
      );
    } catch (err) {
      logger.error(`BlocksDB.updateOne error: ${err.message}`);
      throw err;
    }
  }

  static async find(query, options) {
    try {
      let cursor = db().collection('blocks').find(query);
      cursor = applyCursorOptions(cursor, options);
      return cursor.toArray();
    } catch (err) {
      logger.error(`BlocksDB.find error: ${err.message}`);
      throw err;
    }
  }

  static async findOne(query) {
    try {
      const block = await db().collection('blocks').findOne(query);
      return block ? MongoBlock.decode(block) : null;
    } catch (err) {
      logger.error(`BlocksDB.findOne error: ${err.message}`);
      throw err;
    }
  }

  static async count(query) {
    try {
      return db().collection('blocks').countDocuments(query);
    } catch (err) {
      logger.error(`BlocksDB.count error: ${err.message}`);
      throw err;
    }
  }

  static async findLatestBlock() {
    try {
      const block = await db().collection('blocks').findOne(
        {},
        { sort: { blockNum: -1 } },
      );
      return block ? MongoBlock.decode(block) : null;
    } catch (err) {
      logger.error(`BlocksDB.findLatestBlock error: ${err.message}`);
      throw err;
    }
  }
};
