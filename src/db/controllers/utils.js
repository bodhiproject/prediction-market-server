const { isEmpty, isArray, each, isNil } = require('lodash');
const { ORDER_DIRECTION } = require('../../constants');

const DEFAULT_LIMIT_NUM = 50;
const DEFAULT_SKIP_NUM = 0;

/**
 * Builds MongoDB compatible cursor options.
 * @param {Cursor} cursor MongoDB cursor
 * @param {object} options Options for the cursor
 * @return {Cursor} MongoDB cursor
 */
const applyCursorOptions = (cursor, options = {}) => {
  const { skip, limit } = options;
  let { orderBy } = options;

  if (!isEmpty(orderBy)) {
    const sortDict = [];
    if (!isArray(orderBy)) orderBy = [orderBy]; // Wrap in array if needed

    each(orderBy, (obj) => {
      sortDict.push([
        obj.field,
        obj.direction === ORDER_DIRECTION.ASCENDING ? 1 : -1,
      ]);
    });
    if (!isEmpty(sortDict)) cursor.sort(sortDict);
  }
  cursor.skip(skip || DEFAULT_SKIP_NUM);
  cursor.limit(isNil(limit) ? DEFAULT_LIMIT_NUM : limit);

  return cursor;
};

module.exports = {
  applyCursorOptions,
};
