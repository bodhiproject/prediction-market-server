const { map } = require('lodash');
const { db } = require('..');
const { applyCursorOptions } = require('./utils');
const logger = require('../../utils/logger');
const MongoTxReceipt = require('../parsers/tx-receipt');

/**
 * TxReceipts DB Controller.
 */
module.exports = class TxReceiptsDB {
  static async updateOne(txReceipt) {
    try {
      await db().collection('txReceipts').updateOne(
        { transactionHash: txReceipt.transactionHash },
        { $set: MongoTxReceipt.encode(txReceipt) },
        { upsert: true },
      );
    } catch (err) {
      logger.error(`TxReceiptsDB.updateOne error: ${err.message}`);
      throw err;
    }
  }

  static async find(query, options) {
    try {
      let cursor = db().collection('txReceipts').find(query);
      cursor = applyCursorOptions(cursor, options);
      const arr = await cursor.toArray();
      return map(arr, txReceipt => MongoTxReceipt.decode(txReceipt));
    } catch (err) {
      logger.error(`TxReceiptsDB.find error: ${err.message}`);
      throw err;
    }
  }

  static async findOne(query) {
    try {
      const txReceipt = await db().collection('txReceipts').findOne(query);
      return txReceipt ? MongoTxReceipt.decode(txReceipt) : null;
    } catch (err) {
      logger.error(`TxReceiptsDB.findOne error: ${err.message}`);
      throw err;
    }
  }

  static async count(query) {
    try {
      return db().collection('txReceipts').countDocuments(query);
    } catch (err) {
      logger.error(`TxReceiptsDB.count error: ${err.message}`);
      throw err;
    }
  }
};
