const { map } = require('lodash');
const { db } = require('..');
const { applyCursorOptions } = require('./utils');
const logger = require('../../utils/logger');
const MongoEventLeaderboardUser = require('../parsers/event-leaderboard-user');

/**
 * EventLeaderboardUsers DB Controller.
 */
module.exports = class EventLeaderboardUsersDB {
  static async updateOne(user) {
    try {
      await db().collection('eventLeaderboardUsers').updateOne(
        { userAddress: user.userAddress, eventAddress: user.eventAddress },
        { $set: MongoEventLeaderboardUser.encode(user) },
        { upsert: true },
      );
    } catch (err) {
      logger.error(`EventLeaderboardUsersDB.updateOne error: ${err.message}`);
      throw err;
    }
  }

  static async find(query, options) {
    try {
      let cursor = db().collection('eventLeaderboardUsers').find(query);
      cursor = applyCursorOptions(cursor, options);
      const arr = await cursor.toArray();
      return map(arr, user => MongoEventLeaderboardUser.decode(user));
    } catch (err) {
      logger.error(`EventLeaderboardUsersDB.find error: ${err.message}`);
      throw err;
    }
  }

  static async findOne(query) {
    try {
      const user = await db().collection('eventLeaderboardUsers').findOne(query);
      return user ? MongoEventLeaderboardUser.decode(user) : null;
    } catch (err) {
      logger.error(`EventLeaderboardUsersDB.findOne error: ${err.message}`);
      throw err;
    }
  }

  static async count(query) {
    try {
      return db().collection('eventLeaderboardUsers').countDocuments(query);
    } catch (err) {
      logger.error(`EventLeaderboardUsersDB.count error: ${err.message}`);
      throw err;
    }
  }

  static async addToInvestments(user) {
    try {
      await db().collection('eventLeaderboardUsers').updateOne(
        { userAddress: user.userAddress, eventAddress: user.eventAddress },
        { $inc: { investments: MongoEventLeaderboardUser.encode(user).investments } },
      );
    } catch (err) {
      logger.error(`EventLeaderboardUsersDB.addToInvestments error: ${err.message}`);
      throw err;
    }
  }

  static async addToWinnings(user) {
    try {
      await db().collection('eventLeaderboardUsers').updateOne(
        { userAddress: user.userAddress, eventAddress: user.eventAddress },
        { $inc: { winnings: MongoEventLeaderboardUser.encode(user).winnings } },
      );
    } catch (err) {
      logger.error(`EventLeaderboardUsersDB.addToWinnings error: ${err.message}`);
      throw err;
    }
  }
};
