const { MongoClient } = require('mongodb');
const { Config } = require('../config');
const emitter = require('../utils/emitter');
const logger = require('../utils/logger');
const { EVENT_MESSAGE } = require('../constants');
const { getDBName } = require('../config');
const createBlocks = require('./init/blocks');
const createTxReceipts = require('./init/tx-receipts');
const createNames = require('./init/names');
const createEvents = require('./init/events');
const createBets = require('./init/bets');
const createResultSets = require('./init/result-sets');
const createWithdraws = require('./init/withdraws');
const createDeposits = require('./init/deposits');
const createGlobalLeaderboardUsers = require('./init/global-leaderboard-users');
const createEventLeaderboardUsers = require('./init/event-leaderboard-users');

let client;
let instance;

/**
 * Constructs the MongoDB URI to connect to.
 * @param {string} dbName DB name to connect to.
 * @return {string} URI to MongoDB.
 */
const constructURI = (dbName) => {
  let uri = 'mongodb://';
  if (Config.MONGO_USERNAME) {
    uri += `${Config.MONGO_USERNAME}:${Config.MONGO_PASSWORD}@`;
  }
  uri +=
    `${Config.MONGO_HOSTNAME}:${Config.MONGO_PORT}/` +
    `${dbName}?authSource=admin` +
    `${Config.MONGO_REPLICASET ? `&replicaSet=${Config.MONGO_REPLICASET}` : ''}`;
  return uri;
};

/**
 * Connects to MongoDB and sets up event listeners.
 */
const connectDB = async () => {
  // Establish connection
  const dbName = getDBName();
  const uri = constructURI(dbName);
  client = await MongoClient.connect(uri, { useNewUrlParser: true });

  // Connect to specific DB
  instance = client.db(dbName);
  logger.info(`DB connected to ${dbName} at ${uri}`);

  // Handle events
  instance.on('close', () => {
    logger.error('DB connection closed');
    emitter.emit(EVENT_MESSAGE.DB_DISCONNECTED);
  });
  instance.on('reconnect', () => {
    logger.info('DB connection reconnected');
    emitter.emit(EVENT_MESSAGE.DB_CONNECTED);
  });
};

/**
 * Creates all the collections and indexes.
 */
const createCollections = async () => {
  logger.info('Creating DB collections and indexes');
  await Promise.all([
    createBlocks(instance),
    createTxReceipts(instance),
    createNames(instance),
    createEvents(instance),
    createBets(instance),
    createResultSets(instance),
    createWithdraws(instance),
    createDeposits(instance),
    createGlobalLeaderboardUsers(instance),
    createEventLeaderboardUsers(instance),
  ]);
};

/**
 * All initial setup for the DB.
 */
const initDB = async () => {
  try {
    await connectDB();
    await createCollections();
    emitter.emit(EVENT_MESSAGE.DB_CONNECTED);
  } catch (err) {
    logger.error('DB load error');
    throw err;
  }
};

/**
 * Closes the DB.
 * This should only be used for testing since MongoDB recommends not to close the
 * connection due to overhead of opening TCP connections.
 */
const closeDB = async () => client.close();

const db = () => instance;

module.exports = {
  connectDB,
  initDB,
  closeDB,
  db,
};
