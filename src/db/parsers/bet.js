const { Int32, Long } = require('mongodb');
const { isUndefined } = require('lodash');

/**
 * Bet model encoder to and decoder from MongoDB.
 */
module.exports = class MongoBet {
  static encode(params) {
    const encoded = {};
    Object.assign(encoded, params);

    if (!isUndefined(encoded.blockNum)) {
      encoded.blockNum = Int32(encoded.blockNum);
    }
    if (!isUndefined(encoded.resultIndex)) {
      encoded.resultIndex = Int32(encoded.resultIndex);
    }
    if (!isUndefined(encoded.amount)) {
      encoded.amount = Long.fromString(encoded.amount);
    }
    if (!isUndefined(encoded.eventRound)) {
      encoded.eventRound = Int32(encoded.eventRound);
    }
    return encoded;
  }

  static decode(params) {
    const decoded = {};
    Object.assign(decoded, params);

    decoded.amount = decoded.amount.toString(10);
    return decoded;
  }
};
