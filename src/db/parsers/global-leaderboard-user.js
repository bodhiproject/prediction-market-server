const { Long, Decimal128 } = require('mongodb');
const { isUndefined } = require('lodash');

/**
 * GlobalLeaderboardUser model encoder to/decoder from MongoDB.
 */
module.exports = class MongoGlobalLeaderboardUser {
  static encode(params) {
    const encoded = {};
    Object.assign(encoded, params);

    if (!isUndefined(encoded.investments)) {
      encoded.investments = Long.fromString(encoded.investments);
    }
    if (!isUndefined(encoded.winnings)) {
      encoded.winnings = Long.fromString(encoded.winnings);
    }
    if (!isUndefined(encoded.returnRatio)) {
      encoded.returnRatio = Decimal128.fromString(encoded.returnRatio.toFixed(2));
    }
    return encoded;
  }

  static decode(params) {
    const decoded = {};
    Object.assign(decoded, params);

    decoded.investments = decoded.investments.toString(10);
    decoded.winnings = decoded.winnings.toString(10);
    decoded.returnRatio = Number(decoded.returnRatio.toString());
    return decoded;
  }
};
