const { Int32, Long } = require('mongodb');
const { isUndefined } = require('lodash');

/**
 * Deposit model encoder to/decoder from MongoDB.
 */
module.exports = class MongoDeposit {
  static encode(params) {
    const encoded = {};
    Object.assign(encoded, params);

    if (!isUndefined(encoded.blockNum)) {
      encoded.blockNum = Int32(encoded.blockNum);
    }
    if (!isUndefined(encoded.amount)) {
      encoded.amount = Long.fromString(encoded.amount);
    }
    return encoded;
  }

  static decode(params) {
    const decoded = {};
    Object.assign(decoded, params);

    decoded.amount = decoded.amount.toString(10);
    return decoded;
  }
};
