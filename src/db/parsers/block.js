const { Int32 } = require('mongodb');
const { isUndefined } = require('lodash');

/**
 * Block model encoder to and decoder from MongoDB.
 */
module.exports = class MongoBlock {
  static encode(params) {
    const encoded = {};
    Object.assign(encoded, params);

    if (!isUndefined(encoded.blockNum)) {
      encoded.blockNum = Int32(encoded.blockNum);
    }
    if (!isUndefined(encoded.blockTime)) {
      encoded.blockTime = Int32(encoded.blockTime);
    }
    return encoded;
  }

  static decode(params) {
    const decoded = {};
    Object.assign(decoded, params);

    return decoded;
  }
};
