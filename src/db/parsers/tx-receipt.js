const { Int32, Long } = require('mongodb');
const { isUndefined } = require('lodash');

/**
 * TxReceipt model encoder to/decoder from MongoDB.
 */
module.exports = class MongoTxReceipt {
  static encode(params) {
    const encoded = {};
    Object.assign(encoded, params);

    if (!isUndefined(encoded.blockNum)) {
      encoded.blockNum = Int32(encoded.blockNum);
    }
    if (!isUndefined(encoded.cumulativeGasUsed)) {
      encoded.cumulativeGasUsed = Int32(encoded.cumulativeGasUsed);
    }
    if (!isUndefined(encoded.gasUsed)) encoded.gasUsed = Int32(encoded.gasUsed);
    if (!isUndefined(encoded.gasPrice)) {
      encoded.gasPrice = Long.fromString(encoded.gasPrice);
    }
    return encoded;
  }

  static decode(params) {
    const decoded = {};
    Object.assign(decoded, params);

    decoded.gasPrice = decoded.gasPrice.toString(10);
    return decoded;
  }
};
