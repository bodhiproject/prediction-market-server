const { Int32, Long } = require('mongodb');
const { isUndefined } = require('lodash');

/**
 * Withdraw model encoder to/decoder from MongoDB.
 */
module.exports = class MongoWithdraw {
  static encode(params) {
    const encoded = {};
    Object.assign(encoded, params);

    if (!isUndefined(encoded.blockNum)) {
      encoded.blockNum = Int32(encoded.blockNum);
    }
    if (!isUndefined(encoded.winningAmount)) {
      encoded.winningAmount = Long.fromString(encoded.winningAmount);
    }
    if (!isUndefined(encoded.creatorReturnAmount)) {
      encoded.creatorReturnAmount = Long.fromString(encoded.creatorReturnAmount);
    }
    return encoded;
  }

  static decode(params) {
    const decoded = {};
    Object.assign(decoded, params);

    decoded.winningAmount = decoded.winningAmount.toString(10);
    decoded.creatorReturnAmount = decoded.creatorReturnAmount.toString(10);
    return decoded;
  }
};
