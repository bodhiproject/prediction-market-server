const { Int32, Long, Decimal128 } = require('mongodb');
const { isUndefined, map } = require('lodash');

/**
 * Event model encoder to and decoder from MongoDB.
 */
module.exports = class MongoEvent {
  static encode(params) {
    const encoded = {};
    Object.assign(encoded, params);

    if (!isUndefined(encoded.blockNum)) {
      encoded.blockNum = Int32(encoded.blockNum);
    }
    if (!isUndefined(encoded.version)) encoded.version = Int32(encoded.version);
    if (!isUndefined(encoded.numOfResults)) {
      encoded.numOfResults = Int32(encoded.numOfResults);
    }
    if (!isUndefined(encoded.winningChances)) {
      encoded.winningChances = map(encoded.winningChances, item => Int32(item));
    }
    if (!isUndefined(encoded.odds)) {
      encoded.odds = map(encoded.odds, item => Decimal128.fromString(item.toFixed(2)));
    }
    if (!isUndefined(encoded.betStartTime)) {
      encoded.betStartTime = Int32(encoded.betStartTime);
    }
    if (!isUndefined(encoded.betEndTime)) {
      encoded.betEndTime = Int32(encoded.betEndTime);
    }
    if (!isUndefined(encoded.resultSetStartTime)) {
      encoded.resultSetStartTime = Int32(encoded.resultSetStartTime);
    }
    if (!isUndefined(encoded.resultSetEndTime)) {
      encoded.resultSetEndTime = Int32(encoded.resultSetEndTime);
    }
    if (!isUndefined(encoded.escrowAmount)) {
      encoded.escrowAmount = Long.fromString(encoded.escrowAmount);
    }
    if (!isUndefined(encoded.arbitrationLength)) {
      encoded.arbitrationLength = Int32(encoded.arbitrationLength);
    }
    if (!isUndefined(encoded.arbitrationRewardPercentage)) {
      encoded.arbitrationRewardPercentage = Int32(encoded.arbitrationRewardPercentage);
    }
    if (!isUndefined(encoded.thresholdPercentIncrease)) {
      encoded.thresholdPercentIncrease = Int32(encoded.thresholdPercentIncrease);
    }
    if (!isUndefined(encoded.currentRound)) {
      encoded.currentRound = Int32(encoded.currentRound);
    }
    if (!isUndefined(encoded.currentResultIndex)) {
      encoded.currentResultIndex = Int32(encoded.currentResultIndex);
    }
    if (!isUndefined(encoded.consensusThreshold)) {
      encoded.consensusThreshold = Long.fromString(encoded.consensusThreshold);
    }
    if (!isUndefined(encoded.arbitrationEndTime)) {
      encoded.arbitrationEndTime = Int32(encoded.arbitrationEndTime);
    }
    if (!isUndefined(encoded.betRoundResultsAmount)) {
      encoded.betRoundResultsAmount = map(
        encoded.betRoundResultsAmount,
        item => Long.fromString(item),
      );
    }
    if (!isUndefined(encoded.maxBets)) {
      encoded.maxBets = map(encoded.maxBets, item => Long.fromString(item));
    }
    return encoded;
  }

  static decode(params) {
    const decoded = {};
    Object.assign(decoded, params);

    decoded.odds = map(decoded.odds, item => Number(item.toString()));
    decoded.escrowAmount = decoded.escrowAmount.toString(10);
    decoded.consensusThreshold = decoded.consensusThreshold.toString(10);
    decoded.betRoundResultsAmount = map(
      decoded.betRoundResultsAmount,
      item => item.toString(10),
    );
    decoded.maxBets = map(decoded.maxBets, item => item.toString(10));
    return decoded;
  }
};
