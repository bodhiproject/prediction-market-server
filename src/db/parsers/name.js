/**
 * Name model encoder to/decoder from MongoDB.
 */
module.exports = class MongoName {
  static encode(params) {
    const encoded = {};
    Object.assign(encoded, params);

    return encoded;
  }

  static decode(params) {
    const decoded = {};
    Object.assign(decoded, params);

    return decoded;
  }
};
