const { Router } = require('express');
const { onRequestSuccess, onRequestError, validateQueryParams } = require('./utils');
const RewardEventAPI = require('../api/reward-event');

const router = Router();

router.get('/calculate-winnings', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress', 'address'])) return;

  RewardEventAPI.calculateWinnings(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/get-withdraw-amounts', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress', 'address'])) return;

  RewardEventAPI.getWithdrawAmounts(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/version', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  RewardEventAPI.version(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/round', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  RewardEventAPI.currentRound(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/result-index', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  RewardEventAPI.currentResultIndex(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/consensus-threshold', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  RewardEventAPI.currentConsensusThreshold(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/arbitration-end-time', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  RewardEventAPI.currentArbitrationEndTime(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/event-metadata', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  RewardEventAPI.eventMetadata(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/centralized-metadata', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  RewardEventAPI.centralizedMetadata(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/config-metadata', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  RewardEventAPI.configMetadata(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/total-bets', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  RewardEventAPI.totalBets(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/did-withdraw', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress', 'address'])) return;

  RewardEventAPI.didWithdraw(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

module.exports = router;
