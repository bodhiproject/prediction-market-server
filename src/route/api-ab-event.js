const { Router } = require('express');
const { onRequestSuccess, onRequestError, validateQueryParams } = require('./utils');
const ABEventAPI = require('../api/ab-event');

const router = Router();

router.get('/max-bets', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  ABEventAPI.maxBets(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/calculate-winnings', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress', 'address'])) return;

  ABEventAPI.calculateWinnings(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/calculate-escrow', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  ABEventAPI.calculateEscrowAndUnpaidBet(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/get-withdraw-amounts', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress', 'address'])) return;

  ABEventAPI.getWithdrawAmounts(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/version', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  ABEventAPI.version(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/round', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  ABEventAPI.currentRound(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/result-index', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  ABEventAPI.currentResultIndex(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/consensus-threshold', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  ABEventAPI.currentConsensusThreshold(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/arbitration-end-time', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  ABEventAPI.currentArbitrationEndTime(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/event-metadata', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  ABEventAPI.eventMetadata(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/centralized-metadata', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  ABEventAPI.centralizedMetadata(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/config-metadata', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  ABEventAPI.configMetadata(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/total-bets', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  ABEventAPI.totalBets(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/did-withdraw', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress', 'address'])) return;

  ABEventAPI.didWithdraw(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

module.exports = router;
