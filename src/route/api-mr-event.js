const { Router } = require('express');
const { onRequestSuccess, onRequestError, validateQueryParams } = require('./utils');
const MREventAPI = require('../api/mr-event');

const router = Router();

router.get('/calculate-winnings', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress', 'address'])) return;

  MREventAPI.calculateWinnings(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/version', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  MREventAPI.version(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/round', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  MREventAPI.currentRound(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/result-index', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  MREventAPI.currentResultIndex(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/consensus-threshold', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  MREventAPI.currentConsensusThreshold(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/arbitration-end-time', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  MREventAPI.currentArbitrationEndTime(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/event-metadata', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  MREventAPI.eventMetadata(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/centralized-metadata', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  MREventAPI.centralizedMetadata(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/config-metadata', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  MREventAPI.configMetadata(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/total-bets', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress'])) return;

  MREventAPI.totalBets(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

router.get('/did-withdraw', (req, res, next) => {
  if (!validateQueryParams(req, res, ['eventAddress', 'address'])) return;

  MREventAPI.didWithdraw(req.query)
    .then((result) => {
      onRequestSuccess(res, result, next);
    }, (err) => {
      onRequestError(res, err, next);
    });
});

module.exports = router;
