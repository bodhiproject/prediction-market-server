const logRoutes = require('./api-log');
const configManagerRoutes = require('./api-config-manager');
const mrEventRoutes = require('./api-mr-event');
const abEventRoutes = require('./api-ab-event');
const rewardEventRoutes = require('./api-reward-event');

module.exports = (app) => {
  app.use('/log', logRoutes);
  app.use('/config-manager', configManagerRoutes);
  app.use('/mr-event', mrEventRoutes);
  app.use('/ab-event', abEventRoutes);
  app.use('/reward-event', rewardEventRoutes);
};
