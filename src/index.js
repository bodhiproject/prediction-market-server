require('dotenv').config();
require('./utils/logger');
require('./utils/client-logger');
const { initConfig } = require('./config');
const { initSync, startSync } = require('./sync');
const { initDB } = require('./db');
const initApi = require('./route');

// Handle unhandled promise rejections in entire app
process.on('unhandledRejection', (reason) => {
  throw reason;
});

initConfig();
initSync();
initDB();
require('./web3'); // eslint-disable-line import/newline-after-import
initApi();
startSync();
