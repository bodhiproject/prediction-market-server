const Web3 = require('web3');
const net = require('net');
const { Config, isMainnet } = require('./config');
const { EVENT_MESSAGE, WEB3_PROVIDER } = require('./constants');
const logger = require('./utils/logger');
const emitter = require('./utils/emitter');

let web3;

/**
 * Returns a Web3 WS provider.
 * @return {WebsocketProvider} Web3 provider
 */
const getWsProvider = () => {
  const provider = new Web3.providers.WebsocketProvider(isMainnet()
    ? Config.WS_PROVIDER_MAINNET
    : Config.WS_PROVIDER_TESTNET);
  provider.on('connect', () => {
    logger.info(isMainnet()
      ? 'Web3 WS connected to Mainnet'
      : 'Web3 WS connected to Testnet');
    emitter.emit(EVENT_MESSAGE.WEB3_CONNECTED);
  });
  provider.on('close', (err) => {
    logger.error('Web3 WS connection closed', { error: err && err.message });
    emitter.emit(EVENT_MESSAGE.WEB3_DISCONNECTED);
  });
  provider.on('error', (err) => {
    logger.error('Web3 WS error', { error: err && err.message });
  });
  return provider;
};

/**
 * Returns a Web3 HTTP provider.
 * @return {IpcProvider} Web3 provider
 */
const getHttpProvider = () => {
  const provider = new Web3.providers.HttpProvider(isMainnet()
    ? Config.HTTP_PROVIDER_MAINNET
    : Config.HTTP_PROVIDER_TESTNET);
  logger.info(isMainnet()
    ? 'Web3 HTTP connected to Mainnet'
    : 'Web3 HTTP connected to Testnet');
  emitter.emit(EVENT_MESSAGE.WEB3_CONNECTED);
  return provider;
};

/**
 * Returns a Web3 IPC provider.
 * @return {IpcProvider} Web3 provider
 */
const getIpcProvider = () => {
  const provider = new Web3.providers.IpcProvider(
    isMainnet() ? Config.IPC_PROVIDER_MAINNET : Config.IPC_PROVIDER_TESTNET,
    net,
  );
  provider.on('connect', () => {
    logger.info(isMainnet()
      ? 'Web3 IPC connected to Mainnet'
      : 'Web3 IPC connected to Testnet');
    emitter.emit(EVENT_MESSAGE.WEB3_CONNECTED);
  });
  provider.on('close', (err) => {
    logger.error('Web3 IPC connection closed', { error: err && err.message });
    emitter.emit(EVENT_MESSAGE.WEB3_DISCONNECTED);
  });
  provider.on('error', (err) => {
    logger.error('Web3 IPC error', { error: err && err.message });
  });
  return provider;
};

if (process.env.TEST_ENV !== 'true') {
  switch (Config.PROVIDER) {
    case WEB3_PROVIDER.WS: {
      web3 = new Web3(getWsProvider());
      break;
    }
    case WEB3_PROVIDER.HTTP: {
      web3 = new Web3(getHttpProvider());
      break;
    }
    default: {
      web3 = new Web3(getIpcProvider());
      break;
    }
  }
} else {
  web3 = new Web3();
}

module.exports = web3;
