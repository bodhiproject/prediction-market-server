const { isUndefined, map } = require('lodash');
const { isMainnet, configManagerMeta } = require('../config');
const web3 = require('../web3');
const logger = require('../utils/logger');
const { getContract } = require('../utils/web3-utils');

const getConfigContract = () => {
  const metadata = configManagerMeta(2);
  const address = isMainnet() ? metadata.mainnet : metadata.testnet;
  return getContract(metadata.abi, address);
};

module.exports = {
  async version() {
    try {
      const res = await getConfigContract().methods.version().call();
      return web3.utils.toBN(res).toNumber();
    } catch (err) {
      logger.error(`Error ConfigManager.version(): ${err.message}`);
      throw err;
    }
  },

  async getAddress(args) {
    try {
      const { key } = args;
      if (isUndefined(key)) throw TypeError('missing query param: key');

      return getConfigContract().methods.getAddress(web3.utils.toHex(key)).call();
    } catch (err) {
      logger.error(`Error ConfigManager.getAddress(): ${err.message}`);
      throw err;
    }
  },

  async eventEscrowAmount() {
    try {
      const res = await getConfigContract().methods.eventEscrowAmount().call();
      return web3.utils.toBN(res).toString(10);
    } catch (err) {
      logger.error(`Error ConfigManager.eventEscrowAmount(): ${err.message}`);
      throw err;
    }
  },

  async arbitrationLength() {
    try {
      const res = await getConfigContract().methods.arbitrationLength().call();
      return map(res, len => web3.utils.toBN(len).toNumber());
    } catch (err) {
      logger.error(`Error ConfigManager.arbitrationLength(): ${err.message}`);
      throw err;
    }
  },

  async startingConsensusThreshold() {
    try {
      const res = await getConfigContract().methods.startingConsensusThreshold().call();
      return map(res, len => web3.utils.toBN(len).toString(10));
    } catch (err) {
      logger.error(`Error ConfigManager.startingOracleThreshold(): ${err.message}`);
      throw err;
    }
  },

  async thresholdPercentIncrease() {
    try {
      const res = await getConfigContract().methods.thresholdPercentIncrease().call();
      return web3.utils.toBN(res).toNumber();
    } catch (err) {
      logger.error(`Error ConfigManager.thresholdPercentIncrease(): ${err.message}`);
      throw err;
    }
  },
};
