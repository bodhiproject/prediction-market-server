const { isNull, isUndefined, map } = require('lodash');
const { abEventMeta } = require('../config');
const web3 = require('../web3');
const logger = require('../utils/logger');
const { getContract } = require('../utils/web3-utils');
const EventsDB = require('../db/controllers/events');

const getEventContract = async (eventAddress) => {
  const event = await EventsDB.findOne({ address: eventAddress });
  if (isNull(event)) throw Error('Event not found');

  const metadata = abEventMeta(event.version);
  return getContract(metadata.abi, eventAddress);
};

module.exports = {
  async maxBets(args) {
    try {
      const { eventAddress } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');

      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.maxBets().call();
      return map(res, item => web3.utils.toBN(item).toString(10));
    } catch (err) {
      logger.error(`Error ABEvent.maxBets(): ${err.message}`);
      throw err;
    }
  },

  async calculateWinnings(args) {
    try {
      const { eventAddress, address } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');
      if (isUndefined(address)) throw TypeError('address is not defined');

      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.calculateWinnings(address).call();
      return web3.utils.toBN(res).toString(10);
    } catch (err) {
      logger.error(`Error ABEvent.calculateWinnings(): ${err.message}`);
      throw err;
    }
  },

  // get how many escrow left the creator can withdraw
  async calculateEscrowAndUnpaidBet(args) {
    try {
      const { eventAddress } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');

      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.calculateEscrowAndUnpaidBet().call();
      return map(res, item => web3.utils.toBN(item).toString(10));
    } catch (err) {
      logger.error(`Error ABEvent.calculateEscrowAndUnpaidBet(): ${err.message}`);
      throw err;
    }
  },

  async getWithdrawAmounts(args) {
    try {
      const { eventAddress, address } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');
      if (isUndefined(address)) throw TypeError('address is not defined');

      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.getWithdrawAmounts(address).call();
      return map(res, item => web3.utils.toBN(item).toString(10));
    } catch (err) {
      logger.error(`Error ABEvent.getWithdrawAmounts(): ${err.message}`);
      throw err;
    }
  },

  async version(args) {
    try {
      const { eventAddress } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');

      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.version().call();
      return web3.utils.toBN(res).toNumber();
    } catch (err) {
      logger.error(`Error ABEvent.version(): ${err.message}`);
      throw err;
    }
  },

  async currentRound(args) {
    try {
      const { eventAddress } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');

      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.currentRound().call();
      return web3.utils.toBN(res).toNumber();
    } catch (err) {
      logger.error(`Error ABEvent.currentRound(): ${err.message}`);
      throw err;
    }
  },

  async currentResultIndex(args) {
    try {
      const { eventAddress } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');

      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.currentResultIndex().call();
      return web3.utils.toBN(res).toNumber();
    } catch (err) {
      logger.error(`Error ABEvent.currentResultIndex(): ${err.message}`);
      throw err;
    }
  },

  async currentConsensusThreshold(args) {
    try {
      const { eventAddress } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');

      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.currentConsensusThreshold().call();
      return web3.utils.toBN(res).toString(10);
    } catch (err) {
      logger.error(`Error ABEvent.currentConsensusThreshold(): ${err.message}`);
      throw err;
    }
  },

  async currentArbitrationEndTime(args) {
    try {
      const { eventAddress } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');

      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.currentArbitrationEndTime().call();
      return web3.utils.toBN(res).toString(10);
    } catch (err) {
      logger.error(`Error ABEvent.currentArbitrationEndTime(): ${err.message}`);
      throw err;
    }
  },

  async eventMetadata(args) {
    try {
      const { eventAddress } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');

      const { utils: { toBN, hexToUtf8 } } = web3;
      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.eventMetadata().call();
      return [
        toBN(res[0]).toNumber(),
        res[1],
        map(res[2], item => hexToUtf8(item)),
        toBN(res[3]).toNumber(),
        map(res[4], item => toBN(item).toNumber()),
        map(res[5], item => toBN(item).toNumber()),
      ];
    } catch (err) {
      logger.error(`Error ABEvent.eventMetadata(): ${err.message}`);
      throw err;
    }
  },

  async centralizedMetadata(args) {
    try {
      const { eventAddress } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');

      const { utils: { toBN } } = web3;
      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.centralizedMetadata().call();
      return [
        res[0],
        toBN(res[1]).toNumber(),
        toBN(res[2]).toNumber(),
        toBN(res[3]).toNumber(),
        toBN(res[4]).toNumber(),
      ];
    } catch (err) {
      logger.error(`Error ABEvent.centralizedMetadata(): ${err.message}`);
      throw err;
    }
  },

  async configMetadata(args) {
    try {
      const { eventAddress } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');

      const { utils: { toBN } } = web3;
      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.configMetadata().call();
      return [
        toBN(res[0]).toString(10),
        toBN(res[1]).toNumber(),
        toBN(res[2]).toNumber(),
        toBN(res[3]).toNumber(),
      ];
    } catch (err) {
      logger.error(`Error ABEvent.configMetadata(): ${err.message}`);
      throw err;
    }
  },

  async totalBets(args) {
    try {
      const { eventAddress } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');

      const contract = await getEventContract(eventAddress);
      const res = await contract.methods.totalBets().call();
      return web3.utils.toBN(res).toString(10);
    } catch (err) {
      logger.error(`Error ABEvent.totalBets(): ${err.message}`);
      throw err;
    }
  },

  async didWithdraw(args) {
    try {
      const { eventAddress, address } = args;
      if (isUndefined(eventAddress)) throw TypeError('eventAddress is not defined');
      if (isUndefined(address)) throw TypeError('address is not defined');

      const contract = await getEventContract(eventAddress);
      return contract.methods.didWithdraw(address).call();
    } catch (err) {
      logger.error(`Error ABEvent.didWithdraw(): ${err.message}`);
      throw err;
    }
  },
};
