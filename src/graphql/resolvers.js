const { sum, fill, each, isNull, filter } = require('lodash');
const Queries = require('./queries');
const Mutations = require('./mutations');
const { TX_TYPE, TX_STATUS } = require('../constants');
const pubsub = require('../route/pubsub');
const BlocksDB = require('../db/controllers/blocks');
const TxReceiptsDB = require('../db/controllers/tx-receipts');
const NamesDB = require('../db/controllers/names');
const EventsDB = require('../db/controllers/events');
const BetsDB = require('../db/controllers/bets');
const ResultSetsDB = require('../db/controllers/result-sets');
const WithdrawsDB = require('../db/controllers/withdraws');
const { sumBN, sumArrayBN } = require('../utils/web3-utils');
const { toLowerCase } = require('../utils/index');

module.exports = {
  Query: Queries,
  Mutation: Mutations,
  Subscription: {
    onSyncInfo: { subscribe: () => pubsub.asyncIterator('onSyncInfo') },
  },

  Transaction: {
    __resolveType: (tx) => {
      switch (tx.txType) {
        case TX_TYPE.CREATE_EVENT: return 'Event';
        case TX_TYPE.DEPOSIT: return 'Deposit';
        case TX_TYPE.BET: return 'Bet';
        case TX_TYPE.RESULT_SET: return 'ResultSet';
        case TX_TYPE.VOTE: return 'Bet';
        case TX_TYPE.WITHDRAW: return 'Withdraw';
        default: return null;
      }
    },
  },

  Event: {
    txReceipt: async ({ txid }) => TxReceiptsDB.findOne({ transactionHash: txid }),
    block: async ({ blockNum }) => BlocksDB.findOne({ blockNum }),
    pendingTxs: async ({ address }, args, { pendingTxsAddress }) => {
      if (pendingTxsAddress) {
        const bet = await BetsDB.count({
          txStatus: TX_STATUS.PENDING,
          eventAddress: address,
          betterAddress: pendingTxsAddress,
        });
        const resultSet = await ResultSetsDB.count({
          txStatus: TX_STATUS.PENDING,
          eventAddress: address,
          centralizedOracleAddress: pendingTxsAddress,
        });
        const withdraw = await WithdrawsDB.count({
          txStatus: TX_STATUS.PENDING,
          eventAddress: address,
          winnerAddress: pendingTxsAddress,
        });
        return {
          bet,
          resultSet,
          withdraw,
          total: sum([bet, resultSet, withdraw]),
        };
      }
      return null;
    },
    roundBets: async (
      { address, currentRound, numOfResults },
      args,
      { includeRoundBets, roundBetsAddress },
    ) => {
      if (includeRoundBets) {
        const singleTotalRoundBets = [];
        const singleUserRoundBets = [];
        for (let i = 0; i <= currentRound; i++) {
          singleTotalRoundBets.push(fill(Array(numOfResults), '0'));
          singleUserRoundBets.push(fill(Array(numOfResults), '0'));
        }

        let bets = await BetsDB.find({
          txStatus: TX_STATUS.SUCCESS,
          eventAddress: address,
        });
        // Add result set amount to first round
        const resultSet = await ResultSetsDB.findOne({
          txStatus: TX_STATUS.SUCCESS,
          eventAddress: address,
          eventRound: 0,
        });
        if (resultSet) {
          // Change result set fields to match bet fields for easier parsing
          resultSet.betterAddress = resultSet.centralizedOracleAddress;
          resultSet.eventRound = 1;
          bets = bets.concat(resultSet);
        }
        // Filter out any bets greater than currentRound
        bets = filter(bets, bet => bet.eventRound <= currentRound);

        each(bets, (bet) => {
          // Add to total round bets
          singleTotalRoundBets[bet.eventRound][bet.resultIndex] = sumBN(
            singleTotalRoundBets[bet.eventRound][bet.resultIndex],
            bet.amount,
          ).toString(10);

          // Add to user round bets if address was defined
          if (roundBetsAddress && toLowerCase(roundBetsAddress) === bet.betterAddress) {
            singleUserRoundBets[bet.eventRound][bet.resultIndex] = sumBN(
              singleUserRoundBets[bet.eventRound][bet.resultIndex],
              bet.amount,
            ).toString(10);
          }
        });

        return {
          singleUserRoundBets,
          singleTotalRoundBets,
        };
      }
      return null;
    },
    previousConsensusThreshold: async ({ address, currentRound }) => {
      if (currentRound >= 1) {
        const resultSet = await ResultSetsDB.findOne({
          txStatus: TX_STATUS.SUCCESS,
          eventAddress: address,
          eventRound: currentRound - 1,
        });
        return resultSet.amount;
      }
      return null;
    },
    ownerName: async ({ ownerAddress }) => {
      const nameEntry = await NamesDB.findOne({ address: ownerAddress });
      return nameEntry && nameEntry.name;
    },
    // TODO: add field in db that accumulates
    totalBets: async ({ address }) => {
      const bets = await BetsDB.find({
        txStatus: TX_STATUS.SUCCESS,
        eventAddress: address,
      });
      const amounts = [];
      each(bets, bet => amounts.push(bet.amount));

      // Add result set amount
      const resultSet = await ResultSetsDB.findOne({
        txStatus: TX_STATUS.SUCCESS,
        eventAddress: address,
        eventRound: 0,
      });
      if (!isNull(resultSet)) amounts.push(resultSet.amount);

      return sumArrayBN(amounts).toString(10);
    },
  },

  Deposit: {
    txReceipt: async ({ txid }) => TxReceiptsDB.findOne({ transactionHash: txid }),
    block: async ({ blockNum }) => BlocksDB.findOne({ blockNum }),
    eventName: async ({ eventAddress }) => {
      const event = await EventsDB.findOne({ address: eventAddress });
      return event && event.name;
    },
    depositorName: async ({ depositorAddress }) => {
      const nameEntry = await NamesDB.findOne({ address: depositorAddress });
      return nameEntry && nameEntry.name;
    },
  },

  Bet: {
    txReceipt: async ({ txid }) => TxReceiptsDB.findOne({ transactionHash: txid }),
    block: async ({ blockNum }) => BlocksDB.findOne({ blockNum }),
    resultName: async ({ eventAddress, resultIndex }) => {
      const event = await EventsDB.findOne({ address: eventAddress });
      return event && event.results[resultIndex];
    },
    eventName: async ({ eventAddress }) => {
      const event = await EventsDB.findOne({ address: eventAddress });
      return event && event.name;
    },
    betterName: async ({ betterAddress }) => {
      const nameEntry = await NamesDB.findOne({ address: betterAddress });
      return nameEntry && nameEntry.name;
    },
  },

  ResultSet: {
    txReceipt: async ({ txid }) => TxReceiptsDB.findOne({ transactionHash: txid }),
    block: async ({ blockNum }) => BlocksDB.findOne({ blockNum }),
    resultName: async ({ eventAddress, resultIndex }) => {
      const event = await EventsDB.findOne({ address: eventAddress });
      return event && event.results[resultIndex];
    },
    eventName: async ({ eventAddress }) => {
      const event = await EventsDB.findOne({ address: eventAddress });
      return event && event.name;
    },
    centralizedOracleName: async ({ centralizedOracleAddress }) => {
      const nameEntry = await NamesDB.findOne({ address: centralizedOracleAddress });
      return nameEntry && nameEntry.name;
    },
  },

  Withdraw: {
    txReceipt: async ({ txid }) => TxReceiptsDB.findOne({ transactionHash: txid }),
    block: async ({ blockNum }) => BlocksDB.findOne({ blockNum }),
    eventName: async ({ eventAddress }) => {
      const event = await EventsDB.findOne({ address: eventAddress });
      return event && event.name;
    },
    winnerName: async ({ winnerAddress }) => {
      const nameEntry = await NamesDB.findOne({ address: winnerAddress });
      return nameEntry && nameEntry.name;
    },
  },

  LeaderboardEntry: {
    userName: async ({ userAddress }) => {
      const nameEntry = await NamesDB.findOne({ address: userAddress });
      return nameEntry && nameEntry.name;
    },
  },
};
