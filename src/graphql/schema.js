module.exports = `

enum OrderDirection {
  DESC
  ASC
}

enum EventType {
  MR_EVENT
  AB_EVENT
  REWARD_EVENT
}

enum EventStatus {
  CREATED
  PRE_BETTING
  BETTING
  PRE_RESULT_SETTING
  ORACLE_RESULT_SETTING
  OPEN_RESULT_SETTING
  ARBITRATION
  WITHDRAWING
}

enum TransactionType {
  CREATE_EVENT
  BET
  RESULT_SET
  VOTE
  WITHDRAW
  DEPOSIT
}

enum TransactionStatus {
  PENDING
  SUCCESS
  FAIL
}

interface Transaction {
  txType: TransactionType!
  txid: String!
  txStatus: TransactionStatus!
  txReceipt: TransactionReceipt
  blockNum: Int
  block: Block
}

type Block {
  blockNum: Int!
  blockTime: Int!
}

type PendingTransactions {
  bet: Int!
  resultSet: Int!
  withdraw: Int!
  total: Int!
}

type TransactionReceipt {
  status: Boolean
  blockHash: String
  blockNum: Int
  transactionHash: String!
  from: String!
  to: String
  contractAddress: String
  cumulativeGasUsed: Int
  gasUsed: Int!
  gasPrice: String
}

type PageInfo {
  hasNextPage: Boolean!
  pageNumber: Int!
  count: Int!
  nextTransactionSkips: NextTransactionSkips
}

type PaginatedTransactions {
  totalCount: Int!
  pageInfo: PageInfo
  items: [Transaction]!
}

type NextTransactionSkips {
  nextEventSkip: Int!
  nextDepositSkip: Int!
  nextBetSkip: Int!
  nextResultSetSkip: Int!
  nextWithdrawSkip: Int!
}

type RoundBets {
  singleTotalRoundBets: [[String!]]
  singleUserRoundBets: [[String!]]
}

type Event implements Transaction {
  txType: TransactionType!
  txid: String!
  txStatus: TransactionStatus!
  txReceipt: TransactionReceipt
  blockNum: Int
  block: Block
  address: String
  ownerAddress: String!
  ownerName: String
  version: Int
  name: String!
  results: [String!]!
  numOfResults: Int!
  winningChances: [Int!]
  odds: [Float!]
  centralizedOracle: String!
  betStartTime: Int
  betEndTime: Int!
  resultSetStartTime: Int!
  resultSetEndTime: Int
  escrowAmount: String
  arbitrationLength: Int
  arbitrationRewardPercentage: Int
  thresholdPercentIncrease: Int
  currentRound: Int
  currentResultIndex: Int
  consensusThreshold: String
  previousConsensusThreshold: String
  arbitrationEndTime: Int
  status: EventStatus!
  language: String!
  eventType: EventType!
  pendingTxs: PendingTransactions
  roundBets: RoundBets
  totalBets: String
  withdrawnList: [String]
  maxBets: [String]
}

type PaginatedEvents {
  totalCount: Int!
  pageInfo: PageInfo
  items: [Event]!
}

type Deposit implements Transaction {
  txType: TransactionType!
  txid: String!
  txStatus: TransactionStatus!
  txReceipt: TransactionReceipt
  blockNum: Int
  block: Block
  eventAddress: String!
  depositorAddress: String!
  depositorName: String
  amount: String!
  eventName: String
  eventType: EventType!
}

type PaginatedDeposits {
  totalCount: Int!
  pageInfo: PageInfo
  items: [Deposit]!
}

type Bet implements Transaction {
  txType: TransactionType!
  txid: String!
  txStatus: TransactionStatus!
  txReceipt: TransactionReceipt
  blockNum: Int
  block: Block
  eventAddress: String!
  betterAddress: String!
  betterName: String
  resultIndex: Int!
  amount: String!
  eventRound: Int!
  resultName: String
  eventName: String
  eventType: EventType!
}

type PaginatedBets {
  totalCount: Int!
  pageInfo: PageInfo
  items: [Bet]!
}

type ResultSet implements Transaction {
  txType: TransactionType!
  txid: String!
  txStatus: TransactionStatus!
  txReceipt: TransactionReceipt
  blockNum: Int
  block: Block
  eventAddress: String!
  centralizedOracleAddress: String
  centralizedOracleName: String
  resultIndex: Int!
  amount: String!
  eventRound: Int!
  nextConsensusThreshold: String
  nextArbitrationEndTime: Int
  resultName: String
  eventName: String
  eventType: EventType!
}

type PaginatedResultSets {
  totalCount: Int!
  pageInfo: PageInfo
  items: [ResultSet]!
}

type Withdraw implements Transaction {
  txType: TransactionType!
  txid: String!
  txStatus: TransactionStatus!
  txReceipt: TransactionReceipt
  blockNum: Int
  block: Block
  eventAddress: String!
  winnerAddress: String!
  winnerName: String
  winningAmount: String!
  creatorReturnAmount: String!
  eventName: String
  eventType: EventType!
}

type PaginatedWithdraws {
  totalCount: Int!
  pageInfo: PageInfo
  items: [Withdraw]!
}

type SyncInfo {
  syncBlockNum: Int
  syncBlockTime: Int
  syncPercent: Int
}

type TotalResultBets {
  totalBets: [String]!
  betterBets: [String]
  totalVotes: [String]
  betterVotes: [String]
}

type AllStats {
  eventCount: String!
  participantCount: String!
  totalBets: String!
}

type LeaderboardEntry {
  eventAddress: String
  userAddress: String!
  userName: String
  investments: String!
  winnings: String!
  returnRatio: Float!
}

type PaginatedLeaderboardEntry {
  totalCount: Int!
  pageInfo: PageInfo
  items: [LeaderboardEntry]!
}

input Order {
  field: String!
  direction: OrderDirection!
}

input EventFilter {
  OR: [EventFilter!]
  txid: String
  txStatus: TransactionStatus
  address: String
  ownerAddress: String
  versions: [Int]
  centralizedOracle: String
  currentRound: Int
  currentResultIndex: Int
  status: EventStatus
  language: String
  eventType: EventType
  excludeCentralizedOracle: String
}

input SearchEventsFilter {
  OR: [SearchEventsFilter!]
  txStatus: TransactionStatus
  version: Int
  status: EventStatus
  language: String
  eventType: EventType
}

input WithdrawableEventFilter {
  versions: [Int]
  language: String
  eventType: EventType
  withdrawerAddress: String!
}

input DepositFilter {
  OR: [DepositFilter!]
  txid: String
  txStatus: TransactionStatus
  eventAddress: String
  depositorAddress: String
  eventType: EventType
}

input BetFilter {
  OR: [BetFilter!]
  txid: String
  txStatus: TransactionStatus
  eventAddress: String
  betterAddress: String
  resultIndex: Int
  eventRound: Int
  eventType: EventType
}

input ResultSetFilter {
  OR: [ResultSetFilter!]
  txid: String
  txStatus: TransactionStatus
  eventAddress: String
  centralizedOracleAddress: String
  resultIndex: Int
  eventRound: Int
  eventType: EventType
}

input WithdrawFilter {
  OR: [WithdrawFilter!]
  txid: String
  txStatus: TransactionStatus
  eventAddress: String
  winnerAddress: String
  eventType: EventType
}

input TransactionFilter {
  eventAddress: String
  transactorAddress: String
  txStatus: TransactionStatus
  eventType: EventType
}

input TransactionSkips {
  eventSkip: Int!
  depositSkip: Int!
  betSkip: Int!
  resultSetSkip: Int!
  withdrawSkip: Int!
}

input TotalResultBetsFilter {
  eventAddress: String!
  betterAddress: String
}

input LeaderboardEntryFilter {
  userAddress: String
  eventAddress: String
}

type Query {
  events(
    filter: EventFilter
    orderBy: [Order!]
    limit: Int
    skip: Int
    pendingTxsAddress: String
    includeRoundBets: Boolean
    roundBetsAddress: String
  ): PaginatedEvents!

  searchEvents(
    filter: SearchEventsFilter
    orderBy: [Order!]
    limit: Int
    skip: Int
    searchPhrase: String
    includeRoundBets: Boolean
    roundBetsAddress: String
  ): [Event]!

  withdrawableEvents(
    filter: WithdrawableEventFilter!
    orderBy: [Order!]
    limit: Int
    skip: Int
    includeRoundBets: Boolean
    roundBetsAddress: String
  ): PaginatedEvents!

  deposits(
    filter: DepositFilter
    orderBy: [Order!]
    limit: Int
    skip: Int
  ): PaginatedDeposits!

  bets(
    filter: BetFilter
    orderBy: [Order!]
    limit: Int
    skip: Int
  ): PaginatedBets!

  resultSets(
    filter: ResultSetFilter
    orderBy: [Order!]
    limit: Int
    skip: Int
  ): PaginatedResultSets!

  withdraws(
    filter: WithdrawFilter
    orderBy: [Order!]
    limit: Int
    skip: Int
  ): PaginatedWithdraws!

  transactions(
    filter: TransactionFilter
    limit: Int
    skip: Int
    transactionSkips: TransactionSkips
  ): PaginatedTransactions!

  syncInfo: SyncInfo!

  totalResultBets(
    filter: TotalResultBetsFilter
  ): TotalResultBets!

  allStats: AllStats!

  eventLeaderboardEntries(
    filter: LeaderboardEntryFilter
    orderBy: [Order!]
    limit: Int
    skip: Int
  ): PaginatedLeaderboardEntry!

  globalLeaderboardEntries(
    filter: LeaderboardEntryFilter
    orderBy: [Order!]
    limit: Int
    skip: Int
  ): PaginatedLeaderboardEntry!
}

type Mutation {
  addPendingEvent(
    txid: String!
    ownerAddress: String!
    name: String!
    results: [String!]!
    numOfResults: Int!
    centralizedOracle: String!
    betEndTime: Int!
    resultSetStartTime: Int!
    language: String!
    eventType: EventType!
  ): Event!

  addPendingBet(
    txid: String!
    eventAddress: String!
    betterAddress: String!
    resultIndex: Int!
    amount: String!
    eventRound: Int!
    eventType: EventType!
  ): Bet!

  addPendingDeposit(
    txid: String!
    eventAddress: String!
    depositorAddress: String!
    amount: String!
    eventType: EventType!
  ): Deposit!

  addPendingResultSet(
    txid: String!
    eventAddress: String!
    centralizedOracleAddress: String!
    resultIndex: Int!
    amount: String!
    eventRound: Int!
    eventType: EventType!
  ): ResultSet!

  addPendingWithdraw(
    txid: String!
    eventAddress: String!
    winnerAddress: String!
    winningAmount: String!
    creatorReturnAmount: String!
    eventType: EventType!
  ): Withdraw!
}

type Subscription {
  onSyncInfo: SyncInfo
}
`;
