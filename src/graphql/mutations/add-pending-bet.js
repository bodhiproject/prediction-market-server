const { isNull } = require('lodash');
const Bet = require('../../models/bet');
const logger = require('../../utils/logger');
const { getTransaction } = require('../../utils/web3-utils');
const BetsDB = require('../../db/controllers/bets');
const TxReceiptsDB = require('../../db/controllers/tx-receipts');
const { TX_STATUS } = require('../../constants');

module.exports = async (root, data) => {
  const {
    txid,
    eventAddress,
    betterAddress,
    resultIndex,
    amount,
    eventRound,
    eventType,
  } = data;

  // Verify not already existing
  const existing = await BetsDB.findOne({ txid });
  if (!isNull(existing)) throw Error('Bet already exists');

  // Fetch transaction info and insert
  const txReceipt = await getTransaction(txid);
  await TxReceiptsDB.updateOne(txReceipt);

  const bet = new Bet({
    txid,
    txStatus: TX_STATUS.PENDING,
    blockNum: txReceipt.blockNum,
    eventAddress,
    betterAddress,
    resultIndex,
    amount,
    eventRound,
    eventType,
  });
  await BetsDB.updateOne(bet);
  logger.debug(`Mutation addPendingBet txid:${txid}`);

  return bet;
};
