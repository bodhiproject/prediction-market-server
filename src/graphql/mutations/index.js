const addPendingEvent = require('./add-pending-event');
const addPendingBet = require('./add-pending-bet');
const addPendingResultSet = require('./add-pending-result-set');
const addPendingWithdraw = require('./add-pending-withdraw');
const addPendingDeposit = require('./add-pending-deposit');

module.exports = {
  addPendingEvent,
  addPendingBet,
  addPendingResultSet,
  addPendingWithdraw,
  addPendingDeposit,
};
