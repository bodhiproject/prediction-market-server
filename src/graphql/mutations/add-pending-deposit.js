const { isNull } = require('lodash');
const Deposit = require('../../models/deposit');
const logger = require('../../utils/logger');
const { getTransaction } = require('../../utils/web3-utils');
const DepositsDB = require('../../db/controllers/deposits');
const TxReceiptsDB = require('../../db/controllers/tx-receipts');
const { TX_STATUS } = require('../../constants');

module.exports = async (root, data) => {
  const {
    txid,
    eventAddress,
    depositorAddress,
    amount,
    eventType,
  } = data;

  // Verify not already existing
  const existing = await DepositsDB.findOne({ txid });
  if (!isNull(existing)) throw Error('Deposit already exists');

  // Fetch transaction info and insert
  const txReceipt = await getTransaction(txid);
  await TxReceiptsDB.updateOne(txReceipt);

  const deposit = new Deposit({
    txid,
    txStatus: TX_STATUS.PENDING,
    blockNum: txReceipt.blockNum,
    eventAddress,
    depositorAddress,
    amount,
    eventType,
  });
  await DepositsDB.updateOne(deposit);
  logger.debug(`Mutation addPendingDeposit txid:${txid}`);

  return deposit;
};
