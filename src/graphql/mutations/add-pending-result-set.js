const { isNull } = require('lodash');
const ResultSet = require('../../models/result-set');
const logger = require('../../utils/logger');
const { getTransaction } = require('../../utils/web3-utils');
const TxReceiptsDB = require('../../db/controllers/tx-receipts');
const ResultSetsDB = require('../../db/controllers/result-sets');
const { TX_STATUS } = require('../../constants');

module.exports = async (root, data) => {
  const {
    txid,
    eventAddress,
    centralizedOracleAddress,
    resultIndex,
    amount,
    eventRound,
    eventType,
  } = data;

  // Verify not already existing
  const existing = await ResultSetsDB.findOne({ txid });
  if (!isNull(existing)) throw Error('ResultSet already exists');

  // Fetch transaction info and insert
  const txReceipt = await getTransaction(txid);
  await TxReceiptsDB.updateOne(txReceipt);

  const resultSet = new ResultSet({
    txid,
    txStatus: TX_STATUS.PENDING,
    blockNum: txReceipt.blockNum,
    eventAddress,
    centralizedOracleAddress,
    resultIndex,
    amount,
    eventRound,
    eventType,
  });
  await ResultSetsDB.updateOne(resultSet);
  logger.debug(`Mutation addPendingResultSet txid:${txid}`);

  return resultSet;
};
