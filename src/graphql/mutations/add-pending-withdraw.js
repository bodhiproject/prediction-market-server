const { isNull } = require('lodash');
const Withdraw = require('../../models/withdraw');
const logger = require('../../utils/logger');
const { getTransaction } = require('../../utils/web3-utils');
const TxReceiptsDB = require('../../db/controllers/tx-receipts');
const WithdrawsDB = require('../../db/controllers/withdraws');
const { TX_STATUS } = require('../../constants');

module.exports = async (root, data) => {
  const {
    txid,
    eventAddress,
    winnerAddress,
    winningAmount,
    creatorReturnAmount,
    eventType,
  } = data;

  // Verify not already existing
  const existing = await WithdrawsDB.findOne({ txid });
  if (!isNull(existing)) throw Error('Withdraw already exists');

  // Fetch transaction info and insert
  const txReceipt = await getTransaction(txid);
  await TxReceiptsDB.updateOne(txReceipt);

  const withdraw = new Withdraw({
    txid,
    txStatus: TX_STATUS.PENDING,
    blockNum: txReceipt.blockNum,
    eventAddress,
    winnerAddress,
    winningAmount,
    creatorReturnAmount,
    eventType,
  });
  await WithdrawsDB.updateOne(withdraw);
  logger.debug(`Mutation addPendingWithdraw txid:${txid}`);

  return withdraw;
};
