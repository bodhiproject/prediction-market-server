const { isNull } = require('lodash');
const { TX_STATUS, EVENT_STATUS } = require('../../constants');
const logger = require('../../utils/logger');
const { getTransaction } = require('../../utils/web3-utils');
const TxReceiptsDB = require('../../db/controllers/tx-receipts');
const EventsDB = require('../../db/controllers/events');
const Event = require('../../models/event');

module.exports = async (root, data) => {
  const {
    txid,
    ownerAddress,
    name,
    results,
    numOfResults,
    centralizedOracle,
    betEndTime,
    resultSetStartTime,
    language,
    eventType,
  } = data;

  // Verify not already existing
  const existing = await EventsDB.findOne({ txid });
  if (!isNull(existing)) throw Error('Event already exists');

  // Fetch transaction info and insert
  const txReceipt = await getTransaction(txid);
  await TxReceiptsDB.updateOne(txReceipt);

  const event = new Event({
    txid,
    txStatus: TX_STATUS.PENDING,
    blockNum: txReceipt.blockNum,
    ownerAddress,
    name,
    results,
    numOfResults,
    centralizedOracle,
    betEndTime,
    resultSetStartTime,
    status: EVENT_STATUS.CREATED,
    language,
    eventType,
  });
  await EventsDB.updateOne(event);
  logger.debug(`Mutation addPendingEvent txid:${txid}`);

  return event;
};
