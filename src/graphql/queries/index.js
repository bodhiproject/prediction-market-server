const events = require('./events');
const searchEvents = require('./search-events');
const deposits = require('./deposits');
const bets = require('./bets');
const resultSets = require('./result-sets');
const withdraws = require('./withdraws');
const transactions = require('./transactions');
const syncInfo = require('./sync-info');
const totalResultBets = require('./total-result-bets');
const allStats = require('./all-stats');
const withdrawableEvents = require('./withdrawable-events');
const { eventLeaderboardEntries, globalLeaderboardEntries } = require('./leaderboard');

module.exports = {
  events,
  searchEvents,
  deposits,
  bets,
  resultSets,
  withdraws,
  transactions,
  syncInfo,
  totalResultBets,
  allStats,
  withdrawableEvents,
  eventLeaderboardEntries,
  globalLeaderboardEntries,
};
