const { uniq } = require('lodash');
const { TX_STATUS } = require('../../constants');

module.exports = async (
  parent,
  {}, // eslint-disable-line no-empty-pattern
  { controllers: { EventsDB, BetsDB, ResultSetsDB } },
) => {
  const eventCount = await EventsDB.count({});
  const uniqueBets = await BetsDB.distinct('betterAddress');
  const uniqueSets = await ResultSetsDB.distinct('centralizedOracleAddress', {
    txStatus: TX_STATUS.SUCCESS,
    eventRound: 0,
  });
  const participantCount = uniq(uniqueBets.concat(uniqueSets)).length;
  const totalBets = await BetsDB.aggregateAmount();

  return {
    eventCount,
    participantCount,
    totalBets,
  };
};
