const { uniqBy, each } = require('lodash');
const { lowercaseFilters, runPaginatedQuery } = require('./utils');
const { EVENT_STATUS } = require('../../constants');

/**
 * Gets all the unique bets for the withdrawer.
 * @param {string} withdrawerAddress Withdrawer address
 * @param {BetsDB} BetsDB BetsDB controller
 * @return {array} Array of unique bets by the withdrawer
 */
const getUniqueBets = async (withdrawerAddress, BetsDB) => {
  if (!withdrawerAddress) throw Error('Must include withdrawerAddress filter');

  let bets = await BetsDB.find(
    { betterAddress: withdrawerAddress },
    { limit: 0 },
  );
  bets = uniqBy(bets, b => [b.eventAddress, b.resultIndex].join());
  return bets;
};

/**
 * Gets all the result sets for event round 0 for the withdrawer.
 * @param {string} withdrawerAddress Withdrawer address
 * @param {ResultSetsDB} ResultSetsDB ResultSetsDB controller
 * @return {array} Result sets by the withdrawer
 */
const getResultSets = async (withdrawerAddress, ResultSetsDB) => {
  if (!withdrawerAddress) throw Error('Must include withdrawerAddress filter');

  const sets = await ResultSetsDB.find(
    { centralizedOracleAddress: withdrawerAddress, eventRound: 0 },
    { limit: 0 },
  );
  return sets;
};

/**
 * Builds all the event filters based on events that have the same winning result index.
 * @param {array} bets Array of unique bets by the withdrawer
 * @param {array} sets Array of result sets by the withdrawer
 * @param {object} filter Filters passed in to the query
 * @return {array} Array of filters for the events query.
 */
const buildFilters = (bets, sets, filter) => {
  const { versions, language, eventType, withdrawerAddress } = filter;
  const filters = [];

  // Add filter to get all owner-created events
  const ownerFilter = {
    status: EVENT_STATUS.WITHDRAWING,
    ownerAddress: withdrawerAddress,
  };
  if (versions) ownerFilter.version = { $in: versions };
  if (language) ownerFilter.language = language;
  if (eventType) ownerFilter.eventType = eventType;
  filters.push(ownerFilter);

  // Add filters for winning bets
  each(bets, (bet) => {
    const currFilter = {
      status: EVENT_STATUS.WITHDRAWING,
      address: bet.eventAddress,
    };
    if (bet.eventRound === 0) {
      // Bet is in betting round.
      // An Invalid final result (0) will refund their bets.
      currFilter.currentResultIndex = { $in: [0, bet.resultIndex] };
    } else {
      // Bet is in voting round.
      currFilter.currentResultIndex = bet.resultIndex;
    }
    if (versions) currFilter.version = { $in: versions };
    if (language) currFilter.language = language;
    if (eventType) currFilter.eventType = eventType;
    filters.push(currFilter);
  });

  // Add filters for winning result sets
  each(sets, (set) => {
    const currFilter = {
      status: EVENT_STATUS.WITHDRAWING,
      address: set.eventAddress,
      currentResultIndex: set.resultIndex,
    };
    if (versions) currFilter.version = { $in: versions };
    if (language) currFilter.language = language;
    if (eventType) currFilter.eventType = eventType;
    filters.push(currFilter);
  });

  return filters;
};

module.exports = async (
  parent,
  { filter, orderBy, limit, skip, includeRoundBets, roundBetsAddress },
  context,
) => {
  const { controllers: { EventsDB, BetsDB, ResultSetsDB } } = context;
  context.includeRoundBets = includeRoundBets;
  context.roundBetsAddress = roundBetsAddress;

  const lowerFilters = lowercaseFilters(filter);
  const bets = await getUniqueBets(lowerFilters.withdrawerAddress, BetsDB);
  const sets = await getResultSets(lowerFilters.withdrawerAddress, ResultSetsDB);
  const query = { $or: buildFilters(bets, sets, lowerFilters) };
  return runPaginatedQuery({
    db: EventsDB,
    query,
    orderBy,
    limit,
    skip,
  });
};
