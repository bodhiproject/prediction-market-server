const { isArray, each } = require('lodash');
const { lowercaseFilters } = require('./utils');

const buildFilters = ({
  OR,
  txStatus,
  version,
  status,
  language,
  eventType,
} = {}) => {
  let filters = [];

  // Handle OR array
  if (isArray(OR)) {
    each(OR, (f) => {
      filters = filters.concat(buildFilters(f));
    });
    return filters;
  }

  // Handle other fields
  const filter = {};
  if (txStatus) filter.txStatus = txStatus;
  if (version) filter.version = version;
  if (status) filter.status = status;
  if (language) filter.language = language;
  if (eventType) filter.eventType = eventType;
  if (Object.keys(filter).length > 0) filters.push(filter);
  return filters;
};

const buildSearchPhrase = (searchPhrase) => {
  if (!searchPhrase) throw Error('Must include searchPhrase');

  const filters = [];
  const searchRegex = new RegExp(`.*${searchPhrase}.*`);
  each(['address', 'name', 'centralizedOracle'], (field) => {
    filters.push({ [field]: { $regex: searchRegex, $options: 'i' } });
  });
  return filters;
};

module.exports = async (
  parent,
  { filter, orderBy, limit, skip, searchPhrase, includeRoundBets, roundBetsAddress },
  context,
) => {
  const { controllers: { EventsDB } } = context;
  context.includeRoundBets = includeRoundBets;
  context.roundBetsAddress = roundBetsAddress;

  const filters = [];
  if (filter) filters.push({ $or: buildFilters(lowercaseFilters(filter)) });
  if (searchPhrase) filters.push({ $or: buildSearchPhrase(searchPhrase) });

  const query = { $and: filters };
  return EventsDB.find(query, { orderBy, limit, skip });
};
