const { concat, orderBy: order, slice } = require('lodash');
const { lowercaseFilters, constructPageInfo } = require('./utils');
const { ORDER_DIRECTION, TX_TYPE } = require('../../constants');

const buildTxFilters = ({
  eventAddress,
  transactorAddress,
  txStatus,
  eventType,
} = {}) => {
  if (!eventAddress && !transactorAddress) {
    throw Error('eventAddress or transactorAddress missing in filters');
  }

  const filter = {};
  if (eventAddress) filter.eventAddress = eventAddress;
  if (transactorAddress) filter.transactorAddress = transactorAddress;
  if (txStatus) filter.txStatus = txStatus;
  if (eventType) filter.eventType = eventType;
  return filter;
};

const buildEventFilters = ({
  eventAddress,
  transactorAddress,
  txStatus,
  eventType,
}) => {
  const filters = {};
  if (eventAddress) filters.address = eventAddress;
  if (transactorAddress) filters.ownerAddress = transactorAddress;
  if (txStatus) filters.txStatus = txStatus;
  if (eventType) filters.eventType = eventType;
  return filters;
};

const buildDepositFilters = ({
  eventAddress,
  transactorAddress,
  txStatus,
  eventType,
}) => {
  const filters = {};
  if (eventAddress) filters.eventAddress = eventAddress;
  if (transactorAddress) filters.depositorAddress = transactorAddress;
  if (txStatus) filters.txStatus = txStatus;
  if (eventType) filters.eventType = eventType;
  return filters;
};

const buildBetFilters = ({
  eventAddress,
  transactorAddress,
  txStatus,
  eventType,
}) => {
  const filters = {};
  if (eventAddress) filters.eventAddress = eventAddress;
  if (transactorAddress) filters.betterAddress = transactorAddress;
  if (txStatus) filters.txStatus = txStatus;
  if (eventType) filters.eventType = eventType;
  return filters;
};

const buildResultSetFilters = ({
  eventAddress,
  transactorAddress,
  txStatus,
  eventType,
}) => {
  const filters = {};
  if (eventAddress) filters.eventAddress = eventAddress;
  if (transactorAddress) filters.centralizedOracleAddress = transactorAddress;
  if (txStatus) filters.txStatus = txStatus;
  if (eventType) filters.eventType = eventType;
  filters.eventRound = 0;
  return filters;
};

const buildWithdrawFilters = ({
  eventAddress,
  transactorAddress,
  txStatus,
  eventType,
}) => {
  const filters = {};
  if (eventAddress) filters.eventAddress = eventAddress;
  if (transactorAddress) filters.winnerAddress = transactorAddress;
  if (txStatus) filters.txStatus = txStatus;
  if (eventType) filters.eventType = eventType;
  return filters;
};

module.exports = async (
  parent,
  {
    filter,
    limit = 10,
    skip = 0,
    transactionSkips: { eventSkip, depositSkip, betSkip, resultSetSkip, withdrawSkip },
  },
  { controllers: { EventsDB, DepositsDB, BetsDB, ResultSetsDB, WithdrawsDB } },
) => {
  const txFilters = buildTxFilters(lowercaseFilters(filter));
  const orderBy = [{ field: 'blockNum', direction: ORDER_DIRECTION.DESCENDING }];
  let totalCount = 0;

  // Run Events query
  const eventFilter = buildEventFilters(txFilters);
  totalCount += await EventsDB.count(eventFilter);
  const events = await EventsDB.find(
    eventFilter,
    { orderBy, skip: eventSkip, limit },
  );

  // Run Deposits query
  const depositFilter = buildDepositFilters(txFilters);
  totalCount += await DepositsDB.count(depositFilter);
  const deposits = await DepositsDB.find(
    depositFilter,
    { orderBy, skip: depositSkip, limit },
  );

  // Run Bets query
  const betFilter = buildBetFilters(txFilters);
  totalCount += await BetsDB.count(betFilter);
  const bets = await BetsDB.find(betFilter, { orderBy, skip: betSkip, limit });

  // Run ResultSets query
  const resultSetFilter = buildResultSetFilters(txFilters);
  totalCount += await ResultSetsDB.count(resultSetFilter);
  const resultSets = await ResultSetsDB.find(
    resultSetFilter,
    { orderBy, skip: resultSetSkip, limit },
  );

  // Run Withdraws query
  const withdrawFilter = buildWithdrawFilters(txFilters);
  totalCount += await WithdrawsDB.count(withdrawFilter);
  const withdraws = await WithdrawsDB.find(
    withdrawFilter,
    { orderBy, skip: withdrawSkip, limit },
  );

  // Combine to single list
  let txs = concat(events, deposits, bets, resultSets, withdraws);
  txs = order(txs, ['blockNum'], [ORDER_DIRECTION.DESCENDING.toLowerCase()]);
  txs = slice(txs, 0, limit);

  // Send counts for each tx type so next query can be paginated
  const pageInfo = constructPageInfo(limit, skip, totalCount);
  pageInfo.nextTransactionSkips = {
    nextEventSkip: eventSkip + txs.filter(tx =>
      tx.txType === TX_TYPE.CREATE_EVENT).length,
    nextDepositSkip: depositSkip + txs.filter(tx =>
      tx.txType === TX_TYPE.DEPOSIT).length,
    nextBetSkip: betSkip + txs.filter(tx =>
      tx.txType === TX_TYPE.BET || tx.txType === TX_TYPE.VOTE).length,
    nextResultSetSkip: resultSetSkip + txs.filter(tx =>
      tx.txType === TX_TYPE.RESULT_SET).length,
    nextWithdrawSkip: withdrawSkip + txs.filter(tx =>
      tx.txType === TX_TYPE.WITHDRAW).length,
  };

  return {
    totalCount,
    pageInfo,
    items: txs,
  };
};
