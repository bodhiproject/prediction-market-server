const logger = require('../../utils/logger');
const { calculateSyncPercent } = require('./utils');

module.exports = async (
  parent,
  {}, // eslint-disable-line no-empty-pattern
  { controllers: { BlocksDB } },
) => {
  let block;
  try {
    block = await BlocksDB.findLatestBlock();
  } catch (err) {
    logger.error(`Error querying latest block: ${err.message}`);
  }
  const syncBlockNum = (block && block.blockNum) || 0;
  const syncBlockTime = (block && block.blockTime) || 0;
  const syncPercent = await calculateSyncPercent(syncBlockNum);

  return {
    syncBlockNum,
    syncBlockTime,
    syncPercent,
  };
};
