const chai = require('chai');
const chaiExclude = require('chai-exclude');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const createResultSets = require('../../../src/db/init/result-sets');
const ResultSetsDB = require('../../../src/db/controllers/result-sets');
const ResultSet = require('../../../src/models/result-set');

chai.use(chaiExclude);
const should = chai.should();
const { expect } = chai;

describe('db/controllers/result-sets', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  beforeEach(async () => {
    await createResultSets(db());
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  const pendingSets = [
    new ResultSet({
      txid: '0xd3594c878efb45d8d38bd4c2e0698541b21d565914f71fe85e9033612d81fab0',
      txStatus: 'PENDING',
      blockNum: null,
      eventAddress: '0x72dd97e774f27b61bf58669be4dbabf9c3d349a4',
      centralizedOracleAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      resultIndex: 2,
      amount: '1000000000',
      eventRound: 0,
      eventType: 'MR_EVENT',
    }),
  ];
  const sets = [
    new ResultSet({
      txid: '0xd3594c878efb45d8d38bd4c2e0698541b21d565914f71fe85e9033612d81fab0',
      txStatus: 'SUCCESS',
      blockNum: 4151388,
      eventAddress: '0x72dd97e774f27b61bf58669be4dbabf9c3d349a4',
      centralizedOracleAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      resultIndex: 2,
      amount: '1000000000',
      eventRound: 0,
      nextConsensusThreshold: '1100000000',
      nextArbitrationEndTime: 1560808386,
      eventType: 'MR_EVENT',
    }),
    new ResultSet({
      txid: '0xb296dd8bb6509f4fefe746f47c3d497d5f553c72d0b1fd2f8c000ae39e3ffd7f',
      txStatus: 'SUCCESS',
      blockNum: 3779203,
      eventAddress: '0x069278634f2d9fcae31755596a88b53568fb7c76',
      centralizedOracleAddress: null,
      resultIndex: 1,
      amount: '12100000000',
      eventRound: 2,
      nextConsensusThreshold: '13310000000',
      nextArbitrationEndTime: 1559777920,
      eventType: 'MR_EVENT',
    }),
  ];

  describe('updateOne', () => {
    it('should insert if does not exist', async () => {
      let res = await ResultSetsDB.findOne({ txid: sets[0].txid });
      should.not.exist(res);
      (await ResultSetsDB.count({})).should.equal(0);

      await ResultSetsDB.updateOne(sets[0]);
      (await ResultSetsDB.count({})).should.equal(1);
      res = await ResultSetsDB.findOne({ txid: sets[0].txid });
      expect(res).excluding('_id').to.deep.equal(sets[0]);
    });

    it('should update if exists', async () => {
      await ResultSetsDB.updateOne(pendingSets[0]);
      let res = await ResultSetsDB.findOne({ txid: pendingSets[0].txid });
      (await ResultSetsDB.count({})).should.equal(1);
      expect(res).excluding(['_id', 'blockNum']).to.deep.equal(pendingSets[0]);
      expect(res.blockNum).to.equal(0);

      await ResultSetsDB.updateOne(sets[0]);
      (await ResultSetsDB.count({})).should.equal(1);
      res = await ResultSetsDB.findOne({ txid: sets[0].txid });
      expect(res).excluding(['_id']).to.deep.equal(sets[0]);
    });
  });

  describe('find', () => {
    beforeEach(async () => {
      await ResultSetsDB.updateOne(sets[0]);
      await ResultSetsDB.updateOne(sets[1]);
    });

    it('should return items matching query', async () => {
      const res = await ResultSetsDB.find({});
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(sets[0]);
      expect(res[1]).excluding('_id').to.deep.equal(sets[1]);
    });

    it('should return items matching query with sort', async () => {
      let res = await ResultSetsDB.find(
        {},
        { orderBy: { field: 'blockNum', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(sets[1]);
      expect(res[1]).excluding('_id').to.deep.equal(sets[0]);

      res = await ResultSetsDB.find(
        {},
        { orderBy: { field: 'eventRound', direction: 'DESC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(sets[1]);
      expect(res[1]).excluding('_id').to.deep.equal(sets[0]);
    });

    it('should return empty array if no matches', async () => {
      const res = await ResultSetsDB.find({
        txid: '0x1234567890123456789012345678901234567890',
      });
      res.length.should.equal(0);
    });
  });

  describe('findOne', () => {
    beforeEach(async () => {
      await ResultSetsDB.updateOne(sets[0]);
    });

    it('should return matching item', async () => {
      const res = await ResultSetsDB.findOne({ txid: sets[0].txid });
      should.exist(res);
      expect(res).excluding('_id').to.deep.equal(sets[0]);
    });

    it('should return null if no match', async () => {
      const res = await ResultSetsDB.findOne({ txid: sets[1].txid });
      should.not.exist(res);
    });
  });

  describe('count', () => {
    it('should return the count', async () => {
      await ResultSetsDB.updateOne(sets[0]);
      (await ResultSetsDB.count({})).should.equal(1);

      await ResultSetsDB.updateOne(sets[1]);
      (await ResultSetsDB.count({})).should.equal(2);
    });
  });
});
