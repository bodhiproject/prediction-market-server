const chai = require('chai');
const chaiExclude = require('chai-exclude');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const createBets = require('../../../src/db/init/bets');
const BetsDB = require('../../../src/db/controllers/bets');
const Bet = require('../../../src/models/bet');

chai.use(chaiExclude);
const should = chai.should();
const { expect } = chai;

describe('db/controllers/bets', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  beforeEach(async () => {
    await createBets(db());
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  const pendingBets = [
    new Bet({
      txid: '0x445ffdcfb4b3e30c3c8d73a7583230b60eff51054035389df7cf187f5d3f75b6',
      txStatus: 'PENDING',
      blockNum: null,
      eventAddress: '0xbeaa4358aa4434227c4545544e50c1b1a52a51c6',
      betterAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      resultIndex: 2,
      amount: '1100000000',
      eventRound: 0,
      eventType: 'MR_EVENT',
    }),
  ];
  const bets = [
    new Bet({
      txid: '0x445ffdcfb4b3e30c3c8d73a7583230b60eff51054035389df7cf187f5d3f75b6',
      txStatus: 'SUCCESS',
      blockNum: 4154205,
      eventAddress: '0xbeaa4358aa4434227c4545544e50c1b1a52a51c6',
      betterAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      resultIndex: 2,
      amount: '1100000000',
      eventRound: 0,
      eventType: 'MR_EVENT',
    }),
    new Bet({
      txid: '0xe314e29163785d1361880588f252c039016943bf4de494b7ae0869fc9897fe13',
      txStatus: 'SUCCESS',
      blockNum: 3810125,
      eventAddress: '0xbbdfec793ef800769898795f469fc3951dc21eea',
      betterAddress: '0x7937a1e86f2cb43d6c91d27ca7a4f93c7f7189c3',
      resultIndex: 1,
      amount: '10000000',
      eventRound: 0,
      eventType: 'MR_EVENT',
    }),
  ];

  describe('updateOne', () => {
    it('should insert if does not exist', async () => {
      let res = await BetsDB.findOne({ txid: bets[0].txid });
      should.not.exist(res);
      (await BetsDB.count({})).should.equal(0);

      await BetsDB.updateOne(bets[0]);
      (await BetsDB.count({})).should.equal(1);
      res = await BetsDB.findOne({ txid: bets[0].txid });
      should.exist(res);
    });

    it('should update if exists', async () => {
      await BetsDB.updateOne(pendingBets[0]);
      let res = await BetsDB.findOne({ txid: pendingBets[0].txid });
      (await BetsDB.count({})).should.equal(1);
      expect(res).excluding(['_id', 'blockNum']).to.deep.equal(pendingBets[0]);
      expect(res.blockNum).to.equal(0);

      await BetsDB.updateOne(bets[0]);
      res = await BetsDB.findOne({ txid: bets[0].txid });
      (await BetsDB.count({})).should.equal(1);
      expect(res).excluding(['_id']).to.deep.equal(bets[0]);
    });
  });

  describe('find', () => {
    beforeEach(async () => {
      await BetsDB.updateOne(bets[0]);
      await BetsDB.updateOne(bets[1]);
    });

    it('should return items matching query', async () => {
      const res = await BetsDB.find({});
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(bets[0]);
      expect(res[1]).excluding('_id').to.deep.equal(bets[1]);
    });

    it('should return items matching query with sort', async () => {
      const res = await BetsDB.find(
        {},
        { orderBy: { field: 'blockNum', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(bets[1]);
      expect(res[1]).excluding('_id').to.deep.equal(bets[0]);
    });

    it('should return empty array if no matches', async () => {
      const res = await BetsDB.find({
        txid: '0x1234567890123456789012345678901234567890',
      });
      res.length.should.equal(0);
    });
  });

  describe('findOne', () => {
    beforeEach(async () => {
      await BetsDB.updateOne(bets[0]);
    });

    it('should return matching item', async () => {
      const res = await BetsDB.findOne({ txid: bets[0].txid });
      should.exist(res);
      expect(res).excluding('_id').to.deep.equal(bets[0]);
    });

    it('should return null if no match', async () => {
      const res = await BetsDB.findOne({ txid: bets[1].txid });
      should.not.exist(res);
    });
  });

  describe('count', () => {
    it('should return the count', async () => {
      await BetsDB.updateOne(bets[0]);
      (await BetsDB.count({})).should.equal(1);

      await BetsDB.updateOne(bets[1]);
      (await BetsDB.count({})).should.equal(2);
    });
  });
});
