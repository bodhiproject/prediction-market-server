const chai = require('chai');
const chaiExclude = require('chai-exclude');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const createNames = require('../../../src/db/init/names');
const NamesDB = require('../../../src/db/controllers/names');
const Name = require('../../../src/models/name');

chai.use(chaiExclude);
const should = chai.should();
const { expect } = chai;

describe('db/controllers/names', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  beforeEach(async () => {
    await createNames(db());
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  const names = [
    new Name({
      address: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
      name: 'deric',
    }),
    new Name({
      address: '0xda184722103b79479e9ef6d999688eae7fc460d4',
      name: 'seven1',
    }),
  ];

  describe('updateOne', () => {
    it('should insert if does not exist', async () => {
      let res = await NamesDB.findOne({ address: names[0].address });
      should.not.exist(res);
      (await NamesDB.count({})).should.equal(0);

      await NamesDB.updateOne(names[0]);
      (await NamesDB.count({})).should.equal(1);
      res = await NamesDB.findOne({ address: names[0].address });
      expect(res).excluding('_id').to.deep.equal(names[0]);
    });

    it('should update if exists', async () => {
      await NamesDB.updateOne(names[0]);
      (await NamesDB.count({})).should.equal(1);
      let res = await NamesDB.findOne({ address: names[0].address });
      expect(res).excluding('_id').to.deep.equal(names[0]);

      const updated = Object.assign({}, names[0]);
      updated.name = 'hello';
      await NamesDB.updateOne(updated);
      res = await NamesDB.findOne({ address: names[0].address });
      expect(res.name).to.equal(updated.name);
      (await NamesDB.count({})).should.equal(1);
    });
  });

  describe('find', () => {
    beforeEach(async () => {
      await NamesDB.updateOne(names[0]);
      await NamesDB.updateOne(names[1]);
    });

    it('should return items matching query', async () => {
      const res = await NamesDB.find({});
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(names[0]);
      expect(res[1]).excluding('_id').to.deep.equal(names[1]);
    });

    it('should return items matching query with sort', async () => {
      const res = await NamesDB.find(
        {},
        { orderBy: { field: 'name', direction: 'DESC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(names[1]);
      expect(res[1]).excluding('_id').to.deep.equal(names[0]);
    });

    it('should return empty array if no matches', async () => {
      const res = await NamesDB.find({
        address: '0x1234567890123456789012345678901234567890',
      });
      res.length.should.equal(0);
    });
  });

  describe('findOne', () => {
    beforeEach(async () => {
      await NamesDB.updateOne(names[0]);
    });

    it('should return matching item', async () => {
      const res = await NamesDB.findOne({ address: names[0].address });
      expect(res).excluding('_id').to.deep.equal(names[0]);
    });

    it('should return null if no match', async () => {
      const res = await NamesDB.findOne({ address: names[1].address });
      should.not.exist(res);
    });
  });

  describe('count', () => {
    it('should return the count', async () => {
      await NamesDB.updateOne(names[0]);
      (await NamesDB.count({})).should.equal(1);

      await NamesDB.updateOne(names[1]);
      (await NamesDB.count({})).should.equal(2);
    });
  });
});
