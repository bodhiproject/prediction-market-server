const chai = require('chai');
const chaiExclude = require('chai-exclude');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const createGlobalLeaderboardUsers = require('../../../src/db/init/global-leaderboard-users');
const GlobalLeaderboardUsersDB = require('../../../src/db/controllers/global-leaderboard-users');
const GlobalLeaderboardUser = require('../../../src/models/global-leaderboard-user');

chai.use(chaiExclude);
const should = chai.should();
const { expect } = chai;

describe('db/controllers/global-leaderboard-users', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  beforeEach(async () => {
    await createGlobalLeaderboardUsers(db());
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  const users = [
    new GlobalLeaderboardUser({
      userAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      investments: '1000000000',
      winnings: '1500000000',
    }),
    new GlobalLeaderboardUser({
      userAddress: '0x123456764c0bd3355b2d54e4fa2203e8343b6d6a',
      investments: '5000000',
      winnings: '5000000',
    }),
  ];

  describe('updateOne', () => {
    it('should insert if does not exist', async () => {
      let res = await GlobalLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
      });
      should.not.exist(res);
      (await GlobalLeaderboardUsersDB.count({})).should.equal(0);

      await GlobalLeaderboardUsersDB.updateOne(users[0]);
      (await GlobalLeaderboardUsersDB.count({})).should.equal(1);
      res = await GlobalLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
      });
      expect(res).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);
      expect(res.returnRatio).to.equal(1.5);
    });

    it('should update if exists', async () => {
      await GlobalLeaderboardUsersDB.updateOne(users[0]);
      let res = await GlobalLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
      });
      (await GlobalLeaderboardUsersDB.count({})).should.equal(1);
      expect(res).excluding(['_id']).to.deep.equal(users[0]);

      const updated = new GlobalLeaderboardUser({
        userAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
        investments: '2000000000',
        winnings: '3000000000',
      });
      await GlobalLeaderboardUsersDB.updateOne(updated);
      res = await GlobalLeaderboardUsersDB.findOne({
        userAddress: updated.userAddress,
      });
      (await GlobalLeaderboardUsersDB.count({})).should.equal(1);
      expect(res).excluding(['_id']).to.deep.equal(updated);
    });
  });

  describe('find', () => {
    beforeEach(async () => {
      await GlobalLeaderboardUsersDB.updateOne(users[0]);
      await GlobalLeaderboardUsersDB.updateOne(users[1]);
    });

    it('should return items matching query', async () => {
      let res = await GlobalLeaderboardUsersDB.find({});
      res.length.should.equal(2);
      expect(res[0]).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);
      expect(res[1]).excluding(['_id', 'returnRatio']).to.deep.equal(users[1]);

      res = await GlobalLeaderboardUsersDB.find({
        userAddress: users[0].userAddress,
      });
      res.length.should.equal(1);
      expect(res[0]).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);
    });

    it('should return items matching query with sort', async () => {
      let res = await GlobalLeaderboardUsersDB.find(
        {},
        { orderBy: { field: 'winnings', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding(['_id', 'returnRatio']).to.deep.equal(users[1]);
      expect(res[1]).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);

      res = await GlobalLeaderboardUsersDB.find(
        {},
        { orderBy: { field: 'investments', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding(['_id', 'returnRatio']).to.deep.equal(users[1]);
      expect(res[1]).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);
    });

    it('should return empty array if no matches', async () => {
      const res = await GlobalLeaderboardUsersDB.find({
        userAddress: '0x1234567890123456789012345678901234567890',
      });
      res.length.should.equal(0);
    });
  });

  describe('findOne', () => {
    beforeEach(async () => {
      await GlobalLeaderboardUsersDB.updateOne(users[0]);
    });

    it('should return matching item', async () => {
      const res = await GlobalLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
      });
      expect(res).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);
    });

    it('should return null if no match', async () => {
      const res = await GlobalLeaderboardUsersDB.findOne({
        userAddress: users[1].userAddress,
      });
      should.not.exist(res);
    });
  });

  describe('count', () => {
    it('should return the count', async () => {
      await GlobalLeaderboardUsersDB.updateOne(users[0]);
      (await GlobalLeaderboardUsersDB.count({})).should.equal(1);

      await GlobalLeaderboardUsersDB.updateOne(users[1]);
      (await GlobalLeaderboardUsersDB.count({})).should.equal(2);
    });
  });

  describe('addToInvestmentsAndWinnings', () => {
    beforeEach(async () => {
      await GlobalLeaderboardUsersDB.updateOne(users[0]);
      (await GlobalLeaderboardUsersDB.count({})).should.equal(1);
      const res = await GlobalLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
      });
      expect(res).excluding('_id').to.deep.equal(users[0]);
    });

    it('should add to investments', async () => {
      let res = await GlobalLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
      });
      expect(res.investments).to.equal('1000000000');
      expect(res.winnings).to.equal('1500000000');

      await GlobalLeaderboardUsersDB.addToInvestmentsAndWinnings(new GlobalLeaderboardUser({
        userAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
        investments: '1',
        winnings: '1',
      }));
      res = await GlobalLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
      });
      expect(res.investments).to.equal('1000000001');
      expect(res.winnings).to.equal('1500000001');
    });

    it('throws if user is undefined', async () => {
      let res = await GlobalLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
      });
      expect(res.investments).to.equal('1000000000');
      expect(res.winnings).to.equal('1500000000');

      await expect(GlobalLeaderboardUsersDB.addToInvestmentsAndWinnings(undefined))
        .to.be.rejectedWith(Error);
      await expect(GlobalLeaderboardUsersDB.addToInvestmentsAndWinnings(null))
        .to.be.rejectedWith(Error);
      res = await GlobalLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
      });
      expect(res.investments).to.equal('1000000000');
      expect(res.winnings).to.equal('1500000000');
    });
  });
});
