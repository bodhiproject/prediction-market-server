const chai = require('chai');
const chaiExclude = require('chai-exclude');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const createWithdraws = require('../../../src/db/init/withdraws');
const WithdrawsDB = require('../../../src/db/controllers/withdraws');
const Withdraw = require('../../../src/models/withdraw');

chai.use(chaiExclude);
const should = chai.should();
const { expect } = chai;

describe('db/controllers/withdraws', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  beforeEach(async () => {
    await createWithdraws(db());
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  const pendingWithdraws = [
    new Withdraw({
      txid: '0x46c8a1608bf4fe59295c50d50efbbc3ea47eff7d4d4079fb82165d7907b96d87',
      txStatus: 'PENDING',
      blockNum: null,
      eventAddress: '0x9376770714fe52bd43d7dab70bb7eec5773079f1',
      winnerAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      winningAmount: '46910000000',
      creatorReturnAmount: '0',
      eventType: 'MR_EVENT',
    }),
  ];

  const withdraws = [
    new Withdraw({
      txid: '0x46c8a1608bf4fe59295c50d50efbbc3ea47eff7d4d4079fb82165d7907b96d87',
      txStatus: 'SUCCESS',
      blockNum: 4185772,
      eventAddress: '0x9376770714fe52bd43d7dab70bb7eec5773079f1',
      winnerAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      winningAmount: '46910000000',
      creatorReturnAmount: '100000000',
      eventType: 'MR_EVENT',
    }),
    new Withdraw({
      txid: '0xa941da0b9d465ec5a394dbfe73032e25181e6f9eb5452e8313899a9f6b05e128',
      txStatus: 'SUCCESS',
      blockNum: 4153179,
      eventAddress: '0x97c4810cbec1c767110f8aaaa6e1d400768f598c',
      winnerAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      winningAmount: '3310000000',
      creatorReturnAmount: '0',
      eventType: 'MR_EVENT',
    }),
  ];

  describe('updateOne', () => {
    it('should insert if does not exist', async () => {
      let res = await WithdrawsDB.findOne({ txid: withdraws[0].txid });
      should.not.exist(res);
      (await WithdrawsDB.count({})).should.equal(0);

      await WithdrawsDB.updateOne(withdraws[0]);
      (await WithdrawsDB.count({})).should.equal(1);
      res = await WithdrawsDB.findOne({ txid: withdraws[0].txid });
      expect(res).excluding('_id').to.deep.equal(withdraws[0]);
    });

    it('should update if exists', async () => {
      await WithdrawsDB.updateOne(pendingWithdraws[0]);
      let res = await WithdrawsDB.findOne({ txid: pendingWithdraws[0].txid });
      (await WithdrawsDB.count({})).should.equal(1);
      expect(res).excluding(['_id', 'blockNum']).to.deep.equal(pendingWithdraws[0]);
      expect(res.blockNum).to.equal(0);

      await WithdrawsDB.updateOne(withdraws[0]);
      (await WithdrawsDB.count({})).should.equal(1);
      res = await WithdrawsDB.findOne({ txid: withdraws[0].txid });
      expect(res).excluding(['_id']).to.deep.equal(withdraws[0]);
    });
  });

  describe('find', () => {
    beforeEach(async () => {
      await WithdrawsDB.updateOne(withdraws[0]);
      await WithdrawsDB.updateOne(withdraws[1]);
    });

    it('should return items matching query', async () => {
      const res = await WithdrawsDB.find({});
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(withdraws[0]);
      expect(res[1]).excluding('_id').to.deep.equal(withdraws[1]);
    });

    it('should return items matching query with sort', async () => {
      let res = await WithdrawsDB.find(
        {},
        { orderBy: { field: 'winningAmount', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(withdraws[1]);
      expect(res[1]).excluding('_id').to.deep.equal(withdraws[0]);

      res = await WithdrawsDB.find(
        {},
        { orderBy: { field: 'creatorReturnAmount', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(withdraws[1]);
      expect(res[1]).excluding('_id').to.deep.equal(withdraws[0]);
    });

    it('should return empty array if no matches', async () => {
      const res = await WithdrawsDB.find({
        txid: '0x1234567890123456789012345678901234567890',
      });
      res.length.should.equal(0);
    });
  });

  describe('findOne', () => {
    beforeEach(async () => {
      await WithdrawsDB.updateOne(withdraws[0]);
    });

    it('should return matching item', async () => {
      const res = await WithdrawsDB.findOne({ txid: withdraws[0].txid });
      should.exist(res);
      expect(res).excluding('_id').to.deep.equal(withdraws[0]);
    });

    it('should return null if no match', async () => {
      const res = await WithdrawsDB.findOne({ txid: withdraws[1].txid });
      should.not.exist(res);
    });
  });

  describe('count', () => {
    it('should return the count', async () => {
      await WithdrawsDB.updateOne(withdraws[0]);
      (await WithdrawsDB.count({})).should.equal(1);

      await WithdrawsDB.updateOne(withdraws[1]);
      (await WithdrawsDB.count({})).should.equal(2);
    });
  });
});
