const chai = require('chai');
const chaiExclude = require('chai-exclude');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const createEvents = require('../../../src/db/init/events');
const EventsDB = require('../../../src/db/controllers/events');
const Event = require('../../../src/models/event');
const Withdraw = require('../../../src/models/withdraw');
const Deposit = require('../../../src/models/deposit');
const Bet = require('../../../src/models/bet');
const { TX_STATUS, EVENT_STATUS, EVENT_TYPE } = require('../../../src/constants');

chai.use(chaiExclude);
const should = chai.should();
const { expect } = chai;

describe('db/controllers/events', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  beforeEach(async () => {
    await createEvents(db());
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  const pendingEvents = [
    new Event({
      txid: '0xa47a223155ee3f9e1d313735547e7ad3e299d20992c0524f6b9694984100f7b0',
      txStatus: TX_STATUS.PENDING,
      blockNum: null,
      ownerAddress: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
      name: 'Test Event 1',
      results: [
        'Invalid',
        '1',
        '2',
      ],
      numOfResults: 3,
      centralizedOracle: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
      betEndTime: 1561168846,
      resultSetStartTime: 1561168846,
      status: EVENT_STATUS.CREATED,
      language: 'en-US',
      eventType: EVENT_TYPE.AB_EVENT,
    }),
  ];
  const events = [
    new Event({
      txid: '0xa47a223155ee3f9e1d313735547e7ad3e299d20992c0524f6b9694984100f7b0',
      txStatus: TX_STATUS.SUCCESS,
      blockNum: 4242848,
      address: '0x60f243ed4a99e3309ef67d9486b9db14334bc6d4',
      ownerAddress: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
      version: 0,
      name: 'Test Event 1',
      results: [
        '0x496e76616c6964',
        '0x61',
        '0x62',
      ],
      numOfResults: 3,
      winningChances: [0, 75, 25],
      odds: [0, 1.2, 3.6],
      centralizedOracle: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
      betStartTime: 1561082467,
      betEndTime: 1561168846,
      resultSetStartTime: 1561168900,
      resultSetEndTime: 1561341646,
      escrowAmount: '10000000000',
      arbitrationLength: 300,
      thresholdPercentIncrease: 10,
      arbitrationRewardPercentage: 10,
      consensusThreshold: '1000000000',
      arbitrationEndTime: 0,
      eventType: EVENT_TYPE.AB_EVENT,
      maxBets: ['0', '7500000000', '2500000000'],
    }),
    new Event({
      txid: '0xf0e68c3cfa4e77f0a5b46afd9f8c5efccd5d90f419053c6d452b021bc203c44f',
      txStatus: TX_STATUS.SUCCESS,
      blockNum: 3979339,
      address: '0xfef4a675dba91b91608dc75c40deaca0333af514',
      ownerAddress: '0x47ba776b3ed5d514d3e206ffee72fa483baffa7e',
      version: 0,
      name: 'Test Event 2',
      results: [
        '0x496e76616c6964',
        '0x61',
        '0x62',
      ],
      numOfResults: 3,
      winningChances: [0, 75, 25],
      odds: [0, 1.2, 3.6],
      centralizedOracle: '0x47ba776b3ed5d514d3e206ffee72fa483baffa7e',
      betStartTime: 1560292822,
      betEndTime: 1560379222,
      resultSetStartTime: 1560379222,
      resultSetEndTime: 1560465622,
      escrowAmount: '10000000000',
      arbitrationLength: 300,
      thresholdPercentIncrease: 10,
      arbitrationRewardPercentage: 10,
      consensusThreshold: '1000000000',
      arbitrationEndTime: 0,
      eventType: EVENT_TYPE.AB_EVENT,
      maxBets: ['0', '7500000000', '2500000000'],
    }),
  ];

  describe('updateOne', () => {
    it('should insert if does not exist', async () => {
      let res = await EventsDB.findOne({ txid: events[0].txid });
      should.not.exist(res);
      (await EventsDB.count({})).should.equal(0);

      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      res = await EventsDB.findOne({ txid: events[0].txid });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should update if exists', async () => {
      await EventsDB.updateOne(pendingEvents[0]);
      (await EventsDB.count({})).should.equal(1);
      let res = await EventsDB.findOne({ txid: pendingEvents[0].txid });
      expect(res).excluding('_id').to.deep.equal(pendingEvents[0]);

      await EventsDB.updateOne(events[0]);
      res = await EventsDB.findOne({ txid: events[0].txid });
      (await EventsDB.count({})).should.equal(1);
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });
  });

  describe('updateOneByAddress', () => {
    it('should update if exists', async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);

      const updated = Object.assign({}, events[0]);
      updated.maxBets = ['0', '5000000000', '5000000000'];
      await EventsDB.updateOneByAddress(updated.address, updated);
      res = await EventsDB.findOne({ address: events[0].address });
      (await EventsDB.count({})).should.equal(1);
      expect(res).excluding('_id').to.deep.equal(updated);
    });

    it('should not insert if does not exist', async () => {
      let res = await EventsDB.findOne({ address: events[0].address });
      should.not.exist(res);
      (await EventsDB.count({})).should.equal(0);

      await EventsDB.updateOneByAddress(events[0].address, events[0]);
      res = await EventsDB.findOne({ address: events[0].address });
      should.not.exist(res);
      (await EventsDB.count({})).should.equal(0);
    });
  });

  describe('find', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
      await EventsDB.updateOne(events[1]);
    });

    it('should return items matching query', async () => {
      const res = await EventsDB.find({});
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(events[0]);
      expect(res[1]).excluding('_id').to.deep.equal(events[1]);
    });

    it('should return items matching query with sort', async () => {
      const res = await EventsDB.find(
        {},
        { orderBy: { field: 'blockNum', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(events[1]);
      expect(res[1]).excluding('_id').to.deep.equal(events[0]);
    });

    it('should return empty array if no matches', async () => {
      const res = await EventsDB.find({
        address: '0x1234567890123456789012345678901234567890',
      });
      res.length.should.equal(0);
    });
  });

  describe('findOne', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
    });

    it('should return matching item', async () => {
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should return null if no match', async () => {
      const res = await EventsDB.findOne({ address: events[1].address });
      should.not.exist(res);
    });
  });

  describe('count', () => {
    it('should return the count', async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);

      await EventsDB.updateOne(events[1]);
      (await EventsDB.count({})).should.equal(2);
    });
  });

  describe('updateEventStatusPreBetting', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should update if query matches', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.CREATED;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusPreBetting(updated.betStartTime - 1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.PRE_BETTING);
    });

    it('should not update if wrong txStatus', async () => {
      const updated = Object.assign({}, events[0]);
      updated.txStatus = TX_STATUS.PENDING;
      updated.status = EVENT_STATUS.CREATED;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusPreBetting(updated.betStartTime + 1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.CREATED);
    });

    it('should not update if wrong time period', async () => {
      const updated = Object.assign({}, events[0]);
      updated.txStatus = TX_STATUS.SUCCESS;
      updated.status = EVENT_STATUS.CREATED;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusPreBetting(updated.betStartTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.CREATED);
    });

    it('should not update if wrong currentRound', async () => {
      const updated = Object.assign({}, events[0]);
      updated.txStatus = TX_STATUS.SUCCESS;
      updated.status = EVENT_STATUS.CREATED;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusPreBetting(updated.betStartTime - 1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.CREATED);
    });
  });

  describe('updateEventStatusBetting', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should update if query matches', async () => {
      // Lower bound
      let updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.PRE_BETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusBetting(updated.betStartTime);
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.BETTING);

      // Upper bound
      updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.PRE_BETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusBetting(updated.betEndTime - 1);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.BETTING);
    });

    it('should not update if wrong txStatus', async () => {
      const updated = Object.assign({}, events[0]);
      updated.txStatus = TX_STATUS.PENDING;
      updated.status = EVENT_STATUS.PRE_BETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusBetting(updated.betStartTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.PRE_BETTING);
    });

    it('should not update if wrong time period', async () => {
      // Lower bound
      let updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.PRE_BETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusBetting(updated.betStartTime - 1);
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.PRE_BETTING);

      // Upper bound
      updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.PRE_BETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusBetting(updated.betEndTime);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.PRE_BETTING);
    });

    it('should not update if wrong currentRound', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.PRE_BETTING;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusBetting(updated.betStartTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.PRE_BETTING);
    });
  });

  describe('updateEventStatusPreResultSetting', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should update if query matches', async () => {
      // Lower bound
      let updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.BETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusPreResultSetting(updated.betEndTime);
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.PRE_RESULT_SETTING);

      // Upper bound
      updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.PRE_BETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusPreResultSetting(updated.resultSetStartTime - 1);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.PRE_RESULT_SETTING);
    });

    it('should not update if wrong txStatus', async () => {
      const updated = Object.assign({}, events[0]);
      updated.txStatus = TX_STATUS.PENDING;
      updated.status = EVENT_STATUS.BETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusPreResultSetting(updated.betEndTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.BETTING);
    });

    it('should not update if wrong time period', async () => {
      // Lower bound
      let updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.BETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusPreResultSetting(updated.betEndTime - 1);
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.BETTING);

      // Upper bound
      updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.BETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusPreResultSetting(updated.resultSetStartTime);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.BETTING);
    });

    it('should not update if wrong currentRound', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.BETTING;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusPreResultSetting(updated.betEndTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.BETTING);
    });
  });

  describe('updateEventStatusOracleResultSetting', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should update if query matches', async () => {
      // Lower bound
      let updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.PRE_RESULT_SETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusOracleResultSetting(updated.resultSetStartTime);
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.ORACLE_RESULT_SETTING);

      // Upper bound
      updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.PRE_BETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusOracleResultSetting(updated.resultSetEndTime - 1);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.ORACLE_RESULT_SETTING);
    });

    it('should not update if wrong txStatus', async () => {
      const updated = Object.assign({}, events[0]);
      updated.txStatus = TX_STATUS.PENDING;
      updated.status = EVENT_STATUS.PRE_RESULT_SETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusOracleResultSetting(updated.resultSetStartTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.PRE_RESULT_SETTING);
    });

    it('should not update if wrong time period', async () => {
      // Lower bound
      let updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.PRE_RESULT_SETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusOracleResultSetting(updated.resultSetStartTime - 1);
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.PRE_RESULT_SETTING);

      // Upper bound
      updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.PRE_RESULT_SETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusOracleResultSetting(updated.resultSetEndTime);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.PRE_RESULT_SETTING);
    });

    it('should not update if wrong currentRound', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.PRE_RESULT_SETTING;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusOracleResultSetting(updated.resultSetStartTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.PRE_RESULT_SETTING);
    });
  });

  describe('updateEventStatusOpenResultSetting', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should update if query matches', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.ORACLE_RESULT_SETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusOpenResultSetting(updated.resultSetEndTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.OPEN_RESULT_SETTING);
    });

    it('should not update if wrong txStatus', async () => {
      const updated = Object.assign({}, events[0]);
      updated.txStatus = TX_STATUS.PENDING;
      updated.status = EVENT_STATUS.ORACLE_RESULT_SETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusOpenResultSetting(updated.resultSetEndTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.ORACLE_RESULT_SETTING);
    });

    it('should not update if wrong time period', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.ORACLE_RESULT_SETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusOpenResultSetting(updated.resultSetEndTime - 1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.ORACLE_RESULT_SETTING);
    });

    it('should not update if wrong currentRound', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.ORACLE_RESULT_SETTING;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusOpenResultSetting(updated.resultSetEndTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.ORACLE_RESULT_SETTING);
    });
  });

  describe('updateEventStatusArbitration', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should update if query matches', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.OPEN_RESULT_SETTING;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusArbitration(updated.arbitrationEndTime - 1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.ARBITRATION);
    });

    it('should not update if wrong txStatus', async () => {
      const updated = Object.assign({}, events[0]);
      updated.txStatus = TX_STATUS.PENDING;
      updated.status = EVENT_STATUS.OPEN_RESULT_SETTING;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusArbitration(updated.arbitrationEndTime - 1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.OPEN_RESULT_SETTING);
    });

    it('should not update if wrong time period', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.OPEN_RESULT_SETTING;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusArbitration(updated.arbitrationEndTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.OPEN_RESULT_SETTING);
    });

    it('should not update if wrong currentRound', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.OPEN_RESULT_SETTING;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusArbitration(updated.arbitrationEndTime - 1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.OPEN_RESULT_SETTING);
    });
  });

  describe('updateEventStatusWithdrawing', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should update if query matches', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.ARBITRATION;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusWithdrawing(updated.arbitrationEndTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.WITHDRAWING);
    });

    it('should return an array of events that were updated', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.ARBITRATION;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      const res = await EventsDB.updateEventStatusWithdrawing(updated.arbitrationEndTime);
      expect(res.length).to.equal(1);
      expect(res[0]).excluding('_id').to.deep.equal(updated);
    });

    it('should not update if wrong txStatus', async () => {
      const updated = Object.assign({}, events[0]);
      updated.txStatus = TX_STATUS.PENDING;
      updated.status = EVENT_STATUS.ARBITRATION;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusWithdrawing(updated.arbitrationEndTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.ARBITRATION);
    });

    it('should not update if wrong time period', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.ARBITRATION;
      updated.currentRound = 1;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusWithdrawing(updated.arbitrationEndTime - 1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.ARBITRATION);
    });

    it('should not update if wrong currentRound', async () => {
      const updated = Object.assign({}, events[0]);
      updated.status = EVENT_STATUS.ARBITRATION;
      updated.currentRound = 0;
      await EventsDB.updateOne(updated);

      await EventsDB.updateEventStatusWithdrawing(updated.arbitrationEndTime);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res.status).to.equal(EVENT_STATUS.ARBITRATION);
    });
  });

  describe('addToWithdrawnList', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should add to withdrawnList array', async () => {
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.withdrawnList.length).to.equal(0);

      // Withdraw 1
      let withdraw = new Withdraw({
        txid: '0x46c8a1608bf4fe59295c50d50efbbc3ea47eff7d4d4079fb82165d7907b96d87',
        txStatus: 'SUCCESS',
        blockNum: 4243848,
        eventAddress: '0x60f243ed4a99e3309ef67d9486b9db14334bc6d4',
        winnerAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
        winningAmount: '46910000000',
        creatorReturnAmount: '100000000',
      });
      await EventsDB.addToWithdrawnList(withdraw);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.withdrawnList.length).to.equal(1);
      expect(res.withdrawnList[0]).to.equal(withdraw.winnerAddress);

      // Withdraw 2
      withdraw = new Withdraw({
        txid: '0x46c8a1608bf4fe59295c50d50efbbc3ea47eff7d4d4079fb82165d7907b96d87',
        txStatus: 'SUCCESS',
        blockNum: 4243849,
        eventAddress: '0x60f243ed4a99e3309ef67d9486b9db14334bc6d4',
        winnerAddress: '0x47ba776b3ed5d514d3e206ffee72fa483baffa7e',
        winningAmount: '46910000000',
        creatorReturnAmount: '100000000',
      });
      await EventsDB.addToWithdrawnList(withdraw);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.withdrawnList.length).to.equal(2);
      expect(res.withdrawnList[1]).to.equal(withdraw.winnerAddress);
    });

    it('should throw if withdraw is undefined', async () => {
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.withdrawnList.length).to.equal(0);

      await expect(EventsDB.addToWithdrawnList(undefined)).to.be.rejectedWith(Error);
      await expect(EventsDB.addToWithdrawnList(null)).to.be.rejectedWith(Error);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.withdrawnList.length).to.equal(0);
    });
  });

  describe('addToEscrowAmount', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should add to escrowAmount', async () => {
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.escrowAmount).to.equal('10000000000');

      const deposit = new Deposit({
        txid: '0x445ffdcfb4b3e30c3c8d73a7583230b60eff51054035389df7cf187f5d3f75b6',
        txStatus: 'SUCCESS',
        blockNum: 4242948,
        eventAddress: '0x60f243ed4a99e3309ef67d9486b9db14334bc6d4',
        depositorAddress: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
        amount: '5000000000',
      });
      await EventsDB.addToEscrowAmount(deposit);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.escrowAmount).to.equal('15000000000');
    });

    it('should throw if deposit is undefined', async () => {
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.escrowAmount).to.equal('10000000000');

      await expect(EventsDB.addToEscrowAmount(undefined)).to.be.rejectedWith(Error);
      await expect(EventsDB.addToEscrowAmount(null)).to.be.rejectedWith(Error);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.escrowAmount).to.equal('10000000000');
    });
  });

  describe('addToBetRoundResultsAmount', () => {
    beforeEach(async () => {
      await EventsDB.updateOne(events[0]);
      (await EventsDB.count({})).should.equal(1);
      const res = await EventsDB.findOne({ address: events[0].address });
      expect(res).excluding('_id').to.deep.equal(events[0]);
    });

    it('should add to betRoundResultsAmount array', async () => {
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.betRoundResultsAmount).to.deep.equal(['0', '0', '0']);

      // Bet 1
      await EventsDB.addToBetRoundResultsAmount(new Bet({
        txid: '0x445ffdcfb4b3e30c3c8d73a7583230b60eff51054035389df7cf187f5d3f75b6',
        txStatus: 'SUCCESS',
        blockNum: 4242950,
        eventAddress: '0x60f243ed4a99e3309ef67d9486b9db14334bc6d4',
        betterAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
        resultIndex: 1,
        amount: '100000000',
        eventRound: 0,
      }));
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.betRoundResultsAmount).to.deep.equal(['0', '100000000', '0']);

      // Bet 2
      await EventsDB.addToBetRoundResultsAmount(new Bet({
        txid: '0x445ffdcfb4b3e30c3c8d73a7583230b60eff51054035389df7cf187f5d3f75b6',
        txStatus: 'SUCCESS',
        blockNum: 4242951,
        eventAddress: '0x60f243ed4a99e3309ef67d9486b9db14334bc6d4',
        betterAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
        resultIndex: 2,
        amount: '200000000',
        eventRound: 0,
      }));
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.betRoundResultsAmount).to.deep.equal(['0', '100000000', '200000000']);

      // Bet 3
      await EventsDB.addToBetRoundResultsAmount(new Bet({
        txid: '0x445ffdcfb4b3e30c3c8d73a7583230b60eff51054035389df7cf187f5d3f75b6',
        txStatus: 'SUCCESS',
        blockNum: 4242952,
        eventAddress: '0x60f243ed4a99e3309ef67d9486b9db14334bc6d4',
        betterAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
        resultIndex: 1,
        amount: '300000000',
        eventRound: 0,
      }));
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.betRoundResultsAmount).to.deep.equal(['0', '400000000', '200000000']);
    });

    it('should throw if bet is undefined', async () => {
      let res = await EventsDB.findOne({ address: events[0].address });
      expect(res.betRoundResultsAmount).to.deep.equal(['0', '0', '0']);

      await expect(EventsDB.addToBetRoundResultsAmount(undefined)).to.be.rejectedWith(Error);
      await expect(EventsDB.addToBetRoundResultsAmount(null)).to.be.rejectedWith(Error);
      res = await EventsDB.findOne({ address: events[0].address });
      expect(res.betRoundResultsAmount).to.deep.equal(['0', '0', '0']);
    });
  });
});
