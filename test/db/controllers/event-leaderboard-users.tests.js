const chai = require('chai');
const chaiExclude = require('chai-exclude');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const createEventLeaderboardUsers = require('../../../src/db/init/event-leaderboard-users');
const EventLeaderboardUsersDB = require('../../../src/db/controllers/event-leaderboard-users');
const EventLeaderboardUser = require('../../../src/models/event-leaderboard-user');

chai.use(chaiExclude);
const should = chai.should();
const { expect } = chai;

describe('db/controllers/event-leaderboard-users', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  beforeEach(async () => {
    await createEventLeaderboardUsers(db());
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  const users = [
    new EventLeaderboardUser({
      eventAddress: '0x09645ea6e4e1f5375f7596b73b3b597e6507201a',
      userAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      investments: '1000000000',
      winnings: '1500000000',
    }),
    new EventLeaderboardUser({
      eventAddress: '0x09645ea6e4e1f5375f7596b73b3b597e6507201a',
      userAddress: '0x123456764c0bd3355b2d54e4fa2203e8343b6d6a',
      investments: '2000000',
      winnings: '5000000',
    }),
  ];

  describe('updateOne', () => {
    it('should insert if does not exist', async () => {
      let res = await EventLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
        eventAddress: users[0].eventAddress,
      });
      should.not.exist(res);
      (await EventLeaderboardUsersDB.count({})).should.equal(0);

      await EventLeaderboardUsersDB.updateOne(users[0]);
      (await EventLeaderboardUsersDB.count({})).should.equal(1);
      res = await EventLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
        eventAddress: users[0].eventAddress,
      });
      expect(res).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);
      expect(res.returnRatio).to.equal(1.5);
    });

    it('should update if exists', async () => {
      await EventLeaderboardUsersDB.updateOne(users[0]);
      let res = await EventLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
        eventAddress: users[0].eventAddress,
      });
      (await EventLeaderboardUsersDB.count({})).should.equal(1);
      expect(res).excluding(['_id']).to.deep.equal(users[0]);

      const updated = new EventLeaderboardUser({
        eventAddress: '0x09645ea6e4e1f5375f7596b73b3b597e6507201a',
        userAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
        investments: '2000000000',
        winnings: '3000000000',
      });
      await EventLeaderboardUsersDB.updateOne(updated);
      res = await EventLeaderboardUsersDB.findOne({
        userAddress: updated.userAddress,
        eventAddress: updated.eventAddress,
      });
      (await EventLeaderboardUsersDB.count({})).should.equal(1);
      expect(res).excluding(['_id']).to.deep.equal(updated);
    });
  });

  describe('find', () => {
    beforeEach(async () => {
      await EventLeaderboardUsersDB.updateOne(users[0]);
      await EventLeaderboardUsersDB.updateOne(users[1]);
    });

    it('should return items matching query', async () => {
      let res = await EventLeaderboardUsersDB.find({});
      res.length.should.equal(2);
      expect(res[0]).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);
      expect(res[1]).excluding(['_id', 'returnRatio']).to.deep.equal(users[1]);

      res = await EventLeaderboardUsersDB.find({
        userAddress: users[0].userAddress,
        eventAddress: users[0].eventAddress,
      });
      res.length.should.equal(1);
      expect(res[0]).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);
    });

    it('should return items matching query with sort', async () => {
      let res = await EventLeaderboardUsersDB.find(
        {},
        { orderBy: { field: 'winnings', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding(['_id', 'returnRatio']).to.deep.equal(users[1]);
      expect(res[1]).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);

      res = await EventLeaderboardUsersDB.find(
        {},
        { orderBy: { field: 'investments', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding(['_id', 'returnRatio']).to.deep.equal(users[1]);
      expect(res[1]).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);
    });

    it('should return empty array if no matches', async () => {
      let res = await EventLeaderboardUsersDB.find({
        userAddress: '0x1234567890123456789012345678901234567890',
        eventAddress: users[0].eventAddress,
      });
      res.length.should.equal(0);

      res = await EventLeaderboardUsersDB.find({
        userAddress: users[0].userAddress,
        eventAddress: '0x1234567890123456789012345678901234567890',
      });
      res.length.should.equal(0);
    });
  });

  describe('findOne', () => {
    beforeEach(async () => {
      await EventLeaderboardUsersDB.updateOne(users[0]);
    });

    it('should return matching item', async () => {
      const res = await EventLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
        eventAddress: users[0].eventAddress,
      });
      expect(res).excluding(['_id', 'returnRatio']).to.deep.equal(users[0]);
    });

    it('should return null if no match', async () => {
      const res = await EventLeaderboardUsersDB.findOne({
        userAddress: users[1].userAddress,
        eventAddress: users[1].eventAddress,
      });
      should.not.exist(res);
    });
  });

  describe('count', () => {
    it('should return the count', async () => {
      await EventLeaderboardUsersDB.updateOne(users[0]);
      (await EventLeaderboardUsersDB.count({})).should.equal(1);

      await EventLeaderboardUsersDB.updateOne(users[1]);
      (await EventLeaderboardUsersDB.count({})).should.equal(2);
    });
  });

  describe('addToInvestments', () => {
    beforeEach(async () => {
      await EventLeaderboardUsersDB.updateOne(users[0]);
      (await EventLeaderboardUsersDB.count({})).should.equal(1);
      const res = await EventLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
        eventAddress: users[0].eventAddress,
      });
      expect(res).excluding('_id').to.deep.equal(users[0]);
    });

    it('should add to investments', async () => {
      let res = await EventLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
        eventAddress: users[0].eventAddress,
      });
      expect(res.investments).to.equal('1000000000');

      await EventLeaderboardUsersDB.addToInvestments(new EventLeaderboardUser({
        eventAddress: '0x09645ea6e4e1f5375f7596b73b3b597e6507201a',
        userAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
        investments: '1',
        winnings: '0',
      }));
      res = await EventLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
        eventAddress: users[0].eventAddress,
      });
      expect(res.investments).to.equal('1000000001');
    });
  });

  describe('addToWinnings', () => {
    beforeEach(async () => {
      await EventLeaderboardUsersDB.updateOne(users[0]);
      (await EventLeaderboardUsersDB.count({})).should.equal(1);
      const res = await EventLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
        eventAddress: users[0].eventAddress,
      });
      expect(res).excluding('_id').to.deep.equal(users[0]);
    });

    it('should add to investments', async () => {
      let res = await EventLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
        eventAddress: users[0].eventAddress,
      });
      expect(res.winnings).to.equal('1500000000');

      await EventLeaderboardUsersDB.addToWinnings(new EventLeaderboardUser({
        eventAddress: '0x09645ea6e4e1f5375f7596b73b3b597e6507201a',
        userAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
        investments: '0',
        winnings: '1',
      }));
      res = await EventLeaderboardUsersDB.findOne({
        userAddress: users[0].userAddress,
        eventAddress: users[0].eventAddress,
      });
      expect(res.winnings).to.equal('1500000001');
    });
  });
});
