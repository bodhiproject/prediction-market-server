const chai = require('chai');
const chaiExclude = require('chai-exclude');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const createDeposits = require('../../../src/db/init/deposits');
const DepositsDB = require('../../../src/db/controllers/deposits');
const Deposit = require('../../../src/models/deposit');

chai.use(chaiExclude);
const should = chai.should();
const { expect } = chai;

describe('db/controllers/deposits', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  beforeEach(async () => {
    await createDeposits(db());
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  const pendingDeposits = [
    new Deposit({
      txid: '0x445ffdcfb4b3e30c3c8d73a7583230b60eff51054035389df7cf187f5d3f75b6',
      txStatus: 'PENDING',
      blockNum: null,
      eventAddress: '0xbeaa4358aa4434227c4545544e50c1b1a52a51c6',
      depositorAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      amount: '1100000000',
      eventType: 'MR_EVENT',
    }),
  ];
  const deposits = [
    new Deposit({
      txid: '0x445ffdcfb4b3e30c3c8d73a7583230b60eff51054035389df7cf187f5d3f75b6',
      txStatus: 'SUCCESS',
      blockNum: 4154205,
      eventAddress: '0xbeaa4358aa4434227c4545544e50c1b1a52a51c6',
      depositorAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      amount: '1100000000',
      eventType: 'MR_EVENT',
    }),
    new Deposit({
      txid: '0xe314e29163785d1361880588f252c039016943bf4de494b7ae0869fc9897fe13',
      txStatus: 'SUCCESS',
      blockNum: 3810125,
      eventAddress: '0xbbdfec793ef800769898795f469fc3951dc21eea',
      depositorAddress: '0x7937a1e86f2cb43d6c91d27ca7a4f93c7f7189c3',
      amount: '10000000',
      eventType: 'MR_EVENT',
    }),
  ];

  describe('updateOne', () => {
    it('should insert if does not exist', async () => {
      let res = await DepositsDB.findOne({ txid: deposits[0].txid });
      should.not.exist(res);
      (await DepositsDB.count({})).should.equal(0);

      await DepositsDB.updateOne(deposits[0]);
      (await DepositsDB.count({})).should.equal(1);
      res = await DepositsDB.findOne({ txid: deposits[0].txid });
      expect(res).excluding('_id').to.deep.equal(deposits[0]);
    });

    it('should update if exists', async () => {
      await DepositsDB.updateOne(pendingDeposits[0]);
      (await DepositsDB.count({})).should.equal(1);
      let res = await DepositsDB.findOne({ txid: pendingDeposits[0].txid });
      expect(res).excluding(['_id', 'blockNum']).to.deep.equal(pendingDeposits[0]);
      expect(res.blockNum).to.equal(0);

      await DepositsDB.updateOne(deposits[0]);
      res = await DepositsDB.findOne({ txid: deposits[0].txid });
      (await DepositsDB.count({})).should.equal(1);
      expect(res).excluding(['_id']).to.deep.equal(deposits[0]);
    });
  });

  describe('find', () => {
    beforeEach(async () => {
      await DepositsDB.updateOne(deposits[0]);
      await DepositsDB.updateOne(deposits[1]);
    });

    it('should return items matching query', async () => {
      const res = await DepositsDB.find({});
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(deposits[0]);
      expect(res[1]).excluding('_id').to.deep.equal(deposits[1]);
    });

    it('should return items matching query with sort', async () => {
      let res = await DepositsDB.find(
        {},
        { orderBy: { field: 'blockNum', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(deposits[1]);
      expect(res[1]).excluding('_id').to.deep.equal(deposits[0]);

      res = await DepositsDB.find(
        {},
        { orderBy: { field: 'amount', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(deposits[1]);
      expect(res[1]).excluding('_id').to.deep.equal(deposits[0]);
    });

    it('should return empty array if no matches', async () => {
      const res = await DepositsDB.find({
        txid: '0x1234567890123456789012345678901234567890',
      });
      res.length.should.equal(0);
    });
  });

  describe('findOne', () => {
    beforeEach(async () => {
      await DepositsDB.updateOne(deposits[0]);
    });

    it('should return matching item', async () => {
      const res = await DepositsDB.findOne({ txid: deposits[0].txid });
      expect(res).excluding('_id').to.deep.equal(deposits[0]);
    });

    it('should return null if no match', async () => {
      const res = await DepositsDB.findOne({ txid: deposits[1].txid });
      should.not.exist(res);
    });
  });

  describe('count', () => {
    it('should return the count', async () => {
      await DepositsDB.updateOne(deposits[0]);
      (await DepositsDB.count({})).should.equal(1);

      await DepositsDB.updateOne(deposits[1]);
      (await DepositsDB.count({})).should.equal(2);
    });
  });
});
