const chai = require('chai');
const chaiExclude = require('chai-exclude');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const createBlocks = require('../../../src/db/init/blocks');
const BlocksDB = require('../../../src/db/controllers/blocks');
const Block = require('../../../src/models/block');

chai.use(chaiExclude);
const should = chai.should();
const { expect } = chai;

describe('db/controllers/blocks', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  beforeEach(async () => {
    await createBlocks(db());
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  const blocks = [
    new Block({ blockNum: 1, blockTime: 3 }),
    new Block({ blockNum: 2, blockTime: 6 }),
    new Block({ blockNum: 3, blockTime: 9 }),
  ];

  describe('updateOne', () => {
    it('should insert if does not exist', async () => {
      let res = await BlocksDB.findOne({ blockNum: blocks[0].blockNum });
      should.not.exist(res);
      (await BlocksDB.count({})).should.equal(0);

      await BlocksDB.updateOne(blocks[0]);
      (await BlocksDB.count({})).should.equal(1);
      res = await BlocksDB.findOne({ blockNum: blocks[0].blockNum });
      should.exist(res);
    });

    it('should update if exists', async () => {
      await BlocksDB.updateOne(blocks[0]);
      let res = await BlocksDB.findOne({ blockNum: blocks[0].blockNum });
      expect(res).excluding('_id').to.deep.equal(blocks[0]);
      (await BlocksDB.count({})).should.equal(1);

      const updated = Object.assign({}, blocks[0]);
      updated.blockTime = 4;
      await BlocksDB.updateOne(updated);
      res = await BlocksDB.findOne({ blockNum: blocks[0].blockNum });
      expect(res.blockTime).to.equal(updated.blockTime);
      (await BlocksDB.count({})).should.equal(1);
    });
  });

  describe('find', () => {
    beforeEach(async () => {
      await BlocksDB.updateOne(blocks[0]);
      await BlocksDB.updateOne(blocks[1]);
    });

    it('should return items matching query', async () => {
      const res = await BlocksDB.find({});
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(blocks[0]);
      expect(res[1]).excluding('_id').to.deep.equal(blocks[1]);
    });

    it('should return items matching query with sort', async () => {
      const res = await BlocksDB.find(
        {},
        { orderBy: { field: 'blockNum', direction: 'DESC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(blocks[1]);
      expect(res[1]).excluding('_id').to.deep.equal(blocks[0]);
    });

    it('should return empty array if no matches', async () => {
      const res = await BlocksDB.find({
        txid: '0x1234567890123456789012345678901234567890',
      });
      res.length.should.equal(0);
    });
  });

  describe('findOne', () => {
    beforeEach(async () => {
      await BlocksDB.updateOne(blocks[0]);
    });

    it('should return matching item', async () => {
      const res = await BlocksDB.findOne({ blockNum: blocks[0].blockNum });
      should.exist(res);
      expect(res).excluding('_id').to.deep.equal(blocks[0]);
    });

    it('should return null if no match', async () => {
      const res = await BlocksDB.findOne({ blockNum: blocks[1].blockNum });
      should.not.exist(res);
    });
  });

  describe('count', () => {
    it('should return the count', async () => {
      await BlocksDB.updateOne(blocks[0]);
      (await BlocksDB.count({})).should.equal(1);

      await BlocksDB.updateOne(blocks[1]);
      (await BlocksDB.count({})).should.equal(2);
    });
  });

  describe('findLatestBlock', () => {
    it('should return the latest block', async () => {
      await BlocksDB.updateOne(blocks[0]);
      await BlocksDB.updateOne(blocks[1]);
      await BlocksDB.updateOne(blocks[2]);

      const res = await BlocksDB.findLatestBlock();
      expect(res).excluding('_id').to.deep.equal(blocks[2]);
    });
  });
});
