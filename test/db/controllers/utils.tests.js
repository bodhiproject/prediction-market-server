const chai = require('chai');
const chaiExclude = require('chai-exclude');
const fs = require('fs-extra');
const { map } = require('lodash');
const { initDB, closeDB, db } = require('../../../src/db');
const BetsDB = require('../../../src/db/controllers/bets');
const MongoBet = require('../../../src/db/parsers/bet');
const Bet = require('../../../src/models/bet');
const { applyCursorOptions } = require('../../../src/db/controllers/utils');

chai.use(chaiExclude);
const { expect } = chai;

describe('db/controllers/utils', () => {
  const bets = [
    new Bet({
      txid: '0x445ffdcfb4b3e30c3c8d73a7583230b60eff51054035389df7cf187f5d3f75b6',
      txStatus: 'SUCCESS',
      blockNum: 4000000,
      eventAddress: '0xbeaa4358aa4434227c4545544e50c1b1a52a51c6',
      betterAddress: '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
      resultIndex: 2,
      amount: '1100000000',
      eventRound: 0,
      eventType: 'MR_EVENT',
    }),
    new Bet({
      txid: '0xe314e29163785d1361880588f252c039016943bf4de494b7ae0869fc9897fe13',
      txStatus: 'SUCCESS',
      blockNum: 3000000,
      eventAddress: '0xbbdfec793ef800769898795f469fc3951dc21eea',
      betterAddress: '0x7937a1e86f2cb43d6c91d27ca7a4f93c7f7189c3',
      resultIndex: 1,
      amount: '10000000',
      eventRound: 0,
      eventType: 'MR_EVENT',
    }),
    new Bet({
      txid: '0x81fcd2aec9f2f45a96eea6deb858a76fecce619df091f62ec92ec3bce6181d18',
      txStatus: 'SUCCESS',
      blockNum: 2000000,
      eventAddress: '0xbbdfec793ef800769898795f469fc3951dc21eea',
      betterAddress: '0x7937a1e86f2cb43d6c91d27ca7a4f93c7f7189c3',
      resultIndex: 1,
      amount: '10000000',
      eventRound: 0,
      eventType: 'MR_EVENT',
    }),
  ];

  before(async () => {
    await initDB();
  });

  afterEach(async () => {
    await db().collection('bets').deleteMany({});
  });

  after(async () => {
    await closeDB();
  });

  describe('applyCursorOptions', () => {
    beforeEach(async () => {
      await BetsDB.updateOne(bets[0]);
      await BetsDB.updateOne(bets[1]);
      await BetsDB.updateOne(bets[2]);
      const res = await BetsDB.find({});
      res.length.should.equal(3);
    });

    it('should sort with the orderBy field', async () => {
      // orderBy as object
      let cursor = db().collection('bets').find({});
      cursor = applyCursorOptions(
        cursor,
        { orderBy: { field: 'blockNum', direction: 'ASC' } },
      );
      let arr = map(await cursor.toArray(), bet => MongoBet.decode(bet));
      expect(arr[0]).excluding('_id').to.deep.equal(bets[2]);
      expect(arr[1]).excluding('_id').to.deep.equal(bets[1]);
      expect(arr[2]).excluding('_id').to.deep.equal(bets[0]);

      cursor = db().collection('bets').find({});
      cursor = applyCursorOptions(
        cursor,
        { orderBy: { field: 'blockNum', direction: 'DESC' } },
      );
      arr = map(await cursor.toArray(), bet => MongoBet.decode(bet));
      expect(arr[0]).excluding('_id').to.deep.equal(bets[0]);
      expect(arr[1]).excluding('_id').to.deep.equal(bets[1]);
      expect(arr[2]).excluding('_id').to.deep.equal(bets[2]);

      // orderBy as array
      cursor = db().collection('bets').find({});
      cursor = applyCursorOptions(
        cursor,
        { orderBy: [{ field: 'blockNum', direction: 'ASC' }] },
      );
      arr = map(await cursor.toArray(), bet => MongoBet.decode(bet));
      expect(arr[0]).excluding('_id').to.deep.equal(bets[2]);
      expect(arr[1]).excluding('_id').to.deep.equal(bets[1]);
      expect(arr[2]).excluding('_id').to.deep.equal(bets[0]);

      cursor = db().collection('bets').find({});
      cursor = applyCursorOptions(
        cursor,
        { orderBy: [{ field: 'blockNum', direction: 'DESC' }] },
      );
      arr = map(await cursor.toArray(), bet => MongoBet.decode(bet));
      expect(arr[0]).excluding('_id').to.deep.equal(bets[0]);
      expect(arr[1]).excluding('_id').to.deep.equal(bets[1]);
      expect(arr[2]).excluding('_id').to.deep.equal(bets[2]);
    });

    it('should skip with the skip field', async () => {
      let cursor = db().collection('bets').find({});
      cursor = applyCursorOptions(cursor, { skip: 1 });
      const arr = map(await cursor.toArray(), bet => MongoBet.decode(bet));
      expect(arr.length).to.equal(2);
      expect(arr[0]).excluding('_id').to.deep.equal(bets[1]);
      expect(arr[1]).excluding('_id').to.deep.equal(bets[2]);
    });

    it('should limit with the limit field', async () => {
      let cursor = db().collection('bets').find({});
      cursor = applyCursorOptions(cursor, { limit: 1 });
      const arr = map(await cursor.toArray(), bet => MongoBet.decode(bet));
      expect(arr.length).to.equal(1);
      expect(arr[0]).excluding('_id').to.deep.equal(bets[0]);
    });
  });
});
