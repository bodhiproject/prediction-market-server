const chai = require('chai');
const chaiExclude = require('chai-exclude');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const createTxReceipts = require('../../../src/db/init/tx-receipts');
const TxReceiptsDB = require('../../../src/db/controllers/tx-receipts');
const TxReceipt = require('../../../src/models/tx-receipt');

chai.use(chaiExclude);
const should = chai.should();
const { expect } = chai;

describe('db/controllers/tx-receipts', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  beforeEach(async () => {
    await createTxReceipts(db());
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  const pendingTxReceipts = [
    new TxReceipt({
      blockHash: null,
      blockNum: null,
      transactionHash: '0x81fcd2aec9f2f45a96eea6deb858a76fecce619df091f62ec92ec3bce6181d18',
      from: '0x47ba776b3ed5d514d3e206ffee72fa483baffa7e',
      to: '0x809388c8770c578cb87df1d1e84e3436d8156fda',
      cumulativeGasUsed: 500000,
      gasUsed: 500000,
      gasPrice: '47619047620',
    }),
  ];

  const txReceipts = [
    new TxReceipt({
      status: true,
      blockHash: '0x67be035531840561747a4c1bf9aa32a31641367ae1270db74cc321c9eb78e82f',
      blockNum: 4400858,
      transactionHash: '0x81fcd2aec9f2f45a96eea6deb858a76fecce619df091f62ec92ec3bce6181d18',
      from: '0x47ba776b3ed5d514d3e206ffee72fa483baffa7e',
      to: '0x809388c8770c578cb87df1d1e84e3436d8156fda',
      contractAddress: null,
      cumulativeGasUsed: 207559,
      gasUsed: 207559,
      gasPrice: '47619047620',
    }),
    new TxReceipt({
      status: true,
      blockHash: '0x039e5b65879c4dab2e81b674d6b4e9a81538df83b9241c1730eeb619b3e95501',
      blockNum: 3400858,
      transactionHash: '0x7e127a113d605559fe1f775d2b33857c2d7210450a1beaf7927ce0dcc8564bd8',
      from: '0x47ba776b3ed5d514d3e206ffee72fa483baffa7e',
      to: '0x809388c8770c578cb87df1d1e84e3436d8156fda',
      contractAddress: null,
      cumulativeGasUsed: 207559,
      gasUsed: 207559,
      gasPrice: '47619047620',
    }),
  ];

  describe('updateOne', () => {
    it('should insert if does not exist', async () => {
      let res = await TxReceiptsDB.findOne({
        transactionHash: txReceipts[0].transactionHash,
      });
      should.not.exist(res);
      (await TxReceiptsDB.count({})).should.equal(0);

      await TxReceiptsDB.updateOne(txReceipts[0]);
      (await TxReceiptsDB.count({})).should.equal(1);
      res = await TxReceiptsDB.findOne({
        transactionHash: txReceipts[0].transactionHash,
      });
      expect(res).excluding(['_id']).to.deep.equal(txReceipts[0]);
    });

    it('should update if exists', async () => {
      await TxReceiptsDB.updateOne(pendingTxReceipts[0]);
      let res = await TxReceiptsDB.findOne({
        transactionHash: pendingTxReceipts[0].transactionHash,
      });
      (await TxReceiptsDB.count({})).should.equal(1);
      expect(res).excluding(['_id']).to.deep.equal(pendingTxReceipts[0]);

      await TxReceiptsDB.updateOne(txReceipts[0]);
      (await TxReceiptsDB.count({})).should.equal(1);
      res = await TxReceiptsDB.findOne({
        transactionHash: pendingTxReceipts[0].transactionHash,
      });
      expect(res).excluding(['_id']).to.deep.equal(txReceipts[0]);
    });
  });

  describe('find', () => {
    beforeEach(async () => {
      await TxReceiptsDB.updateOne(txReceipts[0]);
      await TxReceiptsDB.updateOne(txReceipts[1]);
    });

    it('should return items matching query', async () => {
      let res = await TxReceiptsDB.find({});
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(txReceipts[0]);
      expect(res[1]).excluding('_id').to.deep.equal(txReceipts[1]);

      res = await TxReceiptsDB.find({
        transactionHash: txReceipts[0].transactionHash,
      });
      res.length.should.equal(1);
      expect(res[0]).excluding('_id').to.deep.equal(txReceipts[0]);
    });

    it('should return items matching query with sort', async () => {
      const res = await TxReceiptsDB.find(
        {},
        { orderBy: { field: 'blockNum', direction: 'ASC' } },
      );
      res.length.should.equal(2);
      expect(res[0]).excluding('_id').to.deep.equal(txReceipts[1]);
      expect(res[1]).excluding('_id').to.deep.equal(txReceipts[0]);
    });

    it('should return empty array if no matches', async () => {
      const res = await TxReceiptsDB.find({
        transactionHash: '0x1234567890123456789012345678901234567890123456789012345678901234',
      });
      res.length.should.equal(0);
    });
  });

  describe('findOne', () => {
    beforeEach(async () => {
      await TxReceiptsDB.updateOne(txReceipts[0]);
    });

    it('should return matching item', async () => {
      const res = await TxReceiptsDB.findOne({
        transactionHash: txReceipts[0].transactionHash,
      });
      should.exist(res);
      expect(res).excluding('_id').to.deep.equal(txReceipts[0]);
    });

    it('should return null if no match', async () => {
      const res = await TxReceiptsDB.findOne({
        transactionHash: txReceipts[1].transactionHash,
      });
      should.not.exist(res);
    });
  });

  describe('count', () => {
    it('should return the count', async () => {
      await TxReceiptsDB.updateOne(txReceipts[0]);
      (await TxReceiptsDB.count({})).should.equal(1);

      await TxReceiptsDB.updateOne(txReceipts[1]);
      (await TxReceiptsDB.count({})).should.equal(2);
    });
  });
});
