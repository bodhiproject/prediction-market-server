const chai = require('chai');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const { checkCollectionExists } = require('../../../src/db/init/utils');
const createWithdraws = require('../../../src/db/init/withdraws');

const { assert } = chai;

describe('db/init/withdraws', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  describe('function', () => {
    it('creates the collection if it does not exist', async () => {
      assert.isFalse(await checkCollectionExists(db(), 'withdraws'));
      await createWithdraws(db());
      assert.isTrue(await checkCollectionExists(db(), 'withdraws'));
    });

    it('skips creating the collection if it exists', async () => {
      assert.isFalse(await checkCollectionExists(db(), 'withdraws'));
      await createWithdraws(db());
      assert.isTrue(await checkCollectionExists(db(), 'withdraws'));
      await createWithdraws(db());
    });
  });
});
