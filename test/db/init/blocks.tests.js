const chai = require('chai');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const { checkCollectionExists } = require('../../../src/db/init/utils');
const createBlocks = require('../../../src/db/init/blocks');

const { assert } = chai;

describe('db/init/blocks', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  describe('function', () => {
    it('creates the collection if it does not exist', async () => {
      assert.isFalse(await checkCollectionExists(db(), 'blocks'));
      await createBlocks(db());
      assert.isTrue(await checkCollectionExists(db(), 'blocks'));
    });

    it('skips creating the collection if it exists', async () => {
      assert.isFalse(await checkCollectionExists(db(), 'blocks'));
      await createBlocks(db());
      assert.isTrue(await checkCollectionExists(db(), 'blocks'));
      await createBlocks(db());
    });
  });
});
