const chai = require('chai');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const { checkCollectionExists } = require('../../../src/db/init/utils');
const createNames = require('../../../src/db/init/names');

const { assert } = chai;

describe('db/init/names', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  describe('function', () => {
    it('creates the collection if it does not exist', async () => {
      assert.isFalse(await checkCollectionExists(db(), 'names'));
      await createNames(db());
      assert.isTrue(await checkCollectionExists(db(), 'names'));
    });

    it('skips creating the collection if it exists', async () => {
      assert.isFalse(await checkCollectionExists(db(), 'names'));
      await createNames(db());
      assert.isTrue(await checkCollectionExists(db(), 'names'));
      await createNames(db());
    });
  });
});
