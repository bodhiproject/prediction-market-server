const chai = require('chai');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const { checkCollectionExists } = require('../../../src/db/init/utils');
const createEvents = require('../../../src/db/init/events');

const { assert } = chai;

describe('db/init/events', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  describe('function', () => {
    it('creates the collection if it does not exist', async () => {
      assert.isFalse(await checkCollectionExists(db(), 'events'));
      await createEvents(db());
      assert.isTrue(await checkCollectionExists(db(), 'events'));
    });

    it('skips creating the collection if it exists', async () => {
      assert.isFalse(await checkCollectionExists(db(), 'events'));
      await createEvents(db());
      assert.isTrue(await checkCollectionExists(db(), 'events'));
      await createEvents(db());
    });
  });
});
