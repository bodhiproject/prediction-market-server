const chai = require('chai');
const fs = require('fs-extra');
const { connectDB, closeDB, db } = require('../../../src/db');
const { checkCollectionExists } = require('../../../src/db/init/utils');
const createResultSets = require('../../../src/db/init/result-sets');

const { assert } = chai;

describe('db/init/result-sets', () => {
  before(async () => {
    await connectDB();
    await db().dropDatabase();
  });

  afterEach(async () => {
    await db().dropDatabase();
  });

  after(async () => {
    await closeDB();
  });

  describe('function', () => {
    it('creates the collection if it does not exist', async () => {
      assert.isFalse(await checkCollectionExists(db(), 'resultSets'));
      await createResultSets(db());
      assert.isTrue(await checkCollectionExists(db(), 'resultSets'));
    });

    it('skips creating the collection if it exists', async () => {
      assert.isFalse(await checkCollectionExists(db(), 'resultSets'));
      await createResultSets(db());
      assert.isTrue(await checkCollectionExists(db(), 'resultSets'));
      await createResultSets(db());
    });
  });
});
