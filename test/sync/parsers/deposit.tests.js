const { assert } = require('chai');
const parseDeposit = require('../../../src/sync/parsers/deposit');
const { TX_STATUS } = require('../../../src/constants');
const { equalIgnoreCase } = require('../../assert-utils');

const log = {
  address: '0xa7bfE43003Ee2a8dCfa84f2C0a4a49c9a65f9869',
  topics: [
    '0x87e099c6d69051bce6a39e9d80a7a19dd11037bad20d3bb29ff70a289a7deba0',
    '0x000000000000000000000000a7bfe43003ee2a8dcfa84f2c0a4a49c9a65f9869',
    '0x000000000000000000000000939592864c0bd3355b2d54e4fa2203e8343b6d6a',
  ],
  data: '0x00000000000000000000000000000000000000000000000000000002540be400',
  blockNumber: 5642208,
  transactionHash: '0xd7f3c220c5707e35914838a1a6b74b55f2e81d92d4a7db715d6f227ab18436ae',
  transactionIndex: 0,
  blockHash: '0x60f3eae633fa76a24bb6506c173a75fef7fd7ed60608e887ecba28bb70adb518',
  logIndex: 2,
  payByToken: false,
  removed: false,
  id: 'log_78a10bbb',
};

describe('sync/parsers/deposit', () => {
  it('parses the log', () => {
    const deposit = parseDeposit({ log });

    assert.isString(deposit.txid);
    equalIgnoreCase(
      deposit.txid,
      '0xd7f3c220c5707e35914838a1a6b74b55f2e81d92d4a7db715d6f227ab18436ae',
    );

    assert.isString(deposit.txStatus);
    assert.equal(deposit.txStatus, TX_STATUS.SUCCESS);

    assert.isNumber(deposit.blockNum);
    assert.equal(deposit.blockNum, 5642208);

    assert.isString(deposit.eventAddress);
    equalIgnoreCase(
      deposit.eventAddress,
      '0xa7bfe43003ee2a8dcfa84f2c0a4a49c9a65f9869',
    );

    assert.isString(deposit.depositorAddress);
    equalIgnoreCase(
      deposit.depositorAddress,
      '0x939592864c0bd3355b2d54e4fa2203e8343b6d6a',
    );

    assert.isString(deposit.amount);
    assert.equal(deposit.amount, '10000000000');
  });
});
