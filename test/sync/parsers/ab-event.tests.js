const { assert } = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();
const Web3 = require('web3');
const { each, map } = require('lodash');
const { TX_STATUS, EVENT_TYPE } = require('../../../src/constants');
const Config = require('../../../src/config');
const ABEvent = require('../../../src/config/contracts/ab-event');
const { equalIgnoreCase } = require('../../assert-utils');

const { utils: { utf8ToHex, toBN } } = new Web3();

const log = {
  address: '0xbcb03d8a0b74ff30d0752846b7810ec3f2a0b8fa',
  topics: [
    '0x909b66daf61d278163ec9649d2ac4f75b777dd7c22aafb10677c1c5b5c091ee0',
    '0x00000000000000000000000095e7b8f56467442beece9f112b30bfd83646bdd0',
    '0x00000000000000000000000047ba776b3ed5d514d3e206ffee72fa483baffa7e',
  ],
  data: '0x',
  blockNumber: 6044186,
  blockHash: '0x1e1fe79124d975873754e0723656f71512d87cdc2d8ee8db17e5ff5134c590fc',
  transactionHash: '0xe0dee33cfc4c7c4f435b8f133c9f4b33ae337466d12fe4bcf9808adcf84da8e8',
  transactionIndex: 0,
  logIndex: 6,
};

describe('sync/parsers/ab-event', () => {
  let stubGetEventMeta;
  let parseEvent;

  beforeEach(() => {
    stubGetEventMeta = sinon.stub(Config, 'getEventMeta')
      .returns(ABEvent['0']);
    parseEvent = proxyquire('../../../src/sync/parsers/ab-event', {
      '../../utils/web3-utils': {
        getContract: () => ({
          methods: ({
            eventMetadata: () => ({
              call: async () => [
                0,
                'Test Event',
                map(['Invalid', 'A', 'B'], item => utf8ToHex(item)),
                3,
                [0, 75, 25],
                [toBN(0), toBN(120), toBN(360)],
              ],
            }),
            centralizedMetadata: () => ({
              call: async () => [
                '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
                toBN(1),
                toBN(2),
                toBN(3),
                toBN(4),
              ],
            }),
            configMetadata: () => ({
              call: async () => [
                toBN('100000000'),
                toBN(300),
                toBN(10),
                toBN(10),
              ],
            }),
            currentConsensusThreshold: () => ({
              call: async () => toBN('10000000000'),
            }),
            currentArbitrationEndTime: () => ({
              call: async () => toBN(0),
            }),
            maxBets: () => ({
              call: async () => [
                toBN(0),
                toBN('7500000000'),
                toBN('2500000000'),
              ],
            }),
          }),
        }),
      },
    });
  });

  afterEach(() => {
    stubGetEventMeta.restore();
    sinon.restore();
    proxyquire.callThru();
  });

  it('parses the log and fetches other data', async () => {
    const event = await parseEvent({ log });
    assert.isString(event.txid);
    equalIgnoreCase(
      event.txid,
      '0xe0dee33cfc4c7c4f435b8f133c9f4b33ae337466d12fe4bcf9808adcf84da8e8',
    );

    assert.isString(event.txStatus);
    assert.equal(event.txStatus, TX_STATUS.SUCCESS);

    assert.isNumber(event.blockNum);
    assert.equal(event.blockNum, 6044186);

    assert.isString(event.address);
    equalIgnoreCase(event.address, '0x95e7b8f56467442beece9f112b30bfd83646bdd0');

    assert.isString(event.ownerAddress);
    equalIgnoreCase(event.ownerAddress, '0x47ba776b3ed5d514d3e206ffee72fa483baffa7e');

    assert.isNumber(event.version);
    assert.equal(event.version, 0);

    assert.isString(event.name);
    assert.equal(event.name, 'Test Event');

    each(event.results, result => assert.isString(result));
    assert.deepEqual(event.results, ['Invalid', 'A', 'B']);

    assert.isNumber(event.numOfResults);
    assert.equal(event.numOfResults, 3);

    assert.isArray(event.winningChances);
    assert.deepEqual(event.winningChances, [0, 75, 25]);

    assert.isArray(event.odds);
    assert.deepEqual(event.odds, [0, 1.2, 3.6]);

    assert.isString(event.centralizedOracle);
    equalIgnoreCase(event.centralizedOracle, '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428');

    assert.isNumber(event.betStartTime);
    assert.equal(event.betStartTime, 1);

    assert.isNumber(event.betEndTime);
    assert.equal(event.betEndTime, 2);

    assert.isNumber(event.resultSetStartTime);
    assert.equal(event.resultSetStartTime, 3);

    assert.isNumber(event.resultSetEndTime);
    assert.equal(event.resultSetEndTime, 4);

    assert.isString(event.escrowAmount);
    assert.equal(event.escrowAmount, '0');

    assert.isNumber(event.arbitrationLength);
    assert.equal(event.arbitrationLength, 300);

    assert.isNumber(event.thresholdPercentIncrease);
    assert.equal(event.thresholdPercentIncrease, 10);

    assert.isNumber(event.arbitrationRewardPercentage);
    assert.equal(event.arbitrationRewardPercentage, 10);

    assert.isString(event.consensusThreshold);
    assert.equal(event.consensusThreshold, '10000000000');

    assert.isNumber(event.arbitrationEndTime);
    assert.equal(event.arbitrationEndTime, 0);

    assert.isArray(event.maxBets);
    assert.deepEqual(event.maxBets, ['0', '7500000000', '2500000000']);

    assert.isString(event.eventType);
    assert.equal(event.eventType, EVENT_TYPE.AB_EVENT);
  });
});
