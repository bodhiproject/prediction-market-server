const { assert } = require('chai');
const parseWithdraw = require('../../../src/sync/parsers/withdraw');
const { TX_STATUS } = require('../../../src/constants');
const { equalIgnoreCase } = require('../../assert-utils');

const log = {
  address: '0x7f1B3616847099C1b55B10Fbf11c949BBE48E3A1',
  topics: [
    '0x716f7aca59b4262ef92c728237421a50f37a704fdfed0708db03dd161501a410',
    '0x0000000000000000000000007f1b3616847099c1b55b10fbf11c949bbe48e3a1',
    '0x00000000000000000000000047ba776b3ed5d514d3e206ffee72fa483baffa7e',
  ],
  data: '0x00000000000000000000000000000000000000000000000000000002540be4000000000000000000000000000000000000000000000000000000000022921900',
  blockNumber: 5950073,
  transactionHash: '0x70458ee65ab16c32ac0af1a9e8c31c73724fc0b0daea9b0f91a281d252216a5e',
  transactionIndex: 0,
  blockHash: '0xb0fc0cb216f6640573b51b21ec81e570391b16abddf479fe4c87a368b8a7887f',
  logIndex: 4,
  payByToken: false,
  removed: false,
  id: 'log_d7e18e2f',
};

describe('sync/parsers/withdraw', () => {
  it('parses the log', () => {
    const withdraw = parseWithdraw({ log });

    assert.isString(withdraw.txid);
    equalIgnoreCase(
      withdraw.txid,
      '0x70458ee65ab16c32ac0af1a9e8c31c73724fc0b0daea9b0f91a281d252216a5e',
    );

    assert.isString(withdraw.txStatus);
    assert.equal(withdraw.txStatus, TX_STATUS.SUCCESS);

    assert.isNumber(withdraw.blockNum);
    assert.equal(withdraw.blockNum, 5950073);

    assert.isString(withdraw.eventAddress);
    equalIgnoreCase(
      withdraw.eventAddress,
      '0x7f1b3616847099c1b55b10fbf11c949bbe48e3a1',
    );

    assert.isString(withdraw.winnerAddress);
    equalIgnoreCase(
      withdraw.winnerAddress,
      '0x47ba776b3ed5d514d3e206ffee72fa483baffa7e',
    );

    assert.isString(withdraw.winningAmount);
    assert.equal(withdraw.winningAmount, '10000000000');

    assert.isString(withdraw.creatorReturnAmount);
    assert.equal(withdraw.creatorReturnAmount, '580000000');
  });
});
