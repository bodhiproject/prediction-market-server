const { assert } = require('chai');
const Deposit = require('../../src/models/deposit');
const { TX_STATUS, TX_TYPE } = require('../../src/constants');

const rawInput = {
  txid: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
  txStatus: TX_STATUS.SUCCESS,
  blockNum: 5,
  eventAddress: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
  depositorAddress: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
  amount: '1000000',
};

describe('models/deposit', () => {
  let input;
  describe('validate', () => {
    beforeEach(() => {
      input = {};
      Object.assign(input, rawInput);
    });

    it('It should throw if txid is missing', () => {
      delete input.txid;
      assert.throws(() => new Deposit(input), Error, 'txid must be a String'); // must be double quotes
    });

    it('It should throw if txid is null', () => {
      input.txid = null;
      assert.throws(() => new Deposit(input), Error, 'txid must be a String');
    });

    it('It should throw if txid is undefined', () => {
      input.txid = undefined;
      assert.throws(() => new Deposit(input), Error, 'txid must be a String');
    });

    it('It should throw if txid is number', () => {
      input.txid = 12;
      assert.throws(() => new Deposit(input), Error, 'txid must be a String');
    });

    it('It should throw if txid is array', () => {
      input.txid = ['0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428'];
      assert.throws(() => new Deposit(input), Error, 'txid must be a String');
    });

    it('It should throw if txid is object', () => {
      input.txid = { id: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428' };
      assert.throws(() => new Deposit(input), Error, 'txid must be a String');
    });

    it('It should throw if txStatus is missing', () => {
      delete input.txStatus;
      assert.throws(() => new Deposit(input), Error, 'txStatus must be a String');
    });

    it('It should throw if txStatus is null', () => {
      input.txStatus = null;
      assert.throws(() => new Deposit(input), Error, 'txStatus must be a String');
    });

    it('It should throw if txStatus is undefined', () => {
      input.txStatus = undefined;
      assert.throws(() => new Deposit(input), Error, 'txStatus must be a String');
    });

    it('It should throw if txStatus is number', () => {
      input.txStatus = 12;
      assert.throws(() => new Deposit(input), Error, 'txStatus must be a String');
    });

    it('It should throw if txStatus is array', () => {
      input.txStatus = [TX_STATUS.SUCCESS];
      assert.throws(() => new Deposit(input), Error, 'txStatus must be a String');
    });

    it('It should throw if txStatus is object', () => {
      input.txStatus = { id: TX_STATUS.SUCCESS };
      assert.throws(() => new Deposit(input), Error, 'txStatus must be a String');
    });

    it('It should throw if amount is missing', () => {
      delete input.amount;
      assert.throws(() => new Deposit(input), Error, 'amount must be a String');
    });

    it('It should throw if amount is null', () => {
      input.amount = null;
      assert.throws(() => new Deposit(input), Error, 'amount must be a String');
    });

    it('It should throw if amount is undefined', () => {
      input.amount = undefined;
      assert.throws(() => new Deposit(input), Error, 'amount must be a String');
    });

    it('It should throw if amount is number', () => {
      input.amount = 12;
      assert.throws(() => new Deposit(input), Error, 'amount must be a String');
    });

    it('It should throw if amount is array', () => {
      input.amount = ['100000'];
      assert.throws(() => new Deposit(input), Error, 'amount must be a String');
    });

    it('It should throw if amount is object', () => {
      input.amount = { id: '100000' };
      assert.throws(() => new Deposit(input), Error, 'amount must be a String');
    });

    it('It should throw if eventAddress is missing', () => {
      delete input.eventAddress;
      assert.throws(() => new Deposit(input), Error, 'eventAddress must be a String');
    });

    it('It should throw if eventAddress is null', () => {
      input.eventAddress = null;
      assert.throws(() => new Deposit(input), Error, 'eventAddress must be a String');
    });

    it('It should throw if eventAddress is undefined', () => {
      input.eventAddress = undefined;
      assert.throws(() => new Deposit(input), Error, 'eventAddress must be a String');
    });

    it('It should throw if eventAddress is number', () => {
      input.eventAddress = 12;
      assert.throws(() => new Deposit(input), Error, 'eventAddress must be a String');
    });

    it('It should throw if eventAddress is array', () => {
      input.eventAddress = ['0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428'];
      assert.throws(() => new Deposit(input), Error, 'eventAddress must be a String');
    });

    it('It should throw if eventAddress is object', () => {
      input.eventAddress = { id: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428' };
      assert.throws(() => new Deposit(input), Error, 'eventAddress must be a String');
    });

    it('It should throw if depositorAddress is missing', () => {
      delete input.depositorAddress;
      assert.throws(() => new Deposit(input), Error, 'depositorAddress must be a String');
    });

    it('It should throw if depositorAddress is null', () => {
      input.depositorAddress = null;
      assert.throws(() => new Deposit(input), Error, 'depositorAddress must be a String');
    });

    it('It should throw if depositorAddress is undefined', () => {
      input.depositorAddress = undefined;
      assert.throws(() => new Deposit(input), Error, 'depositorAddress must be a String');
    });

    it('It should throw if depositorAddress is number', () => {
      input.depositorAddress = 12;
      assert.throws(() => new Deposit(input), Error, 'depositorAddress must be a String');
    });

    it('It should throw if depositorAddress is array', () => {
      input.depositorAddress = ['0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428'];
      assert.throws(() => new Deposit(input), Error, 'depositorAddress must be a String');
    });

    it('It should throw if depositorAddress is object', () => {
      input.depositorAddress = { id: '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428' };
      assert.throws(() => new Deposit(input), Error, 'depositorAddress must be a String');
    });

    it('it should format all the fields', () => {
      const deposit = new Deposit(input);
      assert.equal(deposit.txid, input.txid);
      assert.equal(deposit.txStatus, input.txStatus);
      assert.equal(deposit.blockNum, input.blockNum);
      assert.equal(deposit.eventAddress, input.eventAddress);
      assert.equal(deposit.depositorAddress, input.depositorAddress);
      assert.equal(deposit.amount, input.amount);
      assert.equal(deposit.txType, TX_TYPE.DEPOSIT);
    });

    it('It should format the txid to be lowercase', () => {
      input.txid = input.txid.toUpperCase();
      const deposit = new Deposit(input);
      assert.equal(deposit.txid, input.txid.toLowerCase());
    });

    it('It should format the eventAddress to be lowercase', () => {
      input.eventAddress = input.eventAddress.toUpperCase();
      const deposit = new Deposit(input);
      assert.equal(deposit.eventAddress, input.eventAddress.toLowerCase());
    });

    it('It should format the depositorAddress to be lowercase', () => {
      input.depositorAddress = input.depositorAddress.toUpperCase();
      const deposit = new Deposit(input);
      assert.equal(deposit.depositorAddress, input.depositorAddress.toLowerCase());
    });
  });
});
