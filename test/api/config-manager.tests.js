const Chai = require('chai');
const sinon = require('sinon');
const ChaiAsPromised = require('chai-as-promised');
const proxyquire = require('proxyquire').noCallThru();

Chai.use(ChaiAsPromised);
const { assert, expect } = Chai;

describe('api/config-manager', () => {
  let ConfigManager;

  beforeEach(() => {
    ConfigManager = proxyquire('../../src/api/config-manager', {
      '../config': {
        isMainnet: () => false,
        configManagerMeta: () => ({}),
      },
      '../utils/web3-utils': {
        getContract: () => ({
          methods: ({
            version: () => ({ call: async () => 0 }),
            getAddress: () => ({
              call: async () => '0x809388c8770c578cb87df1d1e84e3436d8156fda',
            }),
            eventEscrowAmount: () => ({ call: async () => '10000000000' }),
            arbitrationLength: () => ({
              call: async () => ['172800', '86400', '43200', '21600'],
            }),
            startingConsensusThreshold: () => ({
              call: async () => ['100000000', '200000000', '400000000', '800000000'],
            }),
            thresholdPercentIncrease: () => ({ call: async () => '10' }),
          }),
        }),
      },
    });
  });

  afterEach(() => {
    sinon.restore();
    proxyquire.callThru();
  });

  describe('version()', () => {
    it('returns the version', async () => {
      const res = await ConfigManager.version();
      assert.isDefined(res);
      assert.isNumber(res);
    });
  });

  describe('getAddress()', () => {
    it('returns the address', async () => {
      const res = await ConfigManager.getAddress({ key: 'bodhitoken' });
      assert.isDefined(res);
      assert.isString(res);
    });

    it('throws if key is undefined', () => {
      expect(ConfigManager.getAddress()).to.be.rejectedWith(Error);
    });
  });

  describe('eventEscrowAmount()', () => {
    it('returns the eventEscrowAmount', async () => {
      const res = await ConfigManager.eventEscrowAmount();
      assert.isDefined(res);
      assert.isString(res);
    });
  });

  describe('arbitrationLength()', () => {
    it('returns the arbitrationLength', async () => {
      const res = await ConfigManager.arbitrationLength();
      assert.isDefined(res);
      assert.isArray(res);
      assert.isTrue(res.length === 4);

      assert.isNumber(res[0]);
      assert.isNumber(res[1]);
      assert.isNumber(res[2]);
      assert.isNumber(res[3]);
    });
  });

  describe('startingConsensusThreshold()', () => {
    it('returns the startingConsensusThreshold', async () => {
      const res = await ConfigManager.startingConsensusThreshold();
      assert.isDefined(res);
      assert.isArray(res);
      assert.isTrue(res.length === 4);

      assert.isString(res[0]);
      assert.isString(res[1]);
      assert.isString(res[2]);
      assert.isString(res[3]);
    });
  });

  describe('thresholdPercentIncrease()', () => {
    it('returns the thresholdPercentIncrease', async () => {
      const res = await ConfigManager.thresholdPercentIncrease();
      assert.isDefined(res);
      assert.isNumber(res);
    });
  });
});
