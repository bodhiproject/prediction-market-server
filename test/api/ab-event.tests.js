const { each, map } = require('lodash');
const Chai = require('chai');
const ChaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();
const Web3 = require('web3');

Chai.use(ChaiAsPromised);
const { assert, expect } = Chai;
const { utils: { utf8ToHex, toBN } } = new Web3();

const eventAddress = '0x8b2b1838efff78e5ed9e00d95d9ef071f2c27be6';
const address = '0x939592864C0Bd3355B2D54e4fA2203E8343B6d6a';

describe('api/ab-event', () => {
  let ABEvent;

  beforeEach(() => {
    ABEvent = proxyquire('../../src/api/ab-event', {
      '../db/controllers/events': { findOne: () => ({}) },
      '../config': { abEventMeta: () => ({}) },
      '../utils/web3-utils': {
        getContract: () => ({
          methods: ({
            maxBets: () => ({
              call: async () => ['0', '7500000000', '2500000000'],
            }),
            calculateWinnings: () => ({ call: async () => '10200000000' }),
            calculateEscrowAndUnpaidBet: () => ({
              call: async () => ['9800000000', '900000000'],
            }),
            getWithdrawAmounts: () => ({
              call: async () => ['7500000000', '2500000000', '100000000'],
            }),
            version: () => ({ call: async () => 0 }),
            currentRound: () => ({ call: async () => 0 }),
            currentResultIndex: () => ({ call: async () => 0 }),
            currentConsensusThreshold: () => ({
              call: async () => toBN('10000000000'),
            }),
            currentArbitrationEndTime: () => ({ call: async () => toBN(0) }),
            eventMetadata: () => ({
              call: async () => [
                0,
                'Test Event',
                map(['Invalid', 'A', 'B'], item => utf8ToHex(item)),
                3,
                [0, 75, 25],
                [toBN(0), toBN(120), toBN(360)],
              ],
            }),
            centralizedMetadata: () => ({
              call: async () => [
                '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
                toBN(1),
                toBN(2),
                toBN(3),
                toBN(4),
              ],
            }),
            configMetadata: () => ({
              call: async () => [
                toBN('100000000'),
                toBN(300),
                toBN(10),
                toBN(10),
              ],
            }),
            totalBets: () => ({ call: async () => '10200000000' }),
            didWithdraw: () => ({ call: async () => false }),
          }),
        }),
      },
    });
  });

  afterEach(() => {
    sinon.restore();
    proxyquire.callThru();
  });

  describe('calculateWinnings()', () => {
    it('returns the calculateWinnings', async () => {
      const res = await ABEvent.calculateWinnings({ eventAddress, address });
      assert.isDefined(res);
      assert.isString(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.calculateWinnings({ address })).to.be.rejectedWith(Error);
    });

    it('throws if address is undefined', () => {
      expect(ABEvent.calculateWinnings({ eventAddress })).to.be.rejectedWith(Error);
    });
  });

  describe('getWithdrawAmounts()', () => {
    it('returns the getWithdrawAmounts', async () => {
      const res = await ABEvent.getWithdrawAmounts({ eventAddress, address });
      assert.isArray(res);
      assert.isTrue(res.length === 3);
      each(res, item => assert.isString(item));
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.getWithdrawAmounts({ address })).to.be.rejectedWith(Error);
    });

    it('throws if address is undefined', () => {
      expect(ABEvent.getWithdrawAmounts({ eventAddress })).to.be.rejectedWith(Error);
    });
  });

  describe('calculateEscrowAndUnpaidBet()', () => {
    it('returns the calculateEscrowAndUnpaidBet', async () => {
      const res = await ABEvent.calculateEscrowAndUnpaidBet({ eventAddress });
      assert.isDefined(res);
      assert.isArray(res);
      assert.isTrue(res.length === 2);
      assert.isString(res[0]);
      assert.isString(res[1]);
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.calculateEscrowAndUnpaidBet({})).to.be.rejectedWith(Error);
    });
  });

  describe('version()', () => {
    it('returns the version', async () => {
      const res = await ABEvent.version({ eventAddress });
      assert.isDefined(res);
      assert.isNumber(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.version({})).to.be.rejectedWith(Error);
    });
  });

  describe('currentRound()', () => {
    it('returns the currentRound', async () => {
      const res = await ABEvent.currentRound({ eventAddress });
      assert.isDefined(res);
      assert.isNumber(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.currentRound({})).to.be.rejectedWith(Error);
    });
  });

  describe('currentResultIndex()', () => {
    it('returns the currentResultIndex', async () => {
      const res = await ABEvent.currentResultIndex({ eventAddress });
      assert.isDefined(res);
      assert.isNumber(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.currentResultIndex({})).to.be.rejectedWith(Error);
    });
  });

  describe('currentConsensusThreshold()', () => {
    it('returns the currentConsensusThreshold', async () => {
      const res = await ABEvent.currentConsensusThreshold({ eventAddress });
      assert.isDefined(res);
      assert.isString(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.currentConsensusThreshold({})).to.be.rejectedWith(Error);
    });
  });

  describe('currentArbitrationEndTime()', () => {
    it('returns the currentArbitrationEndTime', async () => {
      const res = await ABEvent.currentArbitrationEndTime({ eventAddress });
      assert.isDefined(res);
      assert.isString(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.currentArbitrationEndTime({})).to.be.rejectedWith(Error);
    });
  });

  describe('eventMetadata()', () => {
    it('returns the eventMetadata', async () => {
      const res = await ABEvent.eventMetadata({ eventAddress });
      assert.isDefined(res);
      assert.isArray(res);
      assert.isTrue(res.length === 6);

      assert.isNumber(res[0]);
      assert.isString(res[1]);
      assert.isArray(res[2]);
      assert.isNumber(res[3]);
      assert.isTrue(res[2].length === res[3]);
      assert.isArray(res[4]);
      each(res[4], item => assert.isNumber(item));
      assert.isArray(res[5]);
      each(res[5], item => assert.isNumber(item));
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.eventMetadata({})).to.be.rejectedWith(Error);
    });
  });

  describe('centralizedMetadata()', () => {
    it('returns the centralizedMetadata', async () => {
      const res = await ABEvent.centralizedMetadata({ eventAddress });
      assert.isDefined(res);
      assert.isArray(res);
      assert.isTrue(res.length === 5);

      assert.isString(res[0]);
      assert.isNumber(res[1]);
      assert.isNumber(res[2]);
      assert.isNumber(res[3]);
      assert.isNumber(res[4]);
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.centralizedMetadata({})).to.be.rejectedWith(Error);
    });
  });

  describe('configMetadata()', () => {
    it('returns the configMetadata', async () => {
      const res = await ABEvent.configMetadata({ eventAddress });
      assert.isDefined(res);
      assert.isArray(res);
      assert.isTrue(res.length === 4);

      assert.isString(res[0]);
      assert.isNumber(res[1]);
      assert.isNumber(res[2]);
      assert.isNumber(res[3]);
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.configMetadata({})).to.be.rejectedWith(Error);
    });
  });

  describe('maxBets()', () => {
    it('returns the max bets', async () => {
      const res = await ABEvent.maxBets({ eventAddress });
      assert.isArray(res);
      each(res, item => assert.isString(item));
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.maxBets({})).to.be.rejectedWith(Error);
    });
  });

  describe('totalBets()', () => {
    it('returns the totalBets', async () => {
      const res = await ABEvent.totalBets({ eventAddress, address });
      assert.isDefined(res);
      assert.isString(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.totalBets({})).to.be.rejectedWith(Error);
    });
  });

  describe('didWithdraw()', () => {
    it('returns the didWithdraw', async () => {
      const res = await ABEvent.didWithdraw({ eventAddress, address });
      assert.isDefined(res);
      assert.isBoolean(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(ABEvent.didWithdraw({ address })).to.be.rejectedWith(Error);
    });

    it('throws if address is undefined', () => {
      expect(ABEvent.didWithdraw({ eventAddress })).to.be.rejectedWith(Error);
    });
  });
});
