const { each, map } = require('lodash');
const Chai = require('chai');
const ChaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();
const Web3 = require('web3');

Chai.use(ChaiAsPromised);
const { assert, expect } = Chai;
const { utils: { utf8ToHex, toBN } } = new Web3();

const eventAddress = '0x8b2b1838efff78e5ed9e00d95d9ef071f2c27be6';
const address = '0x939592864C0Bd3355B2D54e4fA2203E8343B6d6a';

describe('api/reward-event', () => {
  let RewardEvent;

  beforeEach(() => {
    RewardEvent = proxyquire('../../src/api/reward-event', {
      '../db/controllers/events': { findOne: () => ({}) },
      '../config': { rewardEventMeta: () => ({}) },
      '../utils/web3-utils': {
        getContract: () => ({
          methods: ({
            calculateWinnings: () => ({ call: async () => '10200000000' }),
            getWithdrawAmounts: () => ({
              call: async () => ['7500000000', '2500000000', '100000000'],
            }),
            version: () => ({ call: async () => 0 }),
            currentRound: () => ({ call: async () => 0 }),
            currentResultIndex: () => ({ call: async () => 0 }),
            currentConsensusThreshold: () => ({
              call: async () => toBN('10000000000'),
            }),
            currentArbitrationEndTime: () => ({ call: async () => toBN(0) }),
            eventMetadata: () => ({
              call: async () => [
                0,
                'Test Event',
                map(['Invalid', 'A', 'B', 'C'], item => utf8ToHex(item)),
                4,
              ],
            }),
            centralizedMetadata: () => ({
              call: async () => [
                '0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428',
                toBN(1),
                toBN(2),
                toBN(3),
                toBN(4),
              ],
            }),
            configMetadata: () => ({
              call: async () => [
                toBN('100000000'),
                toBN(300),
                toBN(10),
                toBN(10),
              ],
            }),
            totalBets: () => ({ call: async () => '10200000000' }),
            didWithdraw: () => ({ call: async () => false }),
          }),
        }),
      },
    });
  });

  afterEach(() => {
    sinon.restore();
    proxyquire.callThru();
  });

  describe('calculateWinnings()', () => {
    it('returns the calculateWinnings', async () => {
      const res = await RewardEvent.calculateWinnings({ eventAddress, address });
      assert.isDefined(res);
      assert.isString(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.calculateWinnings({ address })).to.be.rejectedWith(Error);
    });

    it('throws if address is undefined', () => {
      expect(RewardEvent.calculateWinnings({ eventAddress })).to.be.rejectedWith(Error);
    });
  });

  describe('getWithdrawAmounts()', () => {
    it('returns the getWithdrawAmounts', async () => {
      const res = await RewardEvent.getWithdrawAmounts({ eventAddress, address });
      assert.isArray(res);
      assert.isTrue(res.length === 3);
      each(res, item => assert.isString(item));
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.getWithdrawAmounts({ address })).to.be.rejectedWith(Error);
    });

    it('throws if address is undefined', () => {
      expect(RewardEvent.getWithdrawAmounts({ eventAddress })).to.be.rejectedWith(Error);
    });
  });

  describe('version()', () => {
    it('returns the version', async () => {
      const res = await RewardEvent.version({ eventAddress });
      assert.isDefined(res);
      assert.isNumber(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.version({})).to.be.rejectedWith(Error);
    });
  });

  describe('currentRound()', () => {
    it('returns the currentRound', async () => {
      const res = await RewardEvent.currentRound({ eventAddress });
      assert.isDefined(res);
      assert.isNumber(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.currentRound({})).to.be.rejectedWith(Error);
    });
  });

  describe('currentResultIndex()', () => {
    it('returns the currentResultIndex', async () => {
      const res = await RewardEvent.currentResultIndex({ eventAddress });
      assert.isDefined(res);
      assert.isNumber(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.currentResultIndex({})).to.be.rejectedWith(Error);
    });
  });

  describe('currentConsensusThreshold()', () => {
    it('returns the currentConsensusThreshold', async () => {
      const res = await RewardEvent.currentConsensusThreshold({ eventAddress });
      assert.isDefined(res);
      assert.isString(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.currentConsensusThreshold({})).to.be.rejectedWith(Error);
    });
  });

  describe('currentArbitrationEndTime()', () => {
    it('returns the currentArbitrationEndTime', async () => {
      const res = await RewardEvent.currentArbitrationEndTime({ eventAddress });
      assert.isDefined(res);
      assert.isString(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.currentArbitrationEndTime({})).to.be.rejectedWith(Error);
    });
  });

  describe('eventMetadata()', () => {
    it('returns the eventMetadata', async () => {
      const res = await RewardEvent.eventMetadata({ eventAddress });
      assert.isDefined(res);
      assert.isArray(res);
      assert.isTrue(res.length === 4);

      assert.isNumber(res[0]);
      assert.isString(res[1]);
      assert.isArray(res[2]);
      assert.isNumber(res[3]);
      assert.isTrue(res[2].length === res[3]);
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.eventMetadata({})).to.be.rejectedWith(Error);
    });
  });

  describe('centralizedMetadata()', () => {
    it('returns the centralizedMetadata', async () => {
      const res = await RewardEvent.centralizedMetadata({ eventAddress });
      assert.isDefined(res);
      assert.isArray(res);
      assert.isTrue(res.length === 5);

      assert.isString(res[0]);
      assert.isNumber(res[1]);
      assert.isNumber(res[2]);
      assert.isNumber(res[3]);
      assert.isNumber(res[4]);
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.centralizedMetadata({})).to.be.rejectedWith(Error);
    });
  });

  describe('configMetadata()', () => {
    it('returns the configMetadata', async () => {
      const res = await RewardEvent.configMetadata({ eventAddress });
      assert.isDefined(res);
      assert.isArray(res);
      assert.isTrue(res.length === 4);

      assert.isString(res[0]);
      assert.isNumber(res[1]);
      assert.isNumber(res[2]);
      assert.isNumber(res[3]);
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.configMetadata({})).to.be.rejectedWith(Error);
    });
  });

  describe('totalBets()', () => {
    it('returns the totalBets', async () => {
      const res = await RewardEvent.totalBets({ eventAddress, address });
      assert.isDefined(res);
      assert.isString(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.totalBets({})).to.be.rejectedWith(Error);
    });
  });

  describe('didWithdraw()', () => {
    it('returns the didWithdraw', async () => {
      const res = await RewardEvent.didWithdraw({ eventAddress, address });
      assert.isDefined(res);
      assert.isBoolean(res);
    });

    it('throws if eventAddress is undefined', () => {
      expect(RewardEvent.didWithdraw({ address })).to.be.rejectedWith(Error);
    });

    it('throws if address is undefined', () => {
      expect(RewardEvent.didWithdraw({ eventAddress })).to.be.rejectedWith(Error);
    });
  });
});
