const { assert } = require('chai');
const { Config, initConfig } = require('../../src/config');

describe('config/index', () => {
  describe('initConfig', () => {
    beforeEach(() => {
      process.env.NETWORK = 'testnet';
      process.env.SSL = 'false';
      process.env.NAKABASE_API_KEY = 'test';
      Config.NETWORK = 'testnet';
      Config.NAKABASE_API_KEY = 'test';
    });

    describe('validateEnv', () => {
      it('throws if NETWORK is not defined and valid', () => {
        process.env.NETWORK = undefined;
        Config.NETWORK = undefined;
        assert.throws(initConfig, Error, 'NETWORK must be [mainnet|testnet]');

        process.env.NETWORK = '';
        Config.NETWORK = '';
        assert.throws(initConfig, Error, 'NETWORK must be [mainnet|testnet]');

        process.env.NETWORK = 'fake';
        Config.NETWORK = 'fake';
        assert.throws(initConfig, Error, 'NETWORK must be [mainnet|testnet]');
      });

      it('throws if SSL is not defined and valid', () => {
        process.env.SSL = undefined;
        assert.throws(initConfig, Error, 'SSL must be [true|false]');

        process.env.SSL = '';
        assert.throws(initConfig, Error, 'SSL must be [true|false]');

        process.env.SSL = 'fake';
        assert.throws(initConfig, Error, 'SSL must be [true|false]');
      });

      it('throws if NAKABASE_API_KEY is not defined', () => {
        process.env.NAKABASE_API_KEY = undefined;
        Config.NAKABASE_API_KEY = undefined;
        assert.throws(initConfig, Error, 'NAKABASE_API_KEY must be defined');

        process.env.NAKABASE_API_KEY = '';
        Config.NAKABASE_API_KEY = '';
        assert.throws(initConfig, Error, 'NAKABASE_API_KEY must be defined');
      });
    });
  });
});
