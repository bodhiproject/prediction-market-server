const EasyGraphQLTester = require('easygraphql-tester');
const { PAGINATED_EVENTS } = require('../schema-helper');
const schema = require('../../../src/graphql/schema');

const tester = new EasyGraphQLTester(schema);

describe('graphql/queries/events', () => {
  it('should return the query', async () => {
    const query = `
      query {
        events {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the OR filter', async () => {
    const query = `
      query {
        events(filter: { OR: [
          { txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }
          { txStatus: SUCCESS }
          { address: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }
          { ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }
          { versions: [0] }
          { centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }
          { currentRound: 0 }
          { currentResultIndex: 0 }
          { status: BETTING }
          { language: "en-US" }
          { eventType: MR_EVENT }
          { excludeCentralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }
        ] }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the txid filter', async () => {
    const query = `
      query {
        events(filter: { txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the txStatus filter', async () => {
    const query = `
      query {
        events(filter: { txStatus: SUCCESS }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the address filter', async () => {
    const query = `
      query {
        events(filter: { address: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the ownerAddress filter', async () => {
    const query = `
      query {
        events(filter: { ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the version filter', async () => {
    const query = `
      query {
        events(filter: { versions: [0] }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the centralizedOracle filter', async () => {
    const query = `
      query {
        events(filter: { centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the currentRound filter', async () => {
    const query = `
      query {
        events(filter: { currentRound: 0 }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the currentResultIndex filter', async () => {
    const query = `
      query {
        events(filter: { currentResultIndex: 0 }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the status filter', async () => {
    const query = `
      query {
        events(filter: { status: BETTING }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the language filter', async () => {
    const query = `
      query {
        events(filter: { language: "en-US" }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the eventType filter', async () => {
    let query = `
      query {
        events(filter: { eventType: MR_EVENT }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);

    query = `
      query {
        events(filter: { eventType: AB_EVENT }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the excludeCentralizedOracle filter', async () => {
    const query = `
      query {
        events(filter: { excludeCentralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }) {
          ${PAGINATED_EVENTS}
        }
      }
    `;
    tester.test(true, query);
  });
});
