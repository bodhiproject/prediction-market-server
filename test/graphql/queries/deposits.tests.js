const EasyGraphQLTester = require('easygraphql-tester');
const { PAGINATED_DEPOSITS } = require('../schema-helper');
const schema = require('../../../src/graphql/schema');

const tester = new EasyGraphQLTester(schema);

describe('graphql/queries/deposits', () => {
  it('should return the query', async () => {
    const query = `
      query {
        deposits {
          ${PAGINATED_DEPOSITS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the OR filter', async () => {
    const query = `
      query {
        deposits(filter: { OR: [
          { txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }
          { txStatus: SUCCESS }
          { eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }
          { depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }
        ] }) {
          ${PAGINATED_DEPOSITS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the txid filter', async () => {
    const query = `
      query {
        deposits(filter: { txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }) {
          ${PAGINATED_DEPOSITS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the txStatus filter', async () => {
    const query = `
      query {
        deposits(filter: { txStatus: SUCCESS }) {
          ${PAGINATED_DEPOSITS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the eventAddress filter', async () => {
    const query = `
      query {
        deposits(filter: { eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }) {
          ${PAGINATED_DEPOSITS}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the depositorAddress filter', async () => {
    const query = `
      query {
        deposits(filter: { depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428" }) {
          ${PAGINATED_DEPOSITS}
        }
      }
    `;
    tester.test(true, query);
  });
});
