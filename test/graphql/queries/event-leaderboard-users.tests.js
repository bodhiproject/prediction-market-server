const EasyGraphQLTester = require('easygraphql-tester');
const { PAGINATED_LEADERBOARD } = require('../schema-helper');
const schema = require('../../../src/graphql/schema');

const tester = new EasyGraphQLTester(schema);

describe('graphql/queries/event-leaderboard-users', () => {
  it('should return the query', async () => {
    const valid = `
      query {
        eventLeaderboardEntries(filter: {
          userAddress:"0x939592864c0bd3355b2d54e4fa2203e8343b6d6a",
          eventAddress:"0x09645ea6e4e1f5375f7596b73b3b597e6507201a"
        }) {
          ${PAGINATED_LEADERBOARD}
        }
      }
    `;
    tester.test(true, valid);
  });

  it('should accept the eventAddress filter', async () => {
    const valid = `
      query {
        eventLeaderboardEntries(filter: { eventAddress: "0x09645ea6e4e1f5375f7596b73b3b597e6507201a" }) {
          ${PAGINATED_LEADERBOARD}
        }
      }
    `;
    tester.test(true, valid);
  });

  it('should accept the userAddress filter', async () => {
    const valid = `
      query {
        eventLeaderboardEntries(filter: { userAddress:"0x939592864c0bd3355b2d54e4fa2203e8343b6d6a" }) {
          ${PAGINATED_LEADERBOARD}
        }
      }
    `;
    tester.test(true, valid);
  });
});
