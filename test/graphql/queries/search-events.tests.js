const EasyGraphQLTester = require('easygraphql-tester');
const { EVENT } = require('../schema-helper');
const schema = require('../../../src/graphql/schema');

const tester = new EasyGraphQLTester(schema);

describe('graphql/queries/search-events', () => {
  it('should return the query', async () => {
    const query = `
      query {
        searchEvents {
          ${EVENT}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the OR filter', async () => {
    const query = `
      query {
        searchEvents(filter: { OR: [
          { txStatus: SUCCESS }
          { version: 0 }
          { language: "en-US" }
          { eventType: MR_EVENT }
        ] }) {
          ${EVENT}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the txStatus filter', async () => {
    const query = `
      query {
        searchEvents(filter: { txStatus: SUCCESS }) {
          ${EVENT}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the version filter', async () => {
    const query = `
      query {
        searchEvents(filter: { version: 0 }) {
          ${EVENT}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the language filter', async () => {
    const query = `
      query {
        searchEvents(filter: { language: "en-US" }) {
          ${EVENT}
        }
      }
    `;
    tester.test(true, query);
  });

  it('should accept the eventType filter', async () => {
    let query = `
      query {
        searchEvents(filter: { eventType: MR_EVENT }) {
          ${EVENT}
        }
      }
    `;
    tester.test(true, query);

    query = `
      query {
        searchEvents(filter: { eventType: AB_EVENT }) {
          ${EVENT}
        }
      }
    `;
    tester.test(true, query);
  });
});
