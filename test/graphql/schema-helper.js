const PAGE_INFO = `
  pageInfo {
    hasNextPage
    pageNumber
    count
    nextTransactionSkips {
      nextEventSkip
      nextDepositSkip
      nextBetSkip
      nextResultSetSkip
      nextWithdrawSkip
    }
  }
`;

const BLOCK = `
  block {
    blockNum
    blockTime
  }
`;

const TX_RECEIPT = `
  txReceipt {
    status
    blockHash
    blockNum
    transactionHash
    from
    to
    contractAddress
    cumulativeGasUsed
    gasUsed
    gasPrice
  }
`;

const EVENT = `
  txType
  txid
  txStatus
  ${TX_RECEIPT}
  blockNum
  ${BLOCK}
  address
  ownerAddress
  ownerName
  version
  name
  results
  numOfResults
  winningChances
  odds
  centralizedOracle
  betStartTime
  betEndTime
  resultSetStartTime
  resultSetEndTime
  escrowAmount
  arbitrationLength
  arbitrationRewardPercentage
  thresholdPercentIncrease
  currentRound
  currentResultIndex
  consensusThreshold
  previousConsensusThreshold
  arbitrationEndTime
  status
  language
  eventType
  pendingTxs {
    bet
    resultSet
    withdraw
    total
  }
  totalBets
  withdrawnList
  maxBets
`;

const PAGINATED_EVENTS = `
  totalCount
  ${PAGE_INFO}
  items {
    ${EVENT}
  }
`;

const DEPOSIT = `
  txType
  txid
  txStatus
  ${TX_RECEIPT}
  blockNum
  ${BLOCK}
  eventAddress
  depositorAddress
  depositorName
  amount
  eventName
`;

const PAGINATED_DEPOSITS = `
  totalCount
  ${PAGE_INFO}
  items {
    ${DEPOSIT}
  }
`;

const BET = `
  txType
  txid
  txStatus
  ${TX_RECEIPT}
  blockNum
  ${BLOCK}
  eventAddress
  betterAddress
  betterName
  resultIndex
  amount
  eventRound
  resultName
  eventName
`;

const PAGINATED_BETS = `
  totalCount
  ${PAGE_INFO}
  items {
    ${BET}
  }
`;

const RESULT_SET = `
  txType
  txid
  txStatus
  ${TX_RECEIPT}
  blockNum
  ${BLOCK}
  eventAddress
  centralizedOracleAddress
  centralizedOracleName
  resultIndex
  amount
  eventRound
  nextConsensusThreshold
  nextArbitrationEndTime
  resultName
  eventName
`;

const PAGINATED_RESULT_SETS = `
  totalCount
  ${PAGE_INFO}
  items {
    ${RESULT_SET}
  }
`;

const WITHDRAW = `
  txType
  txid
  txStatus
  ${TX_RECEIPT}
  blockNum
  ${BLOCK}
  eventAddress
  winnerAddress
  winnerName
  winningAmount
  creatorReturnAmount
  eventName
`;

const PAGINATED_WITHDRAWS = `
  totalCount
  ${PAGE_INFO}
  items {
    ${WITHDRAW}
  }
`;

const LEADERBOARD = `
  eventAddress
  userAddress
  userName
  investments
  winnings
  returnRatio
`;

const PAGINATED_LEADERBOARD = `
  totalCount
  ${PAGE_INFO}
  items {
    ${LEADERBOARD}
  }
`;

module.exports = {
  PAGE_INFO,
  BLOCK,
  TX_RECEIPT,
  EVENT,
  PAGINATED_EVENTS,
  DEPOSIT,
  PAGINATED_DEPOSITS,
  BET,
  PAGINATED_BETS,
  RESULT_SET,
  PAGINATED_RESULT_SETS,
  WITHDRAW,
  PAGINATED_WITHDRAWS,
  PAGINATED_LEADERBOARD,
};
