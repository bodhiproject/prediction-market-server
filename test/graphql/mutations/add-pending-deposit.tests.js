const EasyGraphQLTester = require('easygraphql-tester');
const { DEPOSIT } = require('../schema-helper');
const schema = require('../../../src/graphql/schema');

const tester = new EasyGraphQLTester(schema);

describe('graphql/mutations/add-pending-deposit', () => {
  it('It should pass if passing all correct arguments', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(true, mutation, {});
  });

  it('It should fail if txid is missing', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if txid is null', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          txid: null
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if txid is number', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          txid: 1
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if txid is array', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          txid: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if txid is object', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          txid: {id: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"}
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
  `;
    tester.test(false, mutation, {});
  });

  it('It should fail if eventAddress is missing', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if eventAddress is null', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: null
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if eventAddress is array', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if eventAddress is object', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: {id: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"}
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if eventAddress is number', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: 1
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if depositorAddress is missing', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if depositorAddress is null', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: null
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if depositorAddress is number', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: 1
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if depositorAddress is array', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if depositorAddress is object', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: {id: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"}
          amount: "1000000"
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if amount is missing', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if amount is null', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          amount: null
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if amount is number', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          amount: 1000000
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if amount is array', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          amount: ["1000000"]
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if amount is object', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          amount: {id: "1000000"}
          eventType: MR_EVENT
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if eventType is missing', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          amount: "1000000"
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if eventType is null', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          amount: "1000000"
          eventType: null
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if eventType is number', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          amount: "1000000"
          eventType: 10000
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if eventType is array', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          amount: "1000000"
          eventType: [MR_EVENT]
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if eventType is object', async () => {
    const mutation = `
      mutation {
        addPendingDeposit(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          depositorAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          amount: "1000000"
          eventType: 'MR_EVENT'
        ) {
          ${DEPOSIT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });
});
