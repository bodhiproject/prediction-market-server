const EasyGraphQLTester = require('easygraphql-tester');
const { EVENT } = require('../schema-helper');
const schema = require('../../../src/graphql/schema');
const { EVENT_TYPE } = require('../../../src/constants');

const tester = new EasyGraphQLTester(schema);

describe('graphql/mutations/add-pending-event', () => {
  it('It passes all arguments', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(true, mutation, {});
  });

  it('It should fail if txid missed', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if ownerAddress missed', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if name missed', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if results missed', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if numOfResults missed', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if centralizedOracle missed', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if betEndTime missed', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if resultSetStartTime missed', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if language missed', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if txid is null', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: null
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if txid is number', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: 1
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if txid is array', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if txid is an object', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: {id: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"}
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if ownerAddress is null', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: null
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if ownerAddress is number', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: 1
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if ownerAddress is array', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if ownerAddress is object', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: {id: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"}
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if name is null', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: null
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if name is number', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: 1
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if name is array', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: ["a"]
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if name is object', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: {id: "a"}
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if results is null', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: null
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if results is number array', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: [1, 2]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if results is object', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: {id: "1"}
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if numOfResults is null', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: null
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if numOfResults is string', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: "2"
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if numOfResults is array', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: ["2"]
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if numOfResults is object', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: {id: "2"}
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if centralizedOracle is null', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: null
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if centralizedOracle is number', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: 1
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if centralizedOracle is array', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('It should fail if centralizedOracle is object', async () => {
    const mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: {id: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"}
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: ${EVENT_TYPE.MR_EVENT}
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });

  it('should fail if eventType is not valid', async () => {
    let mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});

    mutation = `
      mutation {
        addPendingEvent(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          ownerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          name: "General test"
          results: ["1", "2"]
          numOfResults: 2
          centralizedOracle: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          betEndTime: 1560965704
          resultSetStartTime: 1560965704
          language: "en-US"
          eventType: "FAKE"
        ) {
          ${EVENT}
        }
      }
    `;
    tester.test(false, mutation, {});
  });
});
