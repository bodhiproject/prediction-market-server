const EasyGraphQLTester = require('easygraphql-tester');
const { WITHDRAW } = require('../schema-helper');
const schema = require('../../../src/graphql/schema');

const tester = new EasyGraphQLTester(schema);

describe('graphql/mutations/add-pending-withdraw', () => {
  it('It should pass if passing all arguments correctly', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(true, valid, {});
  });

  it('It should fail if txid is missing', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if txid is null', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: null
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if txid is number', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: 1
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if txid is array', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if txid is object', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: {id: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"}
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if eventAddress is missing', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if eventAddress is null', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: null
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if eventAddress is array', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if eventAddress is object', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: {id:"0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"}
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if eventAddress is number', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: 1
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if winnerAddress is missing', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if winnerAddress is null', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: null
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if winnerAddress is number', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: 1
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if winnerAddress is array', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: ["0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"]
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if winnerAddress is object', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: {id:"0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"}
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if winningAmount is missing', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if winningAmount is null', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: null
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if winningAmount is number', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: 100000
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if winningAmount is array', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: ["100000"]
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if winningAmount is object', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: {id:"100000"}
          creatorReturnAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if creatorReturnAmount is missing', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if creatorReturnAmount is null', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: null
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if creatorReturnAmount is number', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: 100000
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if creatorReturnAmount is array', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: ["100000"]
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if creatorReturnAmount is object', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: {id:"100000"}
          eventType: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if eventType is missing', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if eventType is null', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: null
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if eventType is number', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: 123
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if eventType is array', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: [MR_EVENT]
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });

  it('It should fail if eventType is object', async () => {
    const valid = `
      mutation {
        addPendingWithdraw(
          txid: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          eventAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winnerAddress: "0xd5d087daabc73fc6cc5d9c1131b93acbd53a2428"
          winningAmount: "100000"
          creatorReturnAmount: "100000"
          eventType: {id: MR_EVENT
        ) {
          ${WITHDRAW}
        }
      }
    `;
    tester.test(false, valid, {});
  });
});
