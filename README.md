# Bodhi Prediction Market Server (House)

[![pipeline status](https://gitlab.com/bodhiproject/prediction-market-server/badges/master/pipeline.svg)](https://gitlab.com/bodhiproject/prediction-market-server/commits/master)
[![coverage report](https://gitlab.com/bodhiproject/prediction-market-server/badges/master/coverage.svg)](https://gitlab.com/bodhiproject/prediction-market-server/commits/master)

## Installation

1. `git clone https://gitlab.com/bodhiproject/prediction-market-server.git`
2. `cd prediction-market-server`
3. `npm install`

## Dev Environment

### Dev Prerequisites

1. Node/NPM

### Dev Environment Setup

You must specify certain attributes in an `.env` file at the root folder.

```bash
NETWORK=[mainnet|testnet] # required
NAKABASE_API_KEY=your_api_key # required
SSL=[true|false] # required
SSL_KEY=sslKeyAsString # required only if SSL=true
SSL_CERT=sslCertAsString # required only if SSL=true
MONGO_USERNAME=myusername # optional
MONGO_PASSWORD=mypassword # optional
MONGO_HOSTNAME=localhost # optional (localhost)
MONGO_PORT=27017 # optional (27017)
MONGO_DB=myDbName # optional (bodhipmMainnet|bodhipmTestnet)
MONGO_REPLICASET=replicaSetName # optional. only if you are using a replica set.
PROVIDER=[ipc|ws|http] # optional (ipc). use ws or http for local.
IPC_PROVIDER_PATH=path/to/ipc/file # optional (/root/.naka/geth.ipc). only for provider=ipc.
DATA_DIR=/path/to/data/dir # optional (data)
LOG_LEVEL=debug # optional (debug)
```

### Start Dev Server

```bash
npm start
```

### Dev GraphQL Playground

To play around in the GraphQL playground, go to:

1. Go to the GraphQL Playground at `localhost:5000/graphql`
2. Click on `SCHEMA` button on the right side
3. Browse through all the queries, mutations, or subscriptions

## Tags

### Automated Tagging

Gitlab can create a new tag for you when a branch is merged to `master`. The tag will be pulled from the `package.json version`.

1. Login to the Gitlab repo
2. After merging a branch to `master`, it will trigger a new pipeline
3. Click on `CI/CD > Pipelines`
4. Find the pipeline of the branch you merged then click on the `Manual Job` button on the right
5. Select `tag:master`
6. A new tag will automatically be created

## Build

### Automated Docker Image via Gitlab (Recommended)

Anytime a [tag](#tags) is pushed to Gitlab, it will automatically build a Docker image and push it to Docker Hub.

1. Follow the [automated tagging](#automated-tagging) instructions
2. It will automatically go through the stages `build:context` and `build:image` and your new image will be pushed to Docker Hub!

### Manually Build Docker Image

1. `cd docker`
2. Ensure the `package.json version` is updated
3. Build the Docker image: `./build.sh`
4. Push Docker image to Docker Hub: `docker push bodhiproject/prediction-market:$VERSION`

## Deploy

### Deploy Checklist

1. I changed the version in `package.json` and `docker/build.sh`. Then I ran `npm install` to update the `package-lock.json`.
2. I checked that all the contract metadata in `src/config/contracts` is correct.
3. I changed the min contract version in `src/config/index.js` if necessary.

### Deployment Notes

- Server is first built as a Docker image and uploaded to Docker Hub under the organization `bodhiproject`.
- A Helm chart is then used to deploy the service onto Amazon EKS Kubernetes cluster service.
- As stated in the Amazon EKS Helm docs, we should run Tiller locally instead of installing it on the cluster to prevent any security issues.
- Kubectl secrets are used to store any sensitive env variables and are injected when the service is deployed.

### Contract Deploy Block Numbers

It's important to note that when upgrading/deploying new Event Factory contracts that the `mainnetDeployBlock/testnetDeployBlock` are set to a **future block that has not already been parsed by the server already**. This is to ensure that the switch to the new contracts happen seamlessly. In addition, if you **deploy the UI before this future block** then any new events will parsed using the previous Event metadata until the new contract's deploy block has been reached.

### Deploy Prerequisites

1. [Docker](https://docs.docker.com/docker-for-mac/install/)
2. [Kubernetes kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
3. [Amazon eksctl](https://docs.aws.amazon.com/eks/latest/userguide/getting-started-eksctl.html)
4. [Helm](https://docs.aws.amazon.com/eks/latest/userguide/helm.html)
5. [Tiller](https://docs.aws.amazon.com/eks/latest/userguide/helm.html)

### Deploy With Helm

Please see the [Kubernetes repo](https://gitlab.com/nakachain/kubernetes) for deploying it to a cluster.

## Migrations

Migrations must run in sequential order and should fail if any errors occured in the migration.

### Running Migrations

Migrations are now run separately from the main sync logic. If running it on a `replica set`, run it on the master DB and it will propagate to the slave DBs.

1. Edit the `.env` in the root folder with any necessary DB fields:

    ```bash
    DB_LAST_MIGRATION=migration000.js # required. enter the previous migration successfully run.
    MONGO_USERNAME=myusername # optional
    MONGO_PASSWORD=mypassword # optional
    MONGO_HOSTNAME=localhost # optional (localhost)
    MONGO_PORT=27017 # optional (27017)
    MONGO_DB=myDbName # optional (bodhipmMainnet|bodhipmTestnet) depends on the NETWORK
    ```

2. If migrating a DB pod on Kubernetes, [port-forward](#port-forward-db) to a local port first.
3. `npm run migrate`

### Adding New Migration

1. Each new migration script should be named with incrementing numbers.
2. The `next` callback should be passed into each migration.
3. On migration success, call `next(null, true)` after a successful migration so it continues to the next migration.
4. On migration error, call `next(err, false)` so it stops all further migrations. This is already embedded in a try/catch in the sample migration so you can just `throw Error()` in the `try` block.

### Sample Migration

```js
module.exports = async (next) => {
  try {
    next(null, true); // Execute next(null, true) on success
  } catch (err) {
    next(err, false); // Execute next(err, false) on error
  }
};
```

## MongoDB

### Port Forward DB

To access the production DB you will need to port forward the Kubernetes port to your localhost. Once it's forwarded, you can open your favorite MongoDB viewer app and open at as `localhost:27107`. Please note the production DB needs username/password authentication.

In a replicaset, the first service at index 0 (e.g. `mongo-mongodb-replicaset-0`) is generally the master.

```bash
# Port forwards the mongo-mongodb-replicaset-0 service:27017 to localhost:27017
$ kubectl port-forward mongo-mongodb-replicaset-0 27017 27017
```

### Copy DB

Run the following after you have `port-forwarded` the production DB to your local port:

```bash
# mongodump --username username --password password --authenticationDatabase authDbName --db dbName
$ mongodump --username admin --password abcdefgh --authenticationDatabase admin --db bodhipmTestnet
```

### Restore DB

After copying the DB, you can restore it to your local DB. **Don't forget to cancel the port-forwarding so you don't overwrite the production DB!**

```bash
# mongorestore --db dbName dumpedFolder
$ mongorestore --db bodhipmTestnet bodhipmTestnet
```

### Validators

Once the MongoDB validators (`db/validators`) are in production, you cannot just add to it. You will have to write a migration to **replace** the existing validator schema. There is **no add to existing validator function** so you will have to import the existing validator, modify the fields, and run the MongoDB Node driver `db.command()` to replace the validator. In addition, you will have to set `validationLevel: "moderate"`. This will apply the new validation rules to inserts and updates to existing documents that already fulfill the validation criteria. See more at [https://docs.mongodb.com/manual/core/schema-validation/](https://docs.mongodb.com/manual/core/schema-validation/).

```js
// Example migration to modify the Events validator
const { cloneDeep } = require('lodash');
const { db } = require('..');
const logger = require('../../utils/logger');
const eventValidator = require('../validators/events');

module.exports = async (next) => {
  try {
    // Clone and add reward event type
    const validator = cloneDeep(eventValidator);
    validator.$jsonSchema.properties.eventType.enum.push('REWARD_EVENT');

    // Apply new validator
    await db().command({
      collMod: 'events',
      validator,
      validationLevel: 'moderate',
    });

    logger.info('Added EventType REWARD_EVENT');
    next(null, true); // Execute next(null, true) on success
  } catch (err) {
    next(err, false); // Execute next(err, false) on error
  }
};
```

To view existing validators using Robo 3T:

1. Connect to the DB
2. Right-click on the DB name on the left
3. Select `Open Shell`
4. In the new shell window, run the command `db.getCollectionInfos({ name: 'events' })`. Replace the name with the collection you would like to see the validator for.
