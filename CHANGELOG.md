# Changelog

## Current

- [!47] - Remove index check in built image clean script

## 7.3.6

- [!46] - Fix withdrawable events not showing non-centralized oracle result setters

## 7.3.5

- [!45] - Fix withdrawable events query not showing all events

## 7.3.4

- [!44] - Remove install stage for CI/CD

## 7.3.3

- [!43] - Optimize automation variables

## 7.3.2

- [!42] - Disable tagging stage when doing S3 cleanup

## 7.3.1

- [!41] - Add clean stage for removing old build contexts from S3

## 7.3.0

- [!40] - Gitlab automated building and push to Docker Hub

## 7.2.3

- [!39] - Fix search

## 7.2.2

- [!38] - Update version

## 7.2.1

- [!36] - Update ABEvent and RewardEvent contracts

## 7.2.0

- [!30] - Migration to add eventType for bets, deposits, resultSets, withdraws collection and add eventType for transactions query
- [!31] - Migration to add RewardEvent type and separate validators from inits
- [!35] - Make migrations standalone and add RewardEvent escrowAmount migration

## 7.1.0

- [!20] - Merge reward server
- [!24] - Fix update leaderboard event type error and winning event not shown in withdraw dashboard
- [!26] - Fix withdraw dashboard show all eventType events you created
- [!27] - Fix reward event deposit always show 0
- [!34] - Update version

## 7.0.9

- [!33] - Use last migration from env if declared

## 7.0.8

- [!32] - Fix event escrow amount parsing

## 7.0.7

- [!29] - Fix for sync logic error

## 7.0.6

- [!28] - Hotfix for changing Event address index

## 7.0.5

- [!23] - Hotfix for handling multiple writes from replicated servers

## 7.0.4

- [!23] - Add mainnet contract metadata

## 7.0.3

- [!21] - Stop parsing EscrowDeposited for MREvents

## 7.0.2

- [!19] - Add missing await to db call

## 7.0.1

- [!18] - Add/remove contract APIs

## 7.0.0

- [!1] - Update master with bodhi-server
- [!4] - Migrate to MongoDB
- [!6] - Fix tests for MongoDB refactor and rename MultipleResultsEvent
- [!7] - Fix string to number is not a function
- [!8] - Add Deposits to transactions query and add deposits query
- [!10] - Adapt to receive unpaidBet, adapt to new contract withdraw event
- [!11] - Use MongoDB replica set and change Docker config
- [!12] - Change DB names
- [!13] - Reset version
- [!14] - Update readme
- [!15] - Update contract
- [!16] - Merge Classic version logic
- [!17] - Fix syncPendingEvent query and configManagerMeta version
