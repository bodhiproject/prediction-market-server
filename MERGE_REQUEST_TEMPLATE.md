# Merge Request
<!-- Please fill out the form for your Merge Request and complete the Checklist. -->

## Checklist

- [ ] I added the `CHANGELOG.md` entry.
- [ ] I added new functionality so I added tests covering it.
- [ ] I fixed a bug so I added a regression test to prevent the bug from silently reappearing again.
- [ ] I checked if I needed to update the docs and did so.

## Release Checklist

- [ ] I changed the version in `package.json`. Then I ran `npm install` to update `package-lock.json`.
- [ ] I checked that all the contract metadata in `src/config/contracts` is correct.
- [ ] I changed the min contract version in `src/config/index.js` if necessary.

## Notes
<!-- Include anything here that needs to be noted for the reviewer. -->