#!/bin/sh
# build.sh
# Builds the Bodhi Prediction Market Docker image

TAG=$(cat package.json | python -c "import json,sys;obj=json.load(sys.stdin);print obj['version'];")

docker build \
-f Dockerfile \
-t "bodhiproject/prediction-market:$TAG" \
../
